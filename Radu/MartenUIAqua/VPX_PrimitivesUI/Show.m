//
//  ShowController.m
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 Radu Cristescu.
//  Distributed under the terms of the MIT licence.
//

#import "Show.h"

@implementation Show
@synthesize window;
@synthesize textMessage;

- (id)init
{
    [[[NSNib alloc] initWithNibNamed:@"Show" bundle:[NSBundle bundleWithIdentifier:@"com.andescotia.frameworks.martenui.aqua"]] instantiateNibWithOwner:self topLevelObjects:nil];

    return self;
}

- (NSInteger)show:(NSString *)displayString
{
    textMessage.stringValue = displayString;
    return [NSApp runModalForWindow:self.window];
}

- (IBAction)onClicked_OK:(NSButton *)sender {
    [NSApp stopModalWithCode:0];
    [self.window close];
}
@end

#pragma mark - C bridge

NSInteger displayShowModal(CFStringRef displayString)
{
    static Show *dialog = nil;
    if (dialog == nil) {
        dialog = [[Show alloc] init];
    }
    return [dialog show:(NSString *)displayString];
}
