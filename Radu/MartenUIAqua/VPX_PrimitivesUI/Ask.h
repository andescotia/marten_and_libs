//
//  Ask.h
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Cocoa/Cocoa.h>

@interface Ask : NSObject {
    NSWindow *window;
    NSTextField *textMessage;
    NSTextField *answerText;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *textMessage;
@property (assign) IBOutlet NSTextField *answerText;

- (IBAction)onClick_OK:(NSButton *)sender;
- (IBAction)onClick_Cancel:(NSButton *)sender;
@end
