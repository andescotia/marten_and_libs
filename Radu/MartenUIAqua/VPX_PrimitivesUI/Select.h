//
//  Select.h
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <Cocoa/Cocoa.h>

@interface Select : NSObject <NSTableViewDataSource> {
    NSWindow *window;
    NSTextField *textMessage;
    NSTableView *list;
    NSArray *_choices;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *textMessage;
@property (assign) IBOutlet NSTableView *list;

@property (assign) NSArray *choices;

- (IBAction)onClick_OK:(NSButton *)sender;
- (IBAction)onClick_Cancel:(NSButton *)sender;
@end
