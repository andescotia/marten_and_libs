//
//  Select.m
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import "Select.h"

@implementation Select

@synthesize window;
@synthesize textMessage;
@synthesize list;
@synthesize choices = _choices;

- (id)init
{
    [[[NSNib alloc] initWithNibNamed:@"Select" bundle:[NSBundle bundleWithIdentifier:@"com.andescotia.frameworks.martenui.aqua"]] instantiateNibWithOwner:self topLevelObjects:nil];
    
    return self;
}

- (NSInteger)show:(NSArray *)choiceStrings displayString:(NSString *)displayString
{
    if (displayString == nil) {
        displayString = @"";
    }
    textMessage.stringValue = displayString;
    
    _choices = choiceStrings;
    list.dataSource = self;
        
    return [NSApp runModalForWindow:window];
}

- (NSString *)getSelection
{
    if (list.selectedRow >= 0) {
        return [_choices objectAtIndex:list.selectedRow];
    }
    
    return nil;
}

- (IBAction)onClick_OK:(NSButton *)sender {
    [NSApp stopModalWithCode:0];
    [self.window close];
}

- (IBAction)onClick_Cancel:(NSButton *)sender {
    [NSApp stopModalWithCode:1];
    [self.window close];
}

#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    if (_choices == nil) {
        return 0;
    }
    return _choices.count;
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    return [_choices objectAtIndex:row];
}

@end

#pragma mark - C bridge

NSInteger displaySelectModal(CFArrayRef choices, CFStringRef displayString, CFStringRef *selectStringOutput)
{
    static Select *dialog = nil;
    if (dialog == nil) {
        dialog = [[Select alloc] init];
    }
    
    NSInteger result = [dialog show:(NSArray *)choices displayString:(NSString *)displayString];

    if (result == 0) {
        *selectStringOutput = (CFStringRef)[[NSString alloc] initWithString:[dialog getSelection]];
    }
    
    return result;
}
