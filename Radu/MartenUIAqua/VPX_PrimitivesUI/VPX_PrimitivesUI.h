//
//  VPX_PrimitivesUI.h
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#ifndef MartenUIAqua_VPX_PrimitivesUI_h
#define MartenUIAqua_VPX_PrimitivesUI_h

// extracted definition of NSInteger and NSUInteger
// because we cannot include Foundation/NSObjCRuntime.h due to Objective C content

#ifndef NSInteger
#if __LP64__ || TARGET_OS_EMBEDDED || TARGET_OS_IPHONE || TARGET_OS_WIN32 || NS_BUILD_32_LIKE_64
typedef long NSInteger;
typedef unsigned long NSUInteger;
#else
typedef int NSInteger;
typedef unsigned int NSUInteger;
#endif
#endif /* NSInteger */

NSInteger displayShowModal(CFStringRef displayString);
NSInteger displayAskModal(CFStringRef displayString, CFStringRef defaultString, CFStringRef *askStringOutput);
NSInteger displayAnswerModal(CFStringRef displayString, CFStringRef button1String, CFStringRef button2String, CFStringRef button3String);
NSInteger displayAnswerModalV(CFStringRef displayString, CFStringRef button1String, CFStringRef button2String, CFStringRef button3String);
NSInteger displaySelectModal(CFArrayRef choices, CFStringRef displayString, CFStringRef *selectStringOutput);

#endif
