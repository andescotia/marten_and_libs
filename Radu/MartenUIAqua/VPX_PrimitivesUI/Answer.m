//
//  Answer.m
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import "Answer.h"

@implementation Answer
@synthesize window;
@synthesize textMessage;
@synthesize button1;
@synthesize button2;
@synthesize button3;

- (id)init
{
    [[[NSNib alloc] initWithNibNamed:@"Answer" bundle:[NSBundle bundleWithIdentifier:@"com.andescotia.frameworks.martenui.aqua"]] instantiateNibWithOwner:self topLevelObjects:nil];
    
    return self;
}

- (id)initV
{
    [[[NSNib alloc] initWithNibNamed:@"AnswerV" bundle:[NSBundle bundleWithIdentifier:@"com.andescotia.frameworks.martenui.aqua"]] instantiateNibWithOwner:self topLevelObjects:nil];
    
    return self;
}

- (NSInteger)show:(NSString *)displayString button1:(NSString *)button1String button2:(NSString *)button2String button3:(NSString *) button3String
{
    if (displayString == nil) {
        displayString = @"";
    }
    
    button1.hidden = NO;
    button2.hidden = button2String == nil;
    button3.hidden = button3String == nil;

    button1.title = button1String;
    if (!button2.isHidden) {
        button2.title = button2String;
    }
    if (!button3.isHidden) {
        button3.title = button3String;
    }
    
    textMessage.stringValue = displayString;
    
    return [NSApp runModalForWindow:self.window];
}

- (IBAction)onButtonClick:(NSButton *)sender {
    [NSApp stopModalWithCode:sender.tag];
    [self.window close];
}
@end

#pragma mark - C bridge

NSInteger displayAnswerModal(CFStringRef displayString, CFStringRef button1String, CFStringRef button2String, CFStringRef button3String)
{
    static Answer *dialog = nil;
    if (dialog == nil) {
        dialog = [[Answer alloc] init];
    }
    return [dialog show:(NSString *)displayString
                button1:(NSString *)button1String
                button2:(NSString *)button2String
                button3:(NSString *)button3String];
}

NSInteger displayAnswerModalV(CFStringRef displayString, CFStringRef button1String, CFStringRef button2String, CFStringRef button3String)
{
    static Answer *dialog = nil;
    if (dialog == nil) {
        dialog = [[Answer alloc] initV];
    }
    return [dialog show:(NSString *)displayString
                button1:(NSString *)button1String
                button2:(NSString *)button2String
                button3:(NSString *)button3String];
}
