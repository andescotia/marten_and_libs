//
//  Ask.m
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import "Ask.h"

@implementation Ask

@synthesize window;
@synthesize textMessage;
@synthesize answerText;

- (id)init
{
    [[[NSNib alloc] initWithNibNamed:@"Ask" bundle:[NSBundle bundleWithIdentifier:@"com.andescotia.frameworks.martenui.aqua"]] instantiateNibWithOwner:self topLevelObjects:nil];
    
    return self;
}

- (NSInteger)show:(NSString *)displayString default:(NSString *)defaultString
{
    if (displayString == nil) {
        displayString = @"";
    }
    if (defaultString == nil) {
        defaultString = @"";
    }
    textMessage.stringValue = displayString;
    answerText.stringValue = defaultString;
    
    return [NSApp runModalForWindow:self.window];
}

- (IBAction)onClick_OK:(NSButton *)sender {
    [NSApp stopModalWithCode:0];
    [self.window close];
}

- (IBAction)onClick_Cancel:(NSButton *)sender {
    [NSApp stopModalWithCode:1];
    [self.window close];
}
@end

#pragma mark - C bridge

NSInteger displayAskModal(CFStringRef displayString, CFStringRef defaultString, CFStringRef *askStringOutput)
{
    static Ask *dialog = nil;
    if (dialog == nil) {
        dialog = [[Ask alloc] init];
    }
    NSInteger result = [dialog show:(NSString *)displayString default:(NSString *)defaultString];
    if (result == 0) {
        *askStringOutput = (CFStringRef)[[NSString alloc] initWithString:dialog.answerText.stringValue];
    }
    return result;
}
