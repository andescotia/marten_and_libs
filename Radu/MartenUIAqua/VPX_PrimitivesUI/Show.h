//
//  ShowController.h
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Show : NSObject {
    NSWindow *window;
    NSTextField *textMessage;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *textMessage;

- (IBAction)onClicked_OK:(NSButton *)sender;
@end
