//
//  Answer.h
//  MartenUIAqua
//
//  Created by Radu on 20/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <Cocoa/Cocoa.h>

@interface Answer : NSObject {
    NSWindow *window;
    NSTextField *textMessage;
    NSButton *button1;
    NSButton *button2;
    NSButton *button3;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *textMessage;
@property (assign) IBOutlet NSButton *button1;
@property (assign) IBOutlet NSButton *button2;
@property (assign) IBOutlet NSButton *button3;

- (IBAction)onButtonClick:(NSButton *)sender;
@end
