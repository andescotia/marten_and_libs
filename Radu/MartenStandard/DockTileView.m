//
//  DockTileView.m
//  MartenStandard
//
//  Created by Radu on 21/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import "DockTileView.h"

#pragma mark - callback holder

@interface callbackHolder : NSObject {
    dockTileDrawingCallback callback;
}

@property (assign) dockTileDrawingCallback callback;

@end

@implementation callbackHolder

@synthesize callback;

+ (callbackHolder*)with:(dockTileDrawingCallback)callback
{
    callbackHolder *holder = [[self alloc] init];
    holder->callback = callback;
    
    return holder;
}

@end

#pragma mark - DockTileView implementation

@implementation DockTileView

@synthesize callbackStack;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        callbackStack = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSRect frame = self.frame;
    CGImageRef myImage = [[NSApp applicationIconImage] CGImageForProposedRect:&frame context:[NSGraphicsContext currentContext] hints:nil];
    
    CGContextRef docktileContext = [[NSGraphicsContext currentContext] graphicsPort];
    
    CGRect rect = NSRectToCGRect(self.frame);
    CGContextDrawImage(docktileContext, rect, myImage);

    for (callbackHolder *holder in callbackStack) {
        dockTileDrawingCallback callback = holder.callback;
        callback(docktileContext);
    }
}

- (void)clearDockTileDrawingStack
{
    [callbackStack removeAllObjects];
}

- (void)addDockTileDrawingCallBack:(dockTileDrawingCallback)callback
{
    [callbackStack addObject:[callbackHolder with:callback]];
}

@end

#pragma mark - C bridge

DockTileView* getDockTileView()
{
    static DockTileView *dockTileView = nil;
    
    if (dockTileView == nil) {
        dockTileView = [[DockTileView alloc] init];
        [NSApp dockTile].contentView = dockTileView;
    }
    
    return dockTileView;
}

void clearDockTileDrawingStack()
{
    [getDockTileView() clearDockTileDrawingStack];
}

void addDockTileDrawingCallBack(dockTileDrawingCallback callback)
{
    [getDockTileView() addDockTileDrawingCallBack:callback];
}

void displayDockTile()
{
    [[NSApp dockTile] display];
}
