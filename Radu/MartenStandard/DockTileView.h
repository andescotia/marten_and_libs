//
//  DockTileView.h
//  MartenStandard
//
//  Created by Radu on 21/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "DockTile.h"

@interface DockTileView : NSView {
    NSMutableArray *callbackStack;
}

@property (assign) NSMutableArray *callbackStack;

@end
