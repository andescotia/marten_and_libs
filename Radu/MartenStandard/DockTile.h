//
//  DockTile.h
//  MartenStandard
//
//  Created by Radu on 21/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#ifndef MartenStandard_DockTile_h
#define MartenStandard_DockTile_h

typedef void (*dockTileDrawingCallback) (CGContextRef dockTileContext);

void clearDockTileDrawingStack();
void addDockTileDrawingCallBack(dockTileDrawingCallback callback);
void displayDockTile();

#endif
