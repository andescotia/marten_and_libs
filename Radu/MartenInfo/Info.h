//
//  Info.h
//  MartenInfo
//
//  Created by Radu on 21/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Info : NSObject {
    NSTextField *bannerText;
    NSTextField *displayText;
    NSTextField *copyrightText;
    NSButton *buyNowButton;
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@property (assign) IBOutlet NSTextField *bannerText;
@property (assign) IBOutlet NSTextField *displayText;
@property (assign) IBOutlet NSTextField *copyrightText;
@property (assign) IBOutlet NSButton *buyNowButton;

- (IBAction)onClick_Logo:(NSButton *)sender;
- (IBAction)onClick_Help:(NSButton *)sender;
- (IBAction)onClick_BuyNow:(NSButton *)sender;
- (IBAction)onClick_OK:(NSButton *)sender;
- (IBAction)onClick_Link:(NSButton *)sender;
@end
