//
//  VPX_PrimitiveInfo.h
//  MartenInfo
//
//  Created by Radu on 21/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#ifndef MartenInfo_VPX_PrimitiveInfo_h
#define MartenInfo_VPX_PrimitiveInfo_h

// extracted definition of NSInteger and NSUInteger
// because we cannot include Foundation/NSObjCRuntime.h due to Objective C content

#ifndef NSInteger
#if __LP64__ || TARGET_OS_EMBEDDED || TARGET_OS_IPHONE || TARGET_OS_WIN32 || NS_BUILD_32_LIKE_64
typedef long NSInteger;
typedef unsigned long NSUInteger;
#else
typedef int NSInteger;
typedef unsigned int NSUInteger;
#endif
#endif /* NSInteger */

NSInteger showInfoModalDialog(CFStringRef bannerString, CFStringRef displayString, CFStringRef copyrightString, bool hasPurchase);

#endif
