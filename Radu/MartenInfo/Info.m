//
//  Info.m
//  MartenInfo
//
//  Created by Radu on 21/01/2022.
//  Copyright (c) 2022 __MyCompanyName__. All rights reserved.
//

#import "Info.h"

#import "VPX_PrimitiveInfo.h"

NSString *AndescotiaHome = @"https://www.andescotia.com/";
NSString *AndescotiaSale = @"https://andescotia.com/store/";

@implementation Info
@synthesize window;
@synthesize bannerText;
@synthesize displayText;
@synthesize copyrightText;
@synthesize buyNowButton;

- (id)init
{
    [[[NSNib alloc] initWithNibNamed:@"Info" bundle:[NSBundle bundleWithIdentifier:@"com.andescotia.frameworks.marten.info"]] instantiateNibWithOwner:self topLevelObjects:nil];
    
    return self;
}

- (NSInteger)showWithBannerString:(NSString *)bannerString displayString:(NSString *)displayString copyrightString:(NSString *)copyrightString hasPurchase:(BOOL)hasPurchase
{
    if (bannerString != nil) {
        bannerText.stringValue = bannerString;
    }
    
    if (displayString != nil) {
        displayText.stringValue = displayString;
    }
    
    if (copyrightString != nil) {
        copyrightText.stringValue = copyrightString;
    }
    
    buyNowButton.hidden = hasPurchase;
    
    return [NSApp runModalForWindow:self.window];
}

- (IBAction)onClick_Logo:(NSButton *)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:AndescotiaHome]];
}

- (IBAction)onClick_Help:(NSButton *)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:AndescotiaHome]];
}

- (IBAction)onClick_BuyNow:(NSButton *)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:AndescotiaSale]];
}

- (IBAction)onClick_Link:(NSButton *)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:AndescotiaHome]];
}

- (IBAction)onClick_OK:(NSButton *)sender {
    [NSApp stopModalWithCode:0];
    [self.window close];
}
@end

#pragma - C bridge

NSInteger showInfoModalDialog(CFStringRef bannerString, CFStringRef displayString, CFStringRef copyrightString, bool hasPurchase)
{
    static Info *dialog = nil;
    if (dialog == nil) {
        dialog = [[Info alloc] init];
    }
    
    return [dialog
            showWithBannerString:(NSString *)bannerString
            displayString:(NSString *)displayString
            copyrightString:(NSString *)copyrightString
            hasPurchase:hasPurchase];
}