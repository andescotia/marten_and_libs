import yaml

from VPL.archive import VplArchive


def unpack(file_name: str) -> VplArchive:
    with open(file_name, 'rb') as f:
        data = f.read()

    archive = VplArchive()
    archive.unpack(data)

    return archive


def export(archive: VplArchive, file_name: str):
    result = archive.export()
    d = yaml.dump(result)
    with open(file_name, 'wb') as f:
        f.write(bytes(d, 'utf-8'))


if __name__ == '__main__':
    app = unpack('files/Info App.vpx')
    export(app, 'files/Info App.vpx.yml')

    section = unpack('files/Info Section.vpl')
    export(section, 'files/Info Section.vpl.yml')

    vpz = unpack('files/Application.vpz')
    export(vpz, 'files/Application.vpz.yml')
