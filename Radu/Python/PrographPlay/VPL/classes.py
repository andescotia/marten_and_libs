class VplObject:
    def __init__(self):
        self.instanceIndex = 0
        self.system = 0
        self.type = 0
        self.mark = 0
        self.use = 0
        self.previousPtr = 0
        self.nextPtr = 0

    def unpack(self, up) -> None:
        self.instanceIndex = up('>H')  # Saved, but not used. Overridden based on the "name" property of VplInstance
        self.system = up('B')
        self.type = up('B')  # tells which class the data is meant for
        self.mark = up('>I')  # Used by the C loader to keep adjusted objects and to avoid circular references
        self.use = up('>I')
        self.previousPtr = up('>I')
        self.nextPtr = up('>I')

    def export(self) -> dict:
        result = {
            '$class': self.__class__.__name__,
            'instanceIndex': self.instanceIndex,
            'system': self.system,
            'type': self.type,
            'mark': self.mark,
            'use': self.use,
            'previousPtr': self.previousPtr,
            'nextPtr': self.nextPtr,
        }
        return result


class VplUndefined(VplObject):
    def __str__(self):
        return 'VplUndefined'


class VplNone(VplObject):
    def __str__(self):
        return 'VplNone'


class VplBoolean(VplObject):
    def __init__(self):
        super().__init__()
        self.value = False

    def unpack(self, up) -> None:
        super().unpack(up)
        self.value = up('>H') != 0

    def export(self) -> dict:
        result = super().export()
        result.update({
            'value': self.value,
        })
        return result

    def __str__(self):
        return str(self.value != 0)


class VplInteger(VplObject):
    def __init__(self):
        super().__init__()
        self.value = 0

    def unpack(self, up) -> None:
        self.value = up('>I')

    def export(self) -> dict:
        result = super().export()
        result.update({
            'value': self.value,
        })
        return result

    def __str__(self):
        return str(self.value)


class VplReal(VplObject):
    def __init__(self):
        super().__init__()
        self.value = 0.0

    def unpack(self, up) -> None:
        super().unpack(up)
        self.value = up('>d')

    def export(self) -> dict:
        result = super().export()
        result.update({
            'value': self.value,
        })
        return result

    def __str__(self):
        return str(self.value)


class VplString(VplObject):
    def __init__(self):
        super().__init__()
        self.string = None
        self.length = 0

    def unpack(self, up) -> None:
        super().unpack(up)

        self.string = up.unpack_cstring(offset_unpack='>I')
        self.length = up('>I')  # This value is bogus on disk. MartenEngine uses the C string's length when loading

    def export(self) -> dict:
        result = super().export()
        result.update({
            'string': self.string,
            'length': self.length,
        })
        return result

    def __str__(self):
        return self.string


class VplList(VplObject):
    def __init__(self):
        super().__init__()
        self.objectListOffset = 0
        self.listLength = 0
        self.objectList = []

    def unpack(self, up) -> None:
        super().unpack(up)

        self.objectListOffset = up('>I')
        self.listLength = up('>I')

        self.objectList = []
        up.push_offset(self.objectListOffset)
        for counter in range(self.listLength):
            offset = up('>I')
            loaded_object = up.load_object(offset)
            self.objectList.append({'offset': offset, 'object': loaded_object})
        up.pop_offset()

    def export(self) -> dict:
        result = super().export()
        result.update({
            'objectListOffset': self.objectListOffset,
            'listLength': self.listLength,
            'objectList': [{'$object': entry['offset']} if entry['offset'] != 0 else None for entry in self.objectList],
        })

        return result

    def __str__(self):
        return 'VplList len={0}'.format(len(self.objectList))


class VplInstance(VplList):
    def __init__(self):
        super().__init__()
        self.name = None

    def unpack(self, up) -> None:
        super().unpack(up)

        self.name = up.unpack_cstring(offset_unpack='>I')

    def export(self) -> dict:
        result = super().export()
        result.update({
            'name': self.name,
        })
        return result

    def __str__(self):
        return 'VplInstance ' + self.name


class VplExternalBlock(VplObject):
    def __init__(self):
        super().__init__()
        self.name = None
        self.size = 0
        self.blockPtr = None
        self.levelOfIndirection = 0

    def export(self) -> dict:
        result = super().export()
        result.update({
            'name': self.name,
            'size': self.size,
            'blockPtr': self.blockPtr,
            'levelOfIndirection': self.levelOfIndirection,
        })
        return result

    def unpack(self, up) -> None:
        super().__init__()

        self.name = up.unpack_cstring(offset_unpack='>I')
        self.size = up('>I')


VplObjectTypeClassMap = [
    VplObject,
    VplUndefined,
    VplNone,
    VplBoolean,
    VplInteger,
    VplReal,
    VplString,
    VplList,
    VplInstance,
    VplExternalBlock,
]


def make_instance_of_type(object_type: int) -> VplObject:
    if object_type < 0 or object_type >= len(VplObjectTypeClassMap):
        raise ValueError('Unacceptable object_type={}'.format(object_type))
    return VplObjectTypeClassMap[object_type]()
