from typing import AnyStr

from VPL.unpacker import Unpacker


class VplArchive:
    def __init__(self):
        self.locationsOffset = 0
        self.objectSize = 0
        self.objectStartOffset = 0
        self.numberOfPositions = 0
        self.positionsOffset = 0
        self.object = None

        self.positions = []
        self.objectCache = {}

    def unpack(self, data: AnyStr):
        up = Unpacker(data)

        self.locationsOffset = up('>I')
        self.objectSize = up('>I')
        self.objectStartOffset = up('>I')  # location of the start of the root VplObject
        self.numberOfPositions = up('>I')  # number of pointers to be adjusted after loading into C structures
        self.positionsOffset = up('>I')  # location of pointer adjustment information

        self.positions = []

        # array of pointers to pointers that have to be adjusted after loading the archive in C structures
        up.push_offset(self.positionsOffset)
        for counter in range(self.numberOfPositions):
            offset = up('>I')

            up.push_offset(offset)
            location = up('>I')
            self.positions.append(location)
            up.pop_offset()
        up.pop_offset()

        self.object = up.load_object(self.objectStartOffset)
        self.objectCache = up.objectCache

    def export(self) -> dict:
        result = {
            '$class': 'VplArchive',
            'locationOffset': self.locationsOffset,
            'objectSize': self.objectSize,
            'objectStartOffset': self.objectStartOffset,
            'numberOfPositions': self.numberOfPositions,
            'positions': self.positions,
            'object': {'$object': self.objectStartOffset},
            '$allObjects': {},
        }

        for obj_id, obj in self.objectCache.items():
            result['$allObjects'].update({obj_id: obj.export()})

        return result
