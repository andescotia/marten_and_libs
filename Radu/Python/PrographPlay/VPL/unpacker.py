from __future__ import annotations

from struct import unpack_from, calcsize
from typing import Any, AnyStr, Tuple

import VPL.classes as classes


class Unpacker:
    def __init__(self, archive: AnyStr):
        self.offsetStack = []
        self.objectCache = {}

        self.offset = 0
        self.archive = archive

    def unpack_next(self, fmt: str) -> Tuple | Any:
        result = unpack_from(fmt, self.archive, self.offset)
        self.offset += calcsize(fmt)

        # unpack the tuple if it has only one result
        if len(result) == 1:
            return result[0]

        return result

    def unpack_cstring(self, encoding: str = 'utf-8', offset_unpack: str | None = None) -> str:
        if offset_unpack is not None:
            offset = self.unpack_next(offset_unpack)
            self.push_offset(offset)

        end_of_string = self.archive.index(0, self.offset)
        text = self.archive[self.offset:end_of_string].decode(encoding)
        self.offset = end_of_string + 1

        if offset_unpack is not None:
            self.pop_offset()

        return text

    def push_offset(self, new_offset: int) -> None:
        self.offsetStack.append(self.offset)
        self.offset = new_offset

    def pop_offset(self) -> None:
        self.offset = self.offsetStack.pop()

    def __call__(self, *args, **kwargs):
        return self.unpack_next(args[0])

    def __str__(self):
        return 'len={0} offset={1} stack={2}'.format(len(self.archive), self.offset, self.offsetStack)

    def load_object(self, object_start_offset: int) -> classes.VplObject | None:
        # An offset of 0 is a NULL pointer. It would never appear in Archive.positions,
        # and it would create a recursive loop as it attempts to load objects from the start of the archive
        if object_start_offset == 0:
            return None

        # the offset is also a memory pointer, and things can point to each other in circles
        if object_start_offset in self.objectCache:
            return self.objectCache[object_start_offset]

        # decode the bare minimum to know what kind of object we're working with
        self.push_offset(object_start_offset)
        _, _, object_type = self.unpack_next('<HBB')
        self.pop_offset()

        # store the reference in the cache early
        # it will be unpacked properly in due course
        loaded_object = classes.make_instance_of_type(object_type)
        self.objectCache[object_start_offset] = loaded_object

        # decode the full object
        self.push_offset(object_start_offset)
        loaded_object.unpack(self)
        self.pop_offset()

        return loaded_object
