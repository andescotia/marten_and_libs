# VplArchive (VPX, VPL, VPZ)

This is a format used to store Marten program "source". It is used by files with these extensions: VPX, VPL, VPZ.

The stored data closely matches the C structure memory layout use by Marten. The pointers are stored as
relative to the beginning of the archive.

The C code "revives" the memory layout by adding the base of the structure to the offsets recorded in the file.
The archive contains a list of the locations of all pointers that have to be adjusted.

All the numeric values (bool, int, double) are stored in Big-Endian format.

Not all fields make sense when loaded from disk. For example, `instanceIndex` is ignored and looked up at load time
based on the `name` field for VplInstance objects, and it has no meaning for any other object type.
