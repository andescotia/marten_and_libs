This project isn't properly licensed by its author.

I (Radu Cristescu) received a dump of the source code from Scott Anderson, representing Andescotia, with the following notes:

* Email number 1, 31 August 2021

> As someone who expressed an interest in Marten and Prograph, I though I would provide an update.
>
> There were plans to update Marten but unfortunately it is clear that there is no time available to do it.  Currently, we use the alpha “Windows” version of Marten to help port/rewrite Prograph code to C#.  The Mac version of Marten is still used for maintenance of a couple of 2013 vintage Prograph applications.
>
> Marten currently runs under MacOS X 10.14.6 and that is probably the last version of MacOS it will run under until it has been rewritten for 64 bit by someone interested in doing it.
>
> Consequently, we have recently decided to make the Marten code base available to anyone who would like to tinker with it and perhaps continue its development.  Basically “open source” it.

* Email number 2, 2 September 2021

> While we used to require NDA’s when we released the source to an interested developer, I think it’s time to just let it go.  And when I look at the effort in really open-sourcing it, for example:
>
> https://www.linuxfoundation.org/resources/open-source-guides/starting-an-open-source-project/
>
> I just don’t have the time.  So let your conscience be your guide.

* My view

My conscience tells me that this is a piece of history that shouldn't be lost, and the world should have access to it. I'm a big fan of the ideas of Bret Victor, and Prograph, the language that Marten implements, was a rare beast that made our computers more than _glorified typewriters_. Therefore, I'm putting my efforts of revival on the Internet for everyone who is interested.

Unfortunately, due to not having a proper licence that would hold weight in a court of law, consider this code to be unusable for any purpose, short of receiving an unequivocal declaration of licensing or dedication to the public domain from the copyright holders - one of which, Jack Small, has left this world in 2011. Tinker with this at your own risk of lost effort.

# License pertaining to my code only, not the rest of Marten

I'm licensing my own code under the MIT licence. Take it and run away with it. Hopefully you'll make something good for the world with it.

To clarify, my work that is brand new are in the _Radu_ directory. Changes to existing Marten files are considered derivative work of the original, and the caveat mentioned earlier applies.
