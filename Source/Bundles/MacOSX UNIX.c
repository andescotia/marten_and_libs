/*
	
	MacOSX UNIX.c
	Copyright 2004 Scott B. Anderson, All Rights Reserved.
	
*/
#include "MacOSX UNIX.h"

Nat4	load_MacOSX_UNIX_Constants(V_Environment environment,char *bundleID);
Nat4	load_MacOSX_UNIX_Structures(V_Environment environment,char *bundleID);
Nat4	load_MacOSX_UNIX_Procedures(V_Environment environment,char *bundleID);

#pragma export on

Nat4	load_UNIX(V_Environment environment)
{
	char			*bundleID = "com.andescotia.frameworks.marten.bsd";
	Nat4			result = 0;

	result = load_MacOSX_UNIX_Constants(environment,bundleID);
	result = load_MacOSX_UNIX_Structures(environment,bundleID);
	result = load_MacOSX_UNIX_Procedures(environment,bundleID);

	return 0;
}

#pragma export off
