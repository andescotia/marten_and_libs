/*
	
	VPL_MacVPL.c
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif	

#include "MacVPL Interpreter.h"

Nat4	load_VPX_PrimitivesInterpreter(V_Environment environment,char *bundleID);

#pragma export on

Nat4	load_Interpreter(V_Environment environment)
{
	char	*bundleID = "com.andescotia.frameworks.marten.interpreter";
	Nat4	result = 0;

	result = load_VPX_PrimitivesInterpreter(environment,bundleID);

	return 0;
}

#pragma export off
