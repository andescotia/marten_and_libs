/*
	
	MacOSX LIBC.h
	Copyright 2005 Andescotia LLC, All Rights Reserved.
	
*/

#ifndef VPXMARTENLIBC
#define VPXMARTENLIBC

void	init_MartenLibC();
Nat4	load_LibC(V_Environment environment);

#endif
