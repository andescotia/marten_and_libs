/*
	
	MacOSX OpenGL.h
	Copyright 2007 Andescotia LLC, All Rights Reserved.
	
*/

#ifndef VPXMARTENOPENGL
#define VPXMARTENOPENGL

Nat4	load_OpenGL(V_Environment environment);

#endif
