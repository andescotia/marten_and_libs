/*
	
	MacVPL.h
	Copyright 2003 Scott B. Anderson, All Rights Reserved.
	
*/

#ifndef VPXCARBON
#define VPXCARBON

#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif

Nat4	load_Carbon(V_Environment environment);

#endif
