/*
 *  VPX_PrimitivesMacFiles.c
 *  PB_56
 *
 *  Created by scott on Sat May 11 2002.
 *  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
 *
 */
#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif	
#include "FSCopyObject.h"

#define		VPLUIModalWindowEncoding		kCFStringEncodingUTF8

Int4 VPLP_make_2D_file_2D_specification( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_make_2D_file_2D_specification( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	object = NULL;
	Int4		volume = 0;
	Int4		directory = 0;
	Int1		*fileName = 0;

    FSSpec		*theFileSpec = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("make-file-specification: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("make-file-specification: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kInteger) return record_error("make-file-specification: Input 1 not integer! - ",moduleName,kERROR,environment);
	volume = ((V_Integer) object)->value;

	object = VPLPrimGetInputObject(1,primitive,environment);
	if( object == NULL || object->type != kInteger) return record_error("make-file-specification: Input 2 not integer! - ",moduleName,kERROR,environment);
	directory = ((V_Integer) object)->value;

	object = VPLPrimGetInputObject(2,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("make-file-specification: Input 3 not string! - ",moduleName,kERROR,environment);
	fileName = ((V_String) object)->string;

	
	theFileSpec = (FSSpec *) X_malloc(sizeof (FSSpec));
	theFileSpec->vRefNum = volume;
	theFileSpec->parID = directory;
#ifdef TOOLBOX	
	strcpy((Int1 *) theFileSpec->name,fileName);
	c2pstr((Int1 *) theFileSpec->name);
#endif
#ifdef CARBON	
	CopyCStringToPascal(fileName,theFileSpec->name);
#endif
	ptrO = create_externalBlock("FSSpec",sizeof (FSSpec),environment);
	ptrO->blockPtr = (void *) theFileSpec;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_scan_2D_folder( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_scan_2D_folder( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object 	block = NULL;
	V_String	file = NULL;
	V_Integer	index = NULL;
	V_List		smallList = NULL;
	Int4		id = 0;
	Int4		volume = 0;
	Int4		directory = 0;
	Nat4		counter = 0;

	Nat4		stringLength = 0;
	Int1		*tempCString = NULL;
    
    CInfoPBRec	record;
    OSErr		result = 0;
    V_List		list = NULL;
    Str31		fileName;
    
	
	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("scan-folder: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("scan-folder: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("scan-folder: Input 1 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	volume = ((V_Integer) block)->value;
	
	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("scan-folder: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	directory = ((V_Integer) block)->value;
	
	(record.hFileInfo).ioVRefNum = 0;
	(record.hFileInfo).ioNamePtr = (unsigned char *) &fileName;
	
	counter = 0;
	while(result == 0){
		(record.hFileInfo).ioVRefNum = volume;
		(record.hFileInfo).ioDirID = directory;
		(record.hFileInfo).ioFDirIndex = ++counter;
	
		result = PBGetCatInfo(&record,kFALSE);
	}
	
	list = create_list(counter-1,environment);

	counter = 0;
	result = 0;
	while(result == 0){
		(record.hFileInfo).ioVRefNum = volume;
		(record.hFileInfo).ioDirID = directory;
		(record.hFileInfo).ioFDirIndex = ++counter;
	
		result = PBGetCatInfo(&record,FALSE);
	
		if(result == 0){
//			if( ((record.hFileInfo).ioFlFndfInfo).fdFlags bit 14 is TRUE --- skip)
	
#ifdef TOOLBOX	
			p2cstr((unsigned char *) &fileName);
			file = create_string((Int1 *) &fileName,environment);
            c2pstr((Int1 *) &fileName);
#endif
#ifdef CARBON	
                        stringLength = *((unsigned char *) &fileName);
                        tempCString = (Int1 *) X_malloc(stringLength+1);
                        CopyPascalStringToC((unsigned char *) &fileName,tempCString);
                        file = create_string( tempCString , environment);
                        X_free(tempCString);
#endif
			if( (record.hFileInfo).ioFlAttrib&16) id = (record.hFileInfo).ioDirID;
			else id = -1;

			index = int_to_integer(id,environment);
			smallList = create_list(2,environment);
		
			*(smallList->objectList+0) = (V_Object) file; /* Insert object */
			*(smallList->objectList+1) = (V_Object) index; /* Insert object */
		
			*(list->objectList + counter - 1) = (V_Object) smallList; /* Insert object */
		}
	}
	
	VPLPrimSetOutputObject(environment,primInArity,0,primitive, (V_Object) list);
	*trigger = kSuccess;

	return kNOERROR;
}

Int4 VPLP_extract_2D_file_2D_specification( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_extract_2D_file_2D_specification( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object		outputObject = NULL;

	V_Object 	block = NULL;

    FSSpec		*theFileSpec = NULL;
    
#ifdef CARBON	
	Nat4		stringLength = 0;
	Int1		*tempCString = NULL;
#endif

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("extract-file-specification: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 3) return record_error("extract-file-specification: Outarity not 3!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("extract-file-specification: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock)block)->name,"FSSpec") != 0) return record_error("extract-file-specification: Input 1 external not type FSSpec! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theFileSpec = (FSSpec *) ((V_ExternalBlock)block)->blockPtr;
	
	block = (V_Object) int_to_integer(theFileSpec->vRefNum,environment);
	outputObject = (V_Object) block;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputObject);
		
	block = (V_Object) int_to_integer(theFileSpec->parID,environment);
	outputObject = (V_Object) block;
	VPLPrimSetOutputObject(environment,primInArity,1,primitive,(V_Object) outputObject);
		
#ifdef TOOLBOX	
	p2cstr((unsigned char *) theFileSpec->name);
	block = (V_Object) create_string( (Int1 *) theFileSpec->name , environment);
	c2pstr((Int1 *) theFileSpec->name);
#endif
#ifdef CARBON	
	stringLength = *((unsigned char *) theFileSpec->name);
	tempCString = (Int1 *) X_malloc(stringLength+1);
	CopyPascalStringToC((unsigned char *) theFileSpec->name,tempCString);
	block = (V_Object) create_string( tempCString , environment);
	X_free(tempCString);
	tempCString = NULL;
#endif
	outputObject = (V_Object) block;
	VPLPrimSetOutputObject(environment,primInArity,2,primitive, (V_Object) outputObject);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_move_2D_file( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_move_2D_file( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    OSErr		err = 0;
	V_Object 	block = NULL;

    FSSpec		*theFileSpec = NULL;
    FSSpec		*destFileSpec = NULL;
	
	V_Integer	outputInt = 0;	
	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("move-file: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("move-file: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("move-file: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock)block)->name,"FSSpec") != 0) return record_error("move-file: Input 1 external not type FSSpec! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theFileSpec = (FSSpec *) ((V_ExternalBlock)block)->blockPtr;
	
	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("move-file: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock)block)->name,"FSSpec") != 0) return record_error("move-file: Input 1 external not type FSSpec! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	destFileSpec = (FSSpec *) ((V_ExternalBlock)block)->blockPtr;
	
	err = FSpCatMove(theFileSpec,destFileSpec);

	outputInt = int_to_integer(err,environment);
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputInt);

	*trigger = kSuccess;
	return kNOERROR;
}

Int4 VPLP_vpx_2D_folder_2D_fsspecs( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_vpx_2D_folder_2D_fsspecs( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    OSErr		err = noErr;
	V_Object 	block = NULL;
	V_ExternalBlock	ptrO = NULL;
	V_List		list = NULL;

	FSCatalogInfo		theCatInfo;
	FSSpec		*theFileSpec = NULL;
	FSRef		theFolderRef;
	FSIterator	theIterator;
	
	Nat4		numberOfItems = 0;
	Nat4		actualNumber = 0;
	Nat4		counter = 0;
	FSSpec		*theFSSpecArray = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("vpx-folder-fsspecs: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("vpx-folder-fsspecs: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
        
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("vpx-folder-fsspecs: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock)block)->name,"FSSpec") != 0) return record_error("vpx-folder-fsspecs: Input 1 external not type FSSpec! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theFileSpec = (FSSpec *) ((V_ExternalBlock)block)->blockPtr;
	
	if(theFileSpec != NULL){
	
		err = FSpMakeFSRef(theFileSpec,&theFolderRef);
	
		if(err == noErr) err = FSGetCatalogInfo(&theFolderRef,kFSCatInfoValence,&theCatInfo,NULL,NULL,NULL);
		if(err == noErr) {
			numberOfItems = theCatInfo.valence;
			theFSSpecArray = (FSSpec *) calloc(numberOfItems,sizeof(FSSpec));
		
			if(theFileSpec != NULL){
				if(err == noErr) err = FSOpenIterator(&theFolderRef,kFSIterateFlat,&theIterator);
				if(err == noErr) err = FSGetCatalogInfoBulk(theIterator,numberOfItems,&actualNumber,NULL,kFSCatInfoNone,NULL,NULL,theFSSpecArray,NULL);
				if(err == noErr) err = FSCloseIterator(theIterator);
			
				if(err == noErr) {
					list = create_list(actualNumber,environment);
					for(counter = 0;counter<actualNumber;counter++){
						ptrO = create_externalBlock("FSSpec",sizeof (FInfo),environment);
						theFileSpec = (FSSpec *) X_malloc(sizeof(FSSpec));
						memcpy(theFileSpec,&theFSSpecArray[counter],sizeof (FSSpec));
						ptrO->blockPtr = (void *) theFileSpec;
						*(list->objectList + counter) = (V_Object) ptrO;
					}
					VPLPrimSetOutputObject(environment,primInArity,0,primitive, (V_Object) list);
				}else{
					VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
				}
				X_free(theFSSpecArray);
				theFSSpecArray = NULL;	
			}else{
				VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
			}
		}else{
			VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
		}
	}else{
		VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
	}

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_vpx_2D_get_2D_folder( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_vpx_2D_get_2D_folder( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
#pragma unused(primOutArity)

	V_Object		outputObject = NULL;


    V_ExternalBlock	ptrO = NULL;
    FSSpec		*theFileSpec = NULL;
	FSSpec		tempSpec;

	OSErr	osError = noErr;
	NavReplyRecord	navReplyStruc;
	AEKeyword	theKeyword;
	DescType	actualType;
	Size		actualSize;
	V_Integer	outputInt = 0;	
        
	NavDialogOptions	navOptionsStruc;

	osError = NavGetDefaultDialogOptions(&navOptionsStruc);
	navOptionsStruc.dialogOptionFlags = navOptionsStruc.dialogOptionFlags | kNavSupportPackages;

	osError = NavChooseFolder(NULL,&navReplyStruc,&navOptionsStruc,NULL,NULL,NULL);
	if(osError == noErr && navReplyStruc.validRecord){
		theFileSpec = (FSSpec *) X_malloc(sizeof (FSSpec));
		 osError = AEGetNthPtr(&(navReplyStruc.selection),1,typeFSS,
			&theKeyword,&actualType,&tempSpec,sizeof(tempSpec),&actualSize);
		if(osError == noErr){
			FSMakeFSSpec(tempSpec.vRefNum,tempSpec.parID,tempSpec.name,theFileSpec);
			outputInt = int_to_integer(0,environment);
			VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputInt);

			ptrO = create_externalBlock("FSSpec",sizeof (FSSpec),environment);
			ptrO->blockPtr = (void *) theFileSpec;
			outputObject = (V_Object) ptrO;
			VPLPrimSetOutputObject(environment,primInArity,1,primitive,(V_Object) outputObject);
		}
		NavDisposeReply(&navReplyStruc);
	}
	if(osError == noErr){
		*trigger = kSuccess;
	}else{
		VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
		VPLPrimSetOutputObjectNULL(environment,primInArity,1,primitive);
		*trigger = kFailure;
	}

	return kNOERROR;
}

Int4 VPLP_copy_2D_object( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_copy_2D_object( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{

	V_Object	object = NULL;	
	Int1		*tempString = NULL;
	FSRef		*sourceRef = NULL;
	FSRef		*destRef = NULL;
	FSRef		newRef;
	FSSpec		newSpec;
	DupeAction	dupeAction;
	OSErr		osErr = noErr;

	Int1		*moduleName = NULL;
	Int1		*primName = "copy-object";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArity(environment,primName, primInArity, 3 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 0 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kExternalBlock,0,primName, primitive, environment );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kExternalBlock,1,primName, primitive, environment );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kString,2,primName, primitive, environment );
	if( result != kNOERROR ) return result;

#endif

	object = VPLPrimGetInputObject(0,primitive,environment);
	if( strcmp(((V_ExternalBlock)object)->name,"FSRef") != 0) return record_error("copy-object: Input 1 external not type FSRef! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	sourceRef = (FSRef *) ((V_ExternalBlock)object)->blockPtr;

	object = VPLPrimGetInputObject(1,primitive,environment);
	if( strcmp(((V_ExternalBlock)object)->name,"FSRef") != 0) return record_error("copy-object: Input 2 external not type FSRef! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	destRef = (FSRef *) ((V_ExternalBlock)object)->blockPtr;

	object = VPLPrimGetInputObject(2,primitive,environment);
	tempString = (Int1*)(((V_String) object)->string);
	
	if(strcmp(tempString,"Replace") == 0) dupeAction = kDupeActionReplace;
	else if(strcmp(tempString,"Standard") == 0) dupeAction = kDupeActionStandard;
	else if(strcmp(tempString,"Rename") == 0) dupeAction = kDupeActionRename;
	else dupeAction = kDupeActionReplace;
	
	osErr = FSCopyObject( sourceRef, destRef, 0, kFSCatInfoNone, 
							  dupeAction, NULL, true, false, NULL, NULL, &newRef, &newSpec);


	if(!osErr) *trigger = kSuccess;
	else *trigger = kFailure;
	return kNOERROR;
}

Int4 VPLP_delete_2D_object( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_delete_2D_object( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{

	FSRef		*sourceRef = NULL;
	OSErr		osErr = paramErr;

	Int1		*primName = "delete-object";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArity(environment,primName, primInArity, 1 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 1 );
	if( result != kNOERROR ) return result;

#endif

	result = VPLPrimGetInputObjectEBlockPtr( (vpl_BlockPtr*)&sourceRef, "FSRef", 0, primName, primitive, environment );
	if( result != kNOERROR ) return result;

	if( sourceRef ) osErr = FSDeleteObjects( sourceRef );

	VPLPrimSetOutputObjectInteger( environment, primInArity, 0, primitive, (vpl_Integer)osErr );

	*trigger = kSuccess;
	return kNOERROR;
}

Int4 VPLP_set_2D_alist_2D_pref( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* savalist */
Int4 VPLP_set_2D_alist_2D_pref( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* savealist */
{

	Nat4		length = 0;
	V_Object	string = NULL;
	V_Object	list = NULL;
	Int1		*tempString = "";

	V_Object	currentObject = NULL;
	FSRef		*convertFSR = NULL;
	AliasHandle tAliasHdl;
	CFMutableArrayRef	theStoreArray = NULL;
    CFDataRef 	tCFDataRef;

	OSStatus	err = noErr;

	CFStringRef	prefName = NULL;
	V_List		theInList = NULL;
	SInt32		numItems = 0;



	if(primInArity != 2) return record_error("saveAList: Input arity is not 2",NULL,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("saveAList: Output arity is not 0",NULL,kWRONGINARITY,environment);

	string = VPLPrimGetInputObject(0,primitive,environment);
	if( string == NULL || string->type != kString){
		record_error("saveAList: Input 1 type not string in module - ",NULL,kWRONGINPUTTYPE,environment);
		return kWRONGINPUTTYPE;
	} else {
		length = ((V_String) string)->length;
		tempString = ((V_String) string)->string;
	}

	prefName = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
//	CFShow( prefName );

	list = VPLPrimGetInputObject(1,primitive,environment);
	if( list == NULL ) {
		theInList = NULL;
		numItems = 0;
	} else if( list->type != kList ){
			record_error("saveAList: Input 2 type not list",NULL,kWRONGINPUTTYPE,environment);
			return kWRONGINPUTTYPE;
	} else {
		theInList = (V_List)list;
		numItems = theInList->listLength;
	}
	
	if( numItems > 0 ) {

		theStoreArray = CFArrayCreateMutable( NULL, 0, &kCFTypeArrayCallBacks );

		for( length=0;length<numItems;length++ ) {
		
			currentObject = *(theInList->objectList+length);
			if( VPLObjectGetType(currentObject) != kExternalBlock ) return record_error("saveAList: Element not FSRef in list!",NULL,kWRONGINPUTTYPE,environment);

			convertFSR = (FSRef*)VPLObjectGetExternalBlockPtr( currentObject );

	    	err = FSNewAlias(NULL, convertFSR, &tAliasHdl);
	    	if( err == noErr ) {
				    tCFDataRef = CFDataCreate(kCFAllocatorDefault,
	                              (UInt8*) *tAliasHdl,
	                              GetHandleSize((Handle) tAliasHdl));
					if( tCFDataRef != NULL ) {
						CFArrayAppendValue( theStoreArray, tCFDataRef );
						CFRelease( tCFDataRef );
					}
					DisposeHandle((Handle) tAliasHdl);
	    	} else {
//	    		printf("Item %d FSNewAlias error: %d\n", length, err);
	    	}
		}
//	    CFShow( theStoreArray );
	}
	
	CFPreferencesSetAppValue( prefName, theStoreArray, kCFPreferencesCurrentApplication);
	CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
	
	if( theStoreArray != NULL) CFRelease( theStoreArray );
	if( prefName != NULL) CFRelease( prefName );

	*trigger = kSuccess;	
	return kNOERROR;
}


Int4 VPLP_get_2D_alist_2D_pref( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* getAList */
Int4 VPLP_get_2D_alist_2D_pref( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* getAList */
{

	Nat4		length = 0;
	V_Object	string = NULL;
	Int1		*tempString = "";

	CFStringRef	prefName = NULL;
	CFArrayRef	prefArray = NULL;
	V_List		theOutList = NULL;
	SInt32		numItems = 0;
	CFIndex		dataSize;
	Boolean		wasChanged;
	V_ExternalBlock	ptrO = NULL;
	
	OSStatus	err;
	AliasHandle tAliasHdl;
	CFDataRef	tCFDataRef;
	FSRef		*outFSRef;

	if(primInArity != 1) return record_error("getAList: Input arity is not 1",NULL,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("getAList: Output arity is not 1",NULL,kWRONGINARITY,environment);

	string = VPLPrimGetInputObject(0,primitive,environment);
	if( string == NULL || string->type != kString){
		record_error("getAList: Input 1 type not string in module - ",NULL,kWRONGINPUTTYPE,environment);
		return kWRONGINPUTTYPE;
	} else {
		length = ((V_String) string)->length;
		tempString = ((V_String) string)->string;
	}

	prefName = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
//	CFShow( prefName );

	if( prefName != NULL ) {
		prefArray = (CFArrayRef) CFPreferencesCopyAppValue( prefName, kCFPreferencesCurrentApplication);
		CFRelease( prefName );
	}

	if( prefArray == NULL ){
		VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
	} else { 
//		CFShow( prefArray );
		
		numItems = CFArrayGetCount( prefArray );
		theOutList = create_list(numItems,environment); /* Create object, add to heap, set refcount */
		
		for( length = 0; length<numItems; length++ ){
			*(theOutList->objectList+length) = (V_Object)NULL;
			
			tCFDataRef = (CFDataRef) CFArrayGetValueAtIndex( prefArray, length );
			if( tCFDataRef != NULL ){
//				CFShow( tCFDataRef );
				
				dataSize = CFDataGetLength(tCFDataRef);
				tAliasHdl = (AliasHandle) NewHandle(dataSize);
				CFDataGetBytes(tCFDataRef, CFRangeMake(0, dataSize), (UInt8*) *tAliasHdl);
				
				outFSRef = (FSRef*)X_malloc( sizeof(FSRef) );
				err = FSResolveAlias(NULL, tAliasHdl, outFSRef, &wasChanged);
				if( err == noErr ) {
					ptrO = create_externalBlock("FSRef",sizeof(FSRef),environment);
					ptrO->blockPtr = (void*)outFSRef;
					ptrO->levelOfIndirection = 0;
					
					put_nth(environment, theOutList, length+1, (V_Object)ptrO);
					
					decrement_count( environment, (V_Object)ptrO );

					} else {
//						printf("Item %d FSResolveAlias error: %d\n", length, err);
						X_free( outFSRef );
					}
//				CFRelease( tCFDataRef );
				DisposeHandle( (Handle)tAliasHdl );
				}
			}
		VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) theOutList);
		CFRelease( prefArray );
		}

	*trigger = kSuccess;	
	return kNOERROR;
}



Nat4	loadConstants_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID);
Nat4	loadConstants_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadStructures_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID);
Nat4	loadStructures_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadProcedures_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID);
Nat4	loadProcedures_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadPrimitives_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID);
Nat4	loadPrimitives_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID)
{
        V_ExtPrimitive	result = NULL;
        V_Dictionary	dictionary = environment->externalPrimitivesTable;

        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"make-file-specification",dictionary,3,1,VPLP_make_2D_file_2D_specification)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"scan-folder",dictionary,2,1,VPLP_scan_2D_folder)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"extract-file-specification",dictionary,1,3,VPLP_extract_2D_file_2D_specification)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"move-file",dictionary,2,1,VPLP_move_2D_file)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"vpx-folder-fsspecs",dictionary,1,1,VPLP_vpx_2D_folder_2D_fsspecs)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kNextCaseOnFailure,"vpx-get-folder",dictionary,0,2,VPLP_vpx_2D_get_2D_folder)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kNextCaseOnFailure,"copy-object",dictionary,3,0,VPLP_copy_2D_object)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kNextCaseOnFailure,"delete-object",dictionary,1,1,VPLP_delete_2D_object)) == NULL) return kERROR;

        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"set-alist-pref",dictionary,2,0,VPLP_set_2D_alist_2D_pref)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"get-alist-pref",dictionary,1,1,VPLP_get_2D_alist_2D_pref)) == NULL) return kERROR;

        return kNOERROR;

}

Nat4	load_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID);
Nat4	load_VPX_PrimitivesMacFiles(V_Environment environment,char *bundleID)
{
		Nat4	result = 0;
		
		result = loadConstants_VPX_PrimitivesMacFiles(environment,bundleID);
		result = loadStructures_VPX_PrimitivesMacFiles(environment,bundleID);
		result = loadProcedures_VPX_PrimitivesMacFiles(environment,bundleID);
		result = loadPrimitives_VPX_PrimitivesMacFiles(environment,bundleID);
		
		return result;
}

