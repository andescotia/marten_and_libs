/*
	
	VPL_Primitives.c
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif	

Int1*	ExStringToInString (Int1* exString);				/* Converts an 8 bit (ASCII) string into an escaped c string */
Int1*	InStringToExString (Int1* inString);				/* Converts an escaped c string into an 8 bit (ASCII) string */

Int1*	ExStringToCNameString (Int1* exString);				/* Converts an 8 bit (ASCII) string into an encoded c name string */
Int1*	CNameStringToExString (Int1*	inString);			/* Converts an encoded c name string into an 8 bit (ASCII) string */

Int1*	ExStringToCNameStringDec (Int1* exString);			/* Converts an 8 bit (ASCII) string into an encoded c name string */
Int1*	CNameStringDecToExString (Int1*	inString);			/* Converts an encoded c name string into an 8 bit (ASCII) string */

Int1*	ExStringToInString (Int1* exString)					/* Converts an 8 bit (ASCII) string into an escaped c string */
{

	Nat1	testChar = 0;
	Int1*	tempString = NULL;
	Int1*	inString = NULL;
	Nat4	theIndex = 0;
	Nat4	theLength = 0;
	Nat4	theSize = 0;
	
	if (exString == NULL) return NULL;
	if (strlen(exString) == 0) return NULL;
	
	
//	printf( "The encoding: %X\n", GetApplicationTextEncoding() );
//	printf( "The locale: %s\n", setlocale( LC_ALL, NULL) );
	
	tempString = (Int1*)calloc(5, 1);
	inString = (Int1*)calloc(1, 1);
	
	for (theIndex = 0; theIndex < strlen(exString); theIndex++)
	{
		testChar = exString[theIndex];
		
		if( isgraph(testChar) )
		{
			switch(testChar) {
				case 34:
 					sprintf( tempString, "\\\"");	/* double quote */
 					break;
				case 39:
 					sprintf( tempString, "\\\'");	/* single quote */
 					break;
				case 92:
 					sprintf( tempString, "\\\\");	/* backslash */
 					break;

				default:							/* every other printable character */
					sprintf( tempString, "%c", testChar);
					break;
				}
		} 
		else if( isspace(testChar) )
		{
			switch(testChar) {
				case 9:
 					sprintf( tempString, "\\t");	/* tab */
 					break;
				case 10:
 					sprintf( tempString, "\\n");	/* new-line */
 					break;
				case 11:
 					sprintf( tempString, "\\v");	/* vertical tab */
 					break;
				case 12:
 					sprintf( tempString, "\\f");	/* form-feed */
 					break;
				case 13:
 					sprintf( tempString, "\\r");	/* carriage return */
 					break;
 				case 32:
 					sprintf( tempString, " ");	 	/* space */
 					break;
			}
		}
		else										/* regular char */
		{
			switch(testChar) {
				case 7:
 					sprintf( tempString, "\\a");	/* bell */
 					break;
				default:							/* hexadecimal */
					sprintf( tempString, "\\x%02X", testChar );
					break;
				}
		}
	
		theLength = strlen( inString );
		theSize = strlen( tempString );
		theLength = theLength + theSize + 1;
		
		inString = (Int1*)realloc( inString, theLength*sizeof(Int1) );
		if( inString == NULL ) goto BAIL;
		inString = strcat( inString, tempString );	
	}

BAIL:	
	X_free(tempString );
	return inString;

} /* ExStringToInString */

Int1*	InStringToExString (Int1*	inString)			/* Converts an escaped c string into an 8 bit (ASCII) string */
{
	Nat1	testChar = 0;
	Int1*	tempString = NULL;
	Int1*	exString = NULL;
	Nat4	theIndex = 0;
	Nat4	theLength = 0;
	Nat4	theSize = 0;
	long 	templng = 0;
	
	if (inString == NULL) return NULL;
	if (strlen(inString) == 0) return NULL;
	
	tempString = (Int1*)calloc(5, 1);
	exString = (Int1*)calloc(1, 1);
	
	for (theIndex = 0; theIndex < strlen(inString); theIndex++)
	{
		testChar = inString[theIndex];
		
		if( testChar == '\\') {
			theIndex++;
			testChar = inString[theIndex];
			switch(testChar) {
				case 'x':						/* hexadecimal */
					theIndex++;
					tempString[0] = inString[theIndex];
					theIndex++;
					tempString[1] = inString[theIndex];
					tempString[2] = 0;
					
					templng = 	strtol( tempString, NULL, 16 );
					testChar = (Nat1) templng;
					break;
				case 'a':
					testChar = 7;				/* bell */
					break;
				case 't':
					testChar = 9;				/* tab */
					break;
				case 'n':
					testChar = 10;				/* new-line */
					break;
				case 'v':
					testChar = 11;				/* vertical tab */
					break;
				case 'f':
					testChar = 12;				/* form-feed */
					break;
				case 'r':
					testChar = 13;				/* carriage return */
					break;
				case '\"':
					testChar = 34;				/* double quote */
					break;
				case '\'':
					testChar = 39;				/* single quote */
					break;
				case '\\':
					testChar = 92;				/* backslash */
					break;
				default:						/* regular char */
					break;
				}
			}
			
		sprintf( tempString, "%c", testChar);
				
		theLength = strlen( exString );
		theSize = strlen( tempString );
		theLength = theLength + theSize + 1;
		
		exString = (Int1*)realloc( exString, theLength*sizeof(Int1) );
		if( exString == NULL ) goto BAIL;
		exString = strcat( exString, tempString );					
		}
		
BAIL:
	X_free(tempString );
	return exString;

} /* InStringToExString */


Int1*	ExStringToCNameString (Int1* exString)		/* Converts an 8 bit (ASCII) string into an encoded c name string */
{

	Nat1	testChar = 0;
	Int1*	tempString = NULL;
	Int1*	inString = NULL;
	Nat4	theIndex = 0;
	Nat4	theLength = 0;
	Nat4	theSize = 0;

	Nat4	theOffset = 0;
	
	if (exString == NULL) return NULL;
	if (strlen(exString) == 0) return NULL;
	
	tempString = (Int1*)calloc(5, 1);
	inString = (Int1*)calloc(1, 1);

	for (theIndex = 0; theIndex < strlen(exString); theIndex++)
	{
		testChar = exString[theIndex];
		
		if( !isalnum(testChar) )					/* pack hexadecimal */
		{
			theOffset = theLength - 2;
			if( (theLength != 0) && (inString[theOffset] == '_') ) {
				theLength = theLength - 1;
				inString = (Int1*)realloc( inString, theLength*sizeof(Int1) );
				if( inString == NULL ) goto BAIL;
				inString[theLength - 1] = 0;
				
				sprintf( tempString, "%02X_", testChar );
			}
			else {
				sprintf( tempString, "_%02X_", testChar );
			}
		}
		else										/* regular char */
		{
			sprintf( tempString, "%c", testChar);
		}
	
		theLength = strlen( inString );
	
		theSize = strlen( tempString );
		theLength = theLength + theSize + 1;
		
		inString = (Int1*)realloc( inString, theLength*sizeof(Int1) );
		if( inString == NULL ) goto BAIL;
		inString = strcat( inString, tempString );	
	}

BAIL:	
	X_free(tempString );
	return inString;

} /* ExStringToCNameString */


Int1*	CNameStringToExString (Int1*	inString)			/* Converts an encoded c name string into an 8 bit (ASCII) string */
{
	Nat1	testChar = 0;
	Int1*	tempString = NULL;
	Int1*	exString = NULL;
	Nat4	theIndex = 0;
	Nat4	theLength = 0;
	Nat4	theSize = 0;
	long 	templng = 0;
	Bool	inHexMode = kFALSE;
	
	if (inString == NULL) return NULL;
	if (strlen(inString) == 0) return NULL;
	
	tempString = (Int1*)calloc(5, 1);
	exString = (Int1*)calloc(1, 1);
	
	for (theIndex = 0; theIndex < strlen(inString); theIndex++)
	{
		testChar = inString[theIndex];
		
		if( testChar == '_') {
			if( !inHexMode ) {
				inHexMode = kTRUE;
				continue;
			}
			else {
				inHexMode = kFALSE;
				continue;
			}
		}
		else {
			if( inHexMode == kTRUE ) {
				tempString[0] = inString[theIndex];	/* hexadecimal */
				theIndex++;
				tempString[1] = inString[theIndex];
				tempString[2] = 0;
					
				templng = strtol( tempString, NULL, 16 );
				testChar = (Nat1) templng;
			}
		}
				
		sprintf( tempString, "%c", testChar);
				
		theLength = strlen( exString );
		theSize = strlen( tempString );
		theLength = theLength + theSize + 1;
		
		exString = (Int1*)realloc( exString, theLength*sizeof(Int1) );
		if( exString == NULL ) goto EXIT;
		exString = strcat( exString, tempString );					
		}
		
EXIT:
	X_free(tempString );
	return exString;

} /* CNameStringToExString */


Int1*	ExStringToCNameStringDec (Int1* exString)		/* Converts an 8 bit (ASCII) string into an encoded c name string */
{

	Nat1	testChar = 0;
	Int1*	tempString = NULL;
	Int1*	inString = NULL;
	Nat4	theIndex = 0;
	Nat4	theLength = 0;
	Nat4	theSize = 0;

	if (exString == NULL) return NULL;
	if (strlen(exString) == 0) return NULL;
	
	tempString = (Int1*)calloc(5, 1);
	inString = (Int1*)calloc(1, 1);

	for (theIndex = 0; theIndex < strlen(exString); theIndex++)
	{
		testChar = exString[theIndex];
		
		if( !isalnum(testChar) )					/* decimal */
		{
			sprintf( tempString, "_%02d_", testChar );
		}
		else										/* regular char */
		{
			sprintf( tempString, "%c", testChar);
		}
	
		theLength = strlen( inString );
	
		theSize = strlen( tempString );
		theLength = theLength + theSize + 1;
		
		inString = (Int1*)realloc( inString, theLength*sizeof(Int1) );
		if( inString == NULL ) goto BAIL;
		inString = strcat( inString, tempString );	
	}

BAIL:	
	X_free(tempString );
	return inString;

} /* ExStringToCNameStringDec */

Int1*	CNameStringDecToExString (Int1*	inString)			/* Converts an encoded c name string into an 8 bit (ASCII) string */
{
	Nat1	testChar = 0;
	Int1*	tempString = NULL;
	Int1*	exString = NULL;
	Nat4	theIndex = 0;
	Nat4	theLength = 0;
	Nat4	theSize = 0;
	long 	templng = 0;
	
	if (inString == NULL) return NULL;
	if (strlen(inString) == 0) return NULL;
	
	tempString = (Int1*)calloc(5, 1);
	exString = (Int1*)calloc(1, 1);
	
	for (theIndex = 0; theIndex < strlen(inString); theIndex++)
	{
		testChar = inString[theIndex];
		
		if( testChar == '_') {
			theIndex++;
			tempString[0] = inString[theIndex];	/* decimal */
			theIndex++;
			tempString[1] = inString[theIndex];
			if( inString[theIndex+1] != '_' ) {
				theIndex++;
				tempString[2] = inString[theIndex];
			} else {
				tempString[2] = 0;
			}

			templng = strtol( tempString, NULL, 10 );
			testChar = (Nat1) templng;
			theIndex++;
		}
				
		sprintf( tempString, "%c", testChar);
				
		theLength = strlen( exString );
		theSize = strlen( tempString );
		theLength = theLength + theSize + 1;
		
		exString = (Int1*)realloc( exString, theLength*sizeof(Int1) );
		if( exString == NULL ) goto EXIT;
		exString = strcat( exString, tempString );					
	}
		
EXIT:
	X_free(tempString );
	return exString;

} /* CNameStringDecToExString */




/* Primitive Interface */

Int4 VPLP_exstring_2D_to_2D_instring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* exstring-to-instring */
Int4 VPLP_exstring_2D_to_2D_instring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* exstring-to-instring */
{

	V_Object	object = NULL;
	Int1		*tempString = NULL;
	Int1		*transString = NULL;
	V_String	theString = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("exstring-to-instring: Inarity is not 1 in module - ",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("exstring-to-instring: Outarity is not 1 in module - ",moduleName,kWRONGOUTARITY,environment);

	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("exstring-to-instring: Input 1 type not string in module - ",moduleName,kWRONGINPUTTYPE,environment);
	tempString = new_string( ((V_String) object)->string , environment );

	transString = ExStringToInString( tempString );
	
	if( transString == NULL ) {
			X_free(tempString);
			VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
			goto EXIT;
			}
	
	theString = create_string( transString,environment);
	X_free(tempString);
	X_free(transString);

	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) theString);

EXIT:
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_instring_2D_to_2D_exstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* instring-to-exstring */
Int4 VPLP_instring_2D_to_2D_exstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* instring-to-exstring */
{

	V_Object	object = NULL;
	Int1		*tempString = NULL;
	Int1		*transString = NULL;
	V_String	theString = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("instring-to-exstring: Inarity is not 1 in module - ",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("instring-to-exstring: Outarity is not 1 in module - ",moduleName,kWRONGOUTARITY,environment);

	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("instring-to-exstring: Input 1 type not string in module - ",moduleName,kWRONGINPUTTYPE,environment);
	tempString = new_string( ((V_String) object)->string , environment );

	transString = InStringToExString( tempString );
	
	if( transString == NULL ) {
			X_free(tempString);
			VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
			goto EXIT;
			}
	
	theString = create_string( transString,environment);
	X_free(tempString);
	X_free(transString);

	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) theString);

EXIT:
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_exstring_2D_to_2D_cnstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* exstring-to-cnstring */
Int4 VPLP_exstring_2D_to_2D_cnstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* exstring-to-cnstring */
{

	V_Object	object = NULL;
	Int1		*tempString = NULL;
	Int1		*transString = NULL;
	V_String	theString = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("exstring-to-cnstring: Inarity is not 1 in module - ",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("exstring-to-cnstring: Outarity is not 1 in module - ",moduleName,kWRONGOUTARITY,environment);

	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("exstring-to-cnstring: Input 1 type not string in module - ",moduleName,kWRONGINPUTTYPE,environment);
	tempString = new_string( ((V_String) object)->string , environment );

	transString = ExStringToCNameString( tempString );			//the new way
	
	if( transString == NULL ) {
			X_free(tempString);
			VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
			goto EXIT;
			}
	
	theString = create_string( transString,environment);
	X_free(tempString);
	X_free(transString);

	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) theString);

EXIT:
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_cnstring_2D_to_2D_exstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* cnstring-to-exstring */
Int4 VPLP_cnstring_2D_to_2D_exstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* cnstring-to-exstring */
{

	V_Object	object = NULL;
	Int1		*tempString = NULL;
	Int1		*transString = NULL;
	V_String	theString = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("cnstring-to-exstring: Inarity is not 1 in module - ",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("cnstring-to-exstring: Outarity is not 1 in module - ",moduleName,kWRONGOUTARITY,environment);

	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("cnstring-to-exstring: Input 1 type not string in module - ",moduleName,kWRONGINPUTTYPE,environment);
	tempString = new_string( ((V_String) object)->string , environment );

	transString = CNameStringToExString( tempString );			//the new way
	
	if( transString == NULL ) {
			X_free(tempString);
			VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
			goto EXIT;
			}
	
	theString = create_string( transString,environment);
	X_free(tempString);
	X_free(transString);

	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) theString);

EXIT:
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_exstring_2D_to_2D_cnstringdec( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* exstring-to-cnstringdec */
Int4 VPLP_exstring_2D_to_2D_cnstringdec( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* exstring-to-cnstringdec */
{

	V_Object	object = NULL;
	Int1		*tempString = NULL;
	Int1		*transString = NULL;
	V_String	theString = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("exstring-to-cnstringdec: Inarity is not 1 in module - ",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("exstring-to-cnstringdec: Outarity is not 1 in module - ",moduleName,kWRONGOUTARITY,environment);

	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("exstring-to-cnstringdec: Input 1 type not string in module - ",moduleName,kWRONGINPUTTYPE,environment);
	tempString = new_string( ((V_String) object)->string , environment );

	transString = ExStringToCNameStringDec( tempString );		//the old way
	
	if( transString == NULL ) {
			X_free(tempString);
			VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
			goto EXIT;
			}
	
	theString = create_string( transString,environment);
	X_free(tempString);
	X_free(transString);

	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) theString);

EXIT:
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_cnstringdec_2D_to_2D_exstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* cnstringdec-to-exstring */
Int4 VPLP_cnstringdec_2D_to_2D_exstring( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* cnstringdec-to-exstring */
{

	V_Object	object = NULL;
	Int1		*tempString = NULL;
	Int1		*transString = NULL;
	V_String	theString = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("cnstringdec-to-exstring: Inarity is not 1 in module - ",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("cnstringdec-to-exstring: Outarity is not 1 in module - ",moduleName,kWRONGOUTARITY,environment);

	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("cnstringdec-to-exstring: Input 1 type not string in module - ",moduleName,kWRONGINPUTTYPE,environment);
	tempString = new_string( ((V_String) object)->string , environment );

	transString = CNameStringDecToExString( tempString );		//the old way
	
	if( transString == NULL ) {
			X_free(tempString);
			VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
			goto EXIT;
			}
	
	theString = create_string( transString,environment);
	X_free(tempString);
	X_free(transString);

	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) theString);

EXIT:
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP__22_in_2D_relative_22_( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* "in" */
Int4 VPLP__22_in_2D_relative_22_( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* "in" */
{

	Nat4		length = 0;
	Int4		offset = 0;
	V_Object	object = NULL;	
	Int1		*tempString = NULL;
	Int1		*tempAString = NULL;
	Int1		*tempCString = NULL;

	Int1		*moduleName = NULL;
	Int1		*primName = "\"in-relative\"";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArityMin(environment,primName, primInArity, 2 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputArityMax(environment,primName, primInArity, 3 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 1 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kString,0,primName, primitive, environment );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kString,1,primName, primitive, environment );
	if( result != kNOERROR ) return result;

	switch(primInArity){
	
		case 3:
			result = VPLPrimCheckInputObjectType( kInteger,2,primName, primitive, environment );
			if( result != kNOERROR ) return result;
			break;

		default:
			break;
	}
#endif

	object = VPLPrimGetInputObject(0,primitive,environment);
	tempString = (Int1*)(((V_String) object)->string);

	object = VPLPrimGetInputObject(1,primitive,environment);
	tempAString = (Int1*)(((V_String) object)->string);
	
	switch(primInArity){
	
		case 3:
			object = VPLPrimGetInputObject(2,primitive,environment);
			offset = ((V_Integer) object)->value - 1;
			if(offset < 0) return record_error("\"in\": Input 3 less than 0 in module - ",moduleName,kWRONGINPUTTYPE,environment);
			if(strlen(tempString) <= offset) return record_error("\"in\": Input 3 greater than length in module - ",moduleName,kWRONGINPUTTYPE,environment);
			tempString = (Int1 *) (tempString + offset);
			break;

		default:
			break;
	}


	tempCString = strstr(tempString,tempAString);
	if(tempCString != NULL) length = 1 + (tempCString - tempString);
	else length = 0;

	VPLPrimSetOutputObjectInteger(environment,primInArity,0,primitive, length);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Nat4	loadConstants_VPX_PrimitivesCString(V_Environment environment,char *bundleID);
Nat4	loadConstants_VPX_PrimitivesCString(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadStructures_VPX_PrimitivesCString(V_Environment environment,char *bundleID);
Nat4	loadStructures_VPX_PrimitivesCString(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadProcedures_VPX_PrimitivesCString(V_Environment environment,char *bundleID);
Nat4	loadProcedures_VPX_PrimitivesCString(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)

        return kNOERROR;

}

Nat4	loadPrimitives_VPX_PrimitivesCString(V_Environment environment,char *bundleID);
Nat4	loadPrimitives_VPX_PrimitivesCString(V_Environment environment,char *bundleID)
{
        V_ExtPrimitive	result = NULL;
        V_Dictionary	dictionary = environment->externalPrimitivesTable;

        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"exstring-to-instring",dictionary,1,1,VPLP_exstring_2D_to_2D_instring)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"instring-to-exstring",dictionary,1,1,VPLP_instring_2D_to_2D_exstring)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"exstring-to-cnstring",dictionary,1,1,VPLP_exstring_2D_to_2D_cnstring)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"cnstring-to-exstring",dictionary,1,1,VPLP_cnstring_2D_to_2D_exstring)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"exstring-to-cnstringdec",dictionary,1,1,VPLP_exstring_2D_to_2D_cnstringdec)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"cnstringdec-to-exstring",dictionary,1,1,VPLP_cnstringdec_2D_to_2D_exstring)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"\"in-relative\"",dictionary,2,1,VPLP__22_in_2D_relative_22_)) == NULL) return kERROR;

        return kNOERROR;

}

Nat4	load_VPX_PrimitivesCString(V_Environment environment,char *bundleID);
Nat4	load_VPX_PrimitivesCString(V_Environment environment,char *bundleID)
{
		Nat4	result = 0;
		
		result = loadConstants_VPX_PrimitivesCString(environment,bundleID);
		result = loadStructures_VPX_PrimitivesCString(environment,bundleID);
		result = loadProcedures_VPX_PrimitivesCString(environment,bundleID);
		result = loadPrimitives_VPX_PrimitivesCString(environment,bundleID);
		
		return result;
}


