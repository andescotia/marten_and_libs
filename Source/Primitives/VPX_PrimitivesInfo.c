/*
	
	VPX_PrimitivesInfo.c
	Copyright 2006 Scott B. Anderson, All Rights Reserved.
	
*/
#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif	

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "VPX_PrimitiveInfo.h"

typedef struct 
{
 UInt32			button;
 CFStringRef	string;
 WindowRef		window;
} VPLUIModalWindowInfo;

//V_List		VPLUISelectList = NULL;	

#define		VPLUIModalWindowEncoding		kCFStringEncodingUTF8

#define		VPLUIModalWindowVersion			CFSTR("Version %@");
#define		VPLUIModalWindowVersionBuild	CFSTR("Version %@ (Build %@)");

#define		VPLUIBundleIDFramework			CFSTR("com.andescotia.frameworks.marten.info")
#define		VPLUIBundleIDBundle				CFSTR("com.andescotia.bundles.marten.info")

#define		VPLUIURLToAndescotiaHome		CFSTR("http://www.andescotia.com/")
#define		VPLUIURLToAndescotiaSale		CFSTR("https://andescotia.com/store/")

CFStringRef GetRelativeInfoDictionaryKey( CFStringRef theKeyName );
CFStringRef GetRelativeInfoDictionaryKey( CFStringRef theKeyName )
{
	CFBundleRef	thisBundle = NULL;
	CFStringRef	returnString = NULL;

	thisBundle = CFBundleGetMainBundle();		// First look in main bundle
	if( thisBundle != NULL )	returnString = CFBundleGetValueForInfoDictionaryKey( thisBundle, theKeyName );

	if( returnString == NULL ) {				// Second look in framework bundle
		thisBundle = CFBundleGetBundleWithIdentifier( VPLUIBundleIDFramework );
		if( thisBundle != NULL )	returnString = CFBundleGetValueForInfoDictionaryKey( thisBundle, theKeyName );
	}
	
	if( returnString != NULL ) CFRetain( returnString );
	
	return returnString;
}

Int4 VPLP_marten_2D_info( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* marten-info */
Int4 VPLP_marten_2D_info( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* marten-info */
{

	V_Object	string = NULL;	

	CFStringRef	displayString = NULL;
	CFStringRef	versionString = NULL;
	CFStringRef	versString = NULL;
	CFStringRef	buildString = NULL;
	CFStringRef	bannerString = NULL;
	Bool		hasPurchase = false;

	Int1		*primName = "marten-info";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
//	result = VPLPrimCheckInputArityMin(environment,primName, primInArity, 1 );
//	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 0 );
	if( result != kNOERROR ) return result;

#endif

	if( primInArity > 0 ) {
		string = VPLPrimGetInputObject(0,primitive,environment);
		if( string->type == kString) bannerString = CFStringCreateWithCString( kCFAllocatorDefault, ((V_String) string)->string, VPLUIModalWindowEncoding );
	}
		
	if( primInArity > 1 ) {
		string = VPLPrimGetInputObject(1,primitive,environment);
		if( string->type == kString) displayString = CFStringCreateWithCString( kCFAllocatorDefault, ((V_String) string)->string, VPLUIModalWindowEncoding );
	}	
	if( displayString == NULL ) {
			versString =	GetRelativeInfoDictionaryKey( CFSTR("CFBundleVersion") );
			buildString =	GetRelativeInfoDictionaryKey( CFSTR("MartenInterpreterVersion") );
			if( buildString && versString )	displayString =	CFStringCreateWithFormat( kCFAllocatorDefault, NULL, CFSTR("Version %@  Build %@"), versString, buildString );
			else if( versString )			displayString =	CFStringCreateWithFormat( kCFAllocatorDefault, NULL, CFSTR("Version %@"), versString );
			else							displayString = CFSTR("");
	}
	
	if( primInArity > 2 ) {
		string = VPLPrimGetInputObject(2,primitive,environment);
		if( string->type == kString) versionString = CFStringCreateWithCString( kCFAllocatorDefault, ((V_String) string)->string, VPLUIModalWindowEncoding );
	} 
	if( versionString == NULL) versionString = GetRelativeInfoDictionaryKey( CFSTR("MartenInfoCopyright") );

	if( primInArity > 3 ) {
		VPLPrimGetInputObjectBool( &hasPurchase, 3, primName, primitive, environment );
	} else hasPurchase = false;

	showInfoModalDialog(bannerString, displayString, versionString, hasPurchase);
    
	if(displayString) CFRelease( displayString );
	if(bannerString) CFRelease( bannerString );
	if(versionString) CFRelease( versionString );

	*trigger = kSuccess;	
	return kNOERROR;
}





Nat4	loadConstants_VPX_PrimitivesInfo(V_Environment environment,char *bundleID);
Nat4	loadConstants_VPX_PrimitivesInfo(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadStructures_VPX_PrimitivesInfo(V_Environment environment,char *bundleID);
Nat4	loadStructures_VPX_PrimitivesInfo(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadProcedures_VPX_PrimitivesInfo(V_Environment environment,char *bundleID);
Nat4	loadProcedures_VPX_PrimitivesInfo(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadPrimitives_VPX_PrimitivesInfo(V_Environment environment,char *bundleID);
Nat4	loadPrimitives_VPX_PrimitivesInfo(V_Environment environment,char *bundleID)
{
        V_ExtPrimitive	result = NULL;
        V_Dictionary	dictionary = environment->externalPrimitivesTable;
		
		if(dictionary){
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"marten-info",dictionary,1,0,VPLP_marten_2D_info)) == NULL) return kERROR;
		}
		
        return kNOERROR;

}

Nat4	load_VPX_PrimitivesInfo(V_Environment environment,char *bundleID);
Nat4	load_VPX_PrimitivesInfo(V_Environment environment,char *bundleID)
{
		Nat4	result = 0;
		
		result = loadConstants_VPX_PrimitivesInfo(environment,bundleID);
		result = loadStructures_VPX_PrimitivesInfo(environment,bundleID);
		result = loadProcedures_VPX_PrimitivesInfo(environment,bundleID);
		result = loadPrimitives_VPX_PrimitivesInfo(environment,bundleID);
		
		return result;
}

