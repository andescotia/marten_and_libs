/*
 *  VPX_PrimitivesMacDrawing.c
 *  PB_56
 *
 *  Created by scott on Sat May 11 2002.
 *  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
 *
 */
#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif	

#ifdef CARBON	
#include <Carbon/Carbon.h>
#endif

#ifdef TOOLBOX	
#include <Types.h>
#include <Memory.h>
#include <Quickdraw.h>
#include <Fonts.h>
#include <Events.h>
#include <Menus.h>
#include <Windows.h>
#include <TextEdit.h>
#include <Dialogs.h>
#include <OSUtils.h>
#include <ToolUtils.h>
#include <SegLoad.h>
#include <Sound.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#endif


Int4 VPLP_move_2D_to( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_move_2D_to( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	top = NULL;
	V_Object	left = NULL;
	Int4		topValue = 0;
	Int4		leftValue = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("move-to: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("move-to: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	left = VPLPrimGetInputObject(0,primitive,environment);
	if( left == NULL || left->type != kInteger) return record_error("move-to: Input 1 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	top = VPLPrimGetInputObject(1,primitive,environment);
	if( top == NULL || top->type != kInteger) return record_error("move-to: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	topValue = ((V_Integer) top)->value;
	leftValue = ((V_Integer) left)->value;
	
	MoveTo(leftValue,topValue);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_line_2D_to( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_line_2D_to( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	top = NULL;
	V_Object	left = NULL;
	Int4		topValue = 0;
	Int4		leftValue = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("line-to: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("line-to: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	left = VPLPrimGetInputObject(0,primitive,environment);
	if( left == NULL || left->type != kInteger) return record_error("line-to: Input 1 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	top = VPLPrimGetInputObject(1,primitive,environment);
	if( top == NULL || top->type != kInteger) return record_error("line-to: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	topValue = ((V_Integer) top)->value;
	leftValue = ((V_Integer) left)->value;
	
	LineTo(leftValue,topValue);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_ints_2D_to_2D_Point( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_ints_2D_to_2D_Point( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	top = NULL;
	V_Object	left = NULL;
	Int4		topValue = 0;
	Int4		leftValue = 0;

    Point		*thePoint = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("ints-to-Point: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("ints-to-Point: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	top = VPLPrimGetInputObject(0,primitive,environment);
	if( top == NULL || top->type != kInteger) return record_error("ints-to-Point: Input 1 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	left = VPLPrimGetInputObject(1,primitive,environment);
	if( left == NULL || left->type != kInteger) return record_error("ints-to-Point: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	topValue = ((V_Integer) top)->value;
	leftValue = ((V_Integer) left)->value;
	
	thePoint = (Point *) X_malloc(sizeof (Point));
	SetPt(thePoint,leftValue,topValue);

	ptrO = create_externalBlock("Point",sizeof (Point),environment);
	ptrO->blockPtr = (void *) thePoint;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_ints_2D_to_2D_Rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_ints_2D_to_2D_Rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	top = NULL;
	V_Object	left = NULL;
	V_Object	bottom = NULL;
	V_Object	right = NULL;
	Int4		topValue = 0;
	Int4		leftValue = 0;
	Int4		bottomValue = 0;
	Int4		rightValue = 0;

    Rect		*theRect = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 4) return record_error("ints-to-Rect: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("ints-to-Rect: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	top = VPLPrimGetInputObject(0,primitive,environment);
	if( top == NULL || top->type != kInteger) return record_error("ints-to-Rect: Input 1 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	left = VPLPrimGetInputObject(1,primitive,environment);
	if( left == NULL || left->type != kInteger) return record_error("ints-to-Rect: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	bottom = VPLPrimGetInputObject(2,primitive,environment);
	if( bottom == NULL || bottom->type != kInteger) return record_error("ints-to-Rect: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	right = VPLPrimGetInputObject(3,primitive,environment);
	if( right == NULL || right->type != kInteger) return record_error("ints-to-Rect: Input 4 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	topValue = ((V_Integer) top)->value;
	leftValue = ((V_Integer) left)->value;
	bottomValue = ((V_Integer) bottom)->value;
	rightValue = ((V_Integer) right)->value;
	
	theRect = (Rect *) X_malloc(sizeof (Rect));
	SetRect(theRect,leftValue,topValue,rightValue,bottomValue);

	ptrO = create_externalBlock("Rect",sizeof (Rect),environment);
	ptrO->blockPtr = (void *) theRect;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_draw_2D_string( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_draw_2D_string( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	string = NULL;
	Int1		*stringValue = 0;
#ifdef CARBON	
     Str255		pascalString;
#endif

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("draw-string: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("draw-string: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	string = VPLPrimGetInputObject(0,primitive,environment);
	if( string == NULL || string->type != kString) return record_error("draw-string: Input 1 type not string! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	stringValue = ((V_String)string)->string;
#ifdef TOOLBOX	
	c2pstr(stringValue);
	DrawString((const unsigned char *) stringValue);
	p2cstr((unsigned char *)stringValue);
#endif
#ifdef CARBON	
	CopyCStringToPascal(stringValue,pascalString);
	DrawString(pascalString);
#endif
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_string_2D_width( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_string_2D_width( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	string = NULL;
	Int1		*stringValue = 0;
	Nat4		stringWidth = 0;
	Nat4		stringLength = 0;
	Nat4		counter = 0;

	V_Integer	outputInt = 0;	
	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("string-width: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("string-width: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	string = VPLPrimGetInputObject(0,primitive,environment);
	if( string == NULL || string->type != kString) return record_error("string-width: Input 1 type not string! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	stringValue = ((V_String)string)->string;
	stringLength = ((V_String)string)->length;
#ifdef TOOLBOX	
	c2pstr(stringValue);
	stringWidth = StringWidth((const unsigned char *) stringValue);
	p2cstr((unsigned char *)stringValue);
#endif
#ifdef CARBON
/*
	CopyCStringToPascal(stringValue,pascalString);
	stringWidth = StringWidth(pascalString);
*/
	for(counter = 0;counter<stringLength;counter++){
		stringWidth += CharWidth(stringValue[counter]);
	}
#endif
	
	outputInt = int_to_integer(stringWidth,environment);
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputInt);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_global_2D_to_2D_local( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_global_2D_to_2D_local( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	block = NULL;
	Point		*globalPoint = NULL;
	Point		*localPoint = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("global-to-local: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("global-to-local: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("global-to-local: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Point") != 0) return record_error("global-to-local: Input 1 external not type Point! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	globalPoint = (Point *) ((V_ExternalBlock) block)->blockPtr;
	localPoint = (Point *) X_malloc(sizeof (Point));
	if( localPoint == NULL) return record_error("global-to-local: Failure to allocate Point! - ",moduleName,kERROR,environment);

	SetPt(localPoint,globalPoint->h,globalPoint->v);
	GlobalToLocal(localPoint);

	ptrO = create_externalBlock("Point",sizeof (Point),environment);
	ptrO->blockPtr = (void *) localPoint;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_frame_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_frame_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("frame-rect: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("frame-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	FrameRect(theRect);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_paint_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_paint_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("paint-rect: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("paint-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("paint-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("paint-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	PaintRect(theRect);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_erase_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_erase_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("erase-rect: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("erase-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("erase-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("erase-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	EraseRect(theRect);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_frame_2D_round_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_frame_2D_round_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;
	Int4				ovalWidth = 0;
	Int4				ovalHeight = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("frame-round-rect: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-round-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-round-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("frame-round-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("frame-round-rect: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	ovalWidth = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("frame-round-rect: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	ovalHeight = ((V_Integer) block)->value;

	FrameRoundRect(theRect,ovalWidth,ovalHeight);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_paint_2D_round_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_paint_2D_round_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;
	Int4				ovalWidth = 0;
	Int4				ovalHeight = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("paint-round-rect: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("paint-round-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("paint-round-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("paint-round-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("paint-round-rect: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	ovalWidth = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("paint-round-rect: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	ovalHeight = ((V_Integer) block)->value;

	PaintRoundRect(theRect,ovalWidth,ovalHeight);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_erase_2D_round_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_erase_2D_round_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;
	Int4				ovalWidth = 0;
	Int4				ovalHeight = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("erase-round-rect: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("erase-round-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("erase-round-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("erase-round-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("erase-round-rect: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	ovalWidth = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("erase-round-rect: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	ovalHeight = ((V_Integer) block)->value;

	EraseRoundRect(theRect,ovalWidth,ovalHeight);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_pt_2D_to_2D_angle( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_pt_2D_to_2D_angle( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
    Point				*thePoint = NULL;
	V_Object			block = NULL;
	short				theAngle = 0;

	V_Integer	outputInt = 0;	
	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("pt-to-angle: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("frame-arc: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-arc: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("frame-arc: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-arc: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Point") != 0) return record_error("frame-arc: Input 1 external not type Point! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	thePoint = (Point *) ((V_ExternalBlock) block)->blockPtr;

	PtToAngle(theRect,*thePoint,&theAngle);
	
	outputInt = int_to_integer(theAngle,environment);
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputInt);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_frame_2D_arc( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_frame_2D_arc( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;
	Int4				startAngle = 0;
	Int4				arcAngle = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("frame-arc: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-arc: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-arc: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("frame-arc: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("frame-arc: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	startAngle = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("frame-arc: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	arcAngle = ((V_Integer) block)->value;

	FrameArc(theRect,startAngle,arcAngle);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_paint_2D_arc( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_paint_2D_arc( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;
	Int4				startAngle = 0;
	Int4				arcAngle = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("paint-arc: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("paint-arc: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("paint-arc: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("paint-arc: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("paint-arc: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	startAngle = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("paint-arc: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	arcAngle = ((V_Integer) block)->value;

	PaintArc(theRect,startAngle,arcAngle);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_erase_2D_arc( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_erase_2D_arc( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;
	Int4				startAngle = 0;
	Int4				arcAngle = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("erase-arc: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("erase-arc: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("erase-arc: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("erase-arc: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("erase-arc: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	startAngle = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("erase-arc: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	arcAngle = ((V_Integer) block)->value;

	EraseArc(theRect,startAngle,arcAngle);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_offset_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_offset_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect		*theRect = NULL;
	V_Object	block = NULL;
	Int4		insetHoriz = 0;
	Int4		insetVert = 0;
	Int4		topValue = 0;
	Int4		leftValue = 0;
	Int4		bottomValue = 0;
	Int4		rightValue = 0;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("offset-rect: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("offset-rect: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("offset-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("offset-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("offset-rect: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	insetHoriz = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("offset-rect: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	insetVert = ((V_Integer) block)->value;

	topValue = theRect->top;
	leftValue = theRect->left;
	bottomValue = theRect->bottom;
	rightValue = theRect->right;
	
	theRect = (Rect *) X_malloc(sizeof (Rect));
	SetRect(theRect,leftValue,topValue,rightValue,bottomValue);

	OffsetRect(theRect,insetHoriz,insetVert);
	
	ptrO = create_externalBlock("Rect",sizeof (Rect),environment);
	ptrO->blockPtr = (void *) theRect;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_inset_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_inset_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect		*theRect = NULL;
	V_Object	block = NULL;
	Int4		insetHoriz = 0;
	Int4		insetVert = 0;
	Int4		topValue = 0;
	Int4		leftValue = 0;
	Int4		bottomValue = 0;
	Int4		rightValue = 0;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("inset-rect: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("inset-rect: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("inset-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("inset-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("inset-rect: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	insetHoriz = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("inset-rect: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	insetVert = ((V_Integer) block)->value;

	topValue = theRect->top;
	leftValue = theRect->left;
	bottomValue = theRect->bottom;
	rightValue = theRect->right;
	
	theRect = (Rect *) X_malloc(sizeof (Rect));
	SetRect(theRect,leftValue,topValue,rightValue,bottomValue);

	InsetRect(theRect,insetHoriz,insetVert);
	
	ptrO = create_externalBlock("Rect",sizeof (Rect),environment);
	ptrO->blockPtr = (void *) theRect;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_sect_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_sect_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object		outputObject = NULL;

    Rect		*theRectA = NULL;
    Rect		*theRectB = NULL;
    Rect		*theRectC = NULL;
	V_Object	block = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("sect-rect: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 2) return record_error("sect-rect: Outarity not 2!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("sect-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("sect-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRectA = (Rect *) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("sect-rect: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("sect-rect: Input 2 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRectB = (Rect *) ((V_ExternalBlock) block)->blockPtr;

	theRectC = (Rect *) X_malloc(sizeof (Rect));

	if(SectRect(theRectA,theRectB,theRectC)){
		outputObject = (V_Object) create_boolean("TRUE",environment);
		VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputObject);

	}else{
		outputObject = (V_Object) create_boolean("FALSE",environment);
		VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputObject);

	}

	ptrO = create_externalBlock("Rect",sizeof (Rect),environment);
	ptrO->blockPtr = (void *) theRectC;
	outputObject = (V_Object) ptrO;
	VPLPrimSetOutputObject(environment,primInArity,1,primitive,(V_Object) outputObject);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_pt_2D_in_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_pt_2D_in_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect		*theRect = NULL;
    Point		*thePoint = NULL;
	V_Object	block = NULL;
	V_Boolean	ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("pt-to-rect: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("pt-to-rect: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("pt-to-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Point") != 0) return record_error("pt-to-rect: Input 1 external not type Point! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	thePoint = (Point *) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("pt-to-rect: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("pt-to-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	if(PtInRect(*thePoint,theRect)) ptrO = create_boolean("TRUE",environment);
	else ptrO = create_boolean("FALSE",environment);

	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_inval_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_inval_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
    WindowRef	*whichWindow = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("inval-rect: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("inval-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("inval-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("inval-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("inval-rect: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"OpaqueWindowPtr") != 0) return record_error("inval-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	whichWindow = (WindowRef *) ((V_ExternalBlock)block)->blockPtr;

#ifdef CARBON	
	InvalWindowRect(*whichWindow,theRect);
#endif
#ifdef TOOLBOX	
	InvalRect(theRect);
#endif
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_valid_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_valid_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("erase-rect: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("erase-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("erase-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("erase-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

#ifdef TOOLBOX	
	ValidRect(theRect);
#endif
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_scroll_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_scroll_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect		*theRect = NULL;
    RgnHandle	*theRgn = NULL;
	V_Object	block = NULL;
	Int4		insetHoriz = 0;
	Int4		insetVert = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 4) return record_error("scroll-rect: Inarity not 4!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("scroll-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("scroll-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("scroll-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("scroll-rect: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	insetHoriz = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kInteger) return record_error("scroll-rect: Input 3 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	insetVert = ((V_Integer) block)->value;

	block = VPLPrimGetInputObject(3,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("scroll-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("scroll-rect: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgn = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	ScrollRect(theRect,insetHoriz,insetVert,*theRgn);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_clip_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_clip_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("clip-rect: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("clip-rect: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("clip-rect: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("clip-rect: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	ClipRect(theRect);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_frame_2D_oval( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_frame_2D_oval( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("frame-oval: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-oval: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-oval: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("frame-oval: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	FrameOval(theRect);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_paint_2D_oval( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_paint_2D_oval( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("paint-oval: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("paint-oval: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("paint-oval: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("paint-oval: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	PaintOval(theRect);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_erase_2D_oval( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_erase_2D_oval( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Rect				*theRect = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("erase-oval: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("erase-oval: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("erase-oval: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"Rect") != 0) return record_error("erase-oval: Input 1 external not type Rect! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRect = (Rect	*) ((V_ExternalBlock) block)->blockPtr;

	EraseOval(theRect);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_set_2D_origin( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_set_2D_origin( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	top = NULL;
	V_Object	left = NULL;
	Int4		topValue = 0;
	Int4		leftValue = 0;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("set-origin: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("set-origin: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	left = VPLPrimGetInputObject(0,primitive,environment);
	if( left == NULL || left->type != kInteger) return record_error("set-origin: Input 1 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	top = VPLPrimGetInputObject(1,primitive,environment);
	if( top == NULL || top->type != kInteger) return record_error("set-origin: Input 2 type not integer! - ",moduleName,kWRONGINPUTTYPE,environment);

	topValue = ((V_Integer) top)->value;
	leftValue = ((V_Integer) left)->value;
	
	SetOrigin(leftValue,topValue);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_new_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_new_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle		*theRgn = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("new-rgn: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("new-rgn: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	theRgn = (RgnHandle *) X_malloc(sizeof (RgnHandle));
	*theRgn = NewRgn();

	ptrO = create_externalBlock("RgnHandle",sizeof (RgnHandle),environment);
	ptrO->blockPtr = (void *) theRgn;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_open_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_open_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
#pragma unused (primitive)

	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("open-rgn: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("open-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	OpenRgn();

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_close_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_close_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgn = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("frame-rgn: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("frame-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgn = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	CloseRgn(*theRgn);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_dispose_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_dispose_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgn = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("dispose-rgn: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("dispose-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("dispose-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("dispose-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgn = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	DisposeRgn(*theRgn);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_copy_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_copy_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgnA = NULL;
    RgnHandle			*theRgnB = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 2) return record_error("copy-rgn: Inarity not 2!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("copy-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("copy-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("copy-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnA = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("copy-rgn: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("copy-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnB = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	CopyRgn(*theRgnA,*theRgnB);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_sect_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_sect_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgnA = NULL;
    RgnHandle			*theRgnB = NULL;
    RgnHandle			*theRgnC = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("sect-rgn: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("sect-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("sect-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("sect-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnA = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("sect-rgn: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("sect-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnB = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("sect-rgn: Input 3 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("sect-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnC = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	SectRgn(*theRgnA,*theRgnB,*theRgnC);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_union_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_union_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgnA = NULL;
    RgnHandle			*theRgnB = NULL;
    RgnHandle			*theRgnC = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("union-rgn: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("union-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("union-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("union-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnA = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("union-rgn: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("union-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnB = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("union-rgn: Input 3 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("union-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnC = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	UnionRgn(*theRgnA,*theRgnB,*theRgnC);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_diff_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_diff_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgnA = NULL;
    RgnHandle			*theRgnB = NULL;
    RgnHandle			*theRgnC = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 3) return record_error("diff-rgn: Inarity not 3!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("diff-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("diff-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("diff-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnA = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(1,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("diff-rgn: Input 2 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("diff-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnB = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	block = VPLPrimGetInputObject(2,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("diff-rgn: Input 3 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("diff-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgnC = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	DiffRgn(*theRgnA,*theRgnB,*theRgnC);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_frame_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_frame_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgn = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("frame-rgn: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("frame-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgn = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	FrameRgn(*theRgn);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_paint_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_paint_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgn = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("frame-rgn: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("frame-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgn = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	PaintRgn(*theRgn);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_erase_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_erase_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgn = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("frame-rgn: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("frame-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("frame-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("frame-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgn = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	EraseRgn(*theRgn);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_invert_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_invert_2D_rgn( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    RgnHandle			*theRgn = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("invert-rgn: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("invert-rgn: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("invert-rgn: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"RgnHandle") != 0) return record_error("invert-rgn: Input 1 external not type RgnHandle! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theRgn = (RgnHandle	*) ((V_ExternalBlock) block)->blockPtr;

	InvertRgn(*theRgn);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_pen_2D_normal( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_pen_2D_normal( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
#pragma unused(primitive)
	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("pen-normal: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("pen-normal: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	PenNormal();

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_pen_2D_stripe( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_pen_2D_stripe( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
#pragma unused(primitive)
    Pattern		thePattern;

	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("pen-normal: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("pen-normal: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	thePattern.pat[0] = 136;
	thePattern.pat[1] = 17;
	thePattern.pat[2] = 34;
	thePattern.pat[3] = 68;
	thePattern.pat[4] = 136;
	thePattern.pat[5] = 17;
	thePattern.pat[6] = 34;
	thePattern.pat[7] = 68;
	
/*	StuffHex(&thePattern,c2pstr("8811224488112244")); */

	PenPat(&thePattern);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_pen_2D_drag( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_pen_2D_drag( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
#pragma unused(primitive)
	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("pen-normal: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("pen-normal: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	PenMode(patXor);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_get_2D_pen_2D_state( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_get_2D_pen_2D_state( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    PenState		*theState = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("get-pen-state: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("get-pen-state: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	theState = (PenState *) X_malloc(sizeof (PenState));
	GetPenState(theState);

	ptrO = create_externalBlock("PenState",sizeof (PenState),environment);
	ptrO->blockPtr = (void *) theState;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_set_2D_pen_2D_state( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_set_2D_pen_2D_state( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    PenState		*theState = NULL;
	V_Object			block = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("set-pen-state: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 0) return record_error("set-pen-state: Outarity not 0!",moduleName,kWRONGOUTARITY,environment);
	
	block = VPLPrimGetInputObject(0,primitive,environment);
	if( block == NULL || block->type != kExternalBlock) return record_error("set-pen-state: Input 1 type not externalBlock! - ",moduleName,kWRONGINPUTTYPE,environment);
	if( strcmp(((V_ExternalBlock) block)->name,"PenState") != 0) return record_error("set-pen-state: Input 1 external not type PenState! - ",moduleName,kWRONGINPUTTYPE,environment);
	
	theState = (PenState *) ((V_ExternalBlock) block)->blockPtr;

	SetPenState(theState);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_get_2D_mouse( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_get_2D_mouse( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
    Point				*thePoint = NULL;
	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("get-mouse: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("get-mouse: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	thePoint = (Point *) X_malloc(sizeof (Point));
	GetMouse(thePoint);

	ptrO = create_externalBlock("Point",sizeof (Point),environment);
	ptrO->blockPtr = (void *) thePoint;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_still_2D_down( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_still_2D_down( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object		outputObject = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 0) return record_error("still-down: Inarity not 0!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("still-down: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	if(StillDown()) {
		outputObject = (V_Object) create_boolean("TRUE",environment);
		VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputObject);

	}
	else {
		outputObject = (V_Object) create_boolean("FALSE",environment);
		VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) outputObject);

	}
	
	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_make_2D_icon_2D_content_2D_info( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_make_2D_icon_2D_content_2D_info( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive )
{
	V_Object	object = NULL;
	Int1		*iconFileName = NULL;
	CFStringRef	cfIconFileName;
	CFBundleRef	theMainBundle;
	CFURLRef	theResourceURL;
	FSRef		theFileRef;
	Boolean		gotFile = 0;
	OSStatus	theResult;
	ControlButtonContentInfo	*theContentInfo = NULL;
	IconFamilyHandle	theIconFamily = NULL;
	IconSuiteRef	*theIconSuite = NULL;

	V_ExternalBlock		ptrO = NULL;

	Int1	*moduleName = NULL;

	if(primInArity != 1) return record_error("make-icon-content-info: Inarity not 1!",moduleName,kWRONGINARITY,environment);
	if(primOutArity != 1) return record_error("make-icon-content-info: Outarity not 1!",moduleName,kWRONGOUTARITY,environment);
	
	object = VPLPrimGetInputObject(0,primitive,environment);
	if( object == NULL || object->type != kString) return record_error("make-icon-content-info: Input 1 type not string! - ",moduleName,kWRONGINPUTTYPE,environment);

	iconFileName = ((V_String) object)->string;
	
	cfIconFileName = CFStringCreateWithCString(NULL,iconFileName,kCFStringEncodingUTF8);

	theMainBundle = CFBundleGetMainBundle();
	
	theResourceURL = CFBundleCopyResourceURL(theMainBundle,cfIconFileName,NULL,NULL);
	
	CFRelease(cfIconFileName);
	
	if(theResourceURL != NULL) {
		gotFile = CFURLGetFSRef(theResourceURL,&theFileRef);
/*		if(gotFile){
			theIconRef = (IconRef *) X_malloc(sizeof(IconRef));
			theResult = RegisterIconRefFromFSRef('mVPL','bTyp',&theFileRef,theIconRef);
			if(theResult == noErr){
				theContentInfo = (ControlButtonContentInfo *) X_malloc(sizeof(ControlButtonContentInfo));
				theContentInfo->contentType = kControlContentIconRef;
				theContentInfo->u.iconRef = *theIconRef;
			}
		} */
		if(gotFile){
			theResult = ReadIconFromFSRef(&theFileRef,&theIconFamily);
			if(theResult == noErr){
				theIconSuite = (IconSuiteRef *) X_malloc(sizeof(IconSuiteRef));
				IconFamilyToIconSuite(theIconFamily,kSelectorAllAvailableData,theIconSuite);
				theContentInfo = (ControlButtonContentInfo *) X_malloc(sizeof(ControlButtonContentInfo));
				theContentInfo->contentType = kControlContentIconSuiteHandle;
				theContentInfo->u.iconSuite = *theIconSuite;
			}
		}
	}

	ptrO = create_externalBlock("ControlButtonContentInfo",sizeof (ControlButtonContentInfo),environment);
	ptrO->blockPtr = (void *) theContentInfo;
	VPLPrimSetOutputObject(environment,primInArity,0,primitive,(V_Object) ptrO);

	*trigger = kSuccess;	
	return kNOERROR;
}

Nat4	loadConstants_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID);
Nat4	loadConstants_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)        

        return kNOERROR;

}

Nat4	loadStructures_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID);
Nat4	loadStructures_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)        

        return kNOERROR;

}

Nat4	loadProcedures_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID);
Nat4	loadProcedures_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)

        return kNOERROR;

}

Int4 VPLP_ints_2D_to_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_ints_2D_to_2D_rect( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ){
	return VPLP_ints_2D_to_2D_Rect(environment,trigger,primInArity,primOutArity,primitive);
}

Int4 VPLP_ints_2D_to_2D_point( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_ints_2D_to_2D_point( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ){
	return VPLP_ints_2D_to_2D_Point(environment,trigger,primInArity,primOutArity,primitive);
}

Nat4	loadPrimitives_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID);
Nat4	loadPrimitives_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID)
{
        V_ExtPrimitive	result = NULL;
        V_Dictionary	dictionary = environment->externalPrimitivesTable;

        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"ints-to-rect",dictionary,4,1,VPLP_ints_2D_to_2D_Rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"ints-to-point",dictionary,2,1,VPLP_ints_2D_to_2D_Point)) == NULL) return kERROR;

        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"move-to",dictionary,2,0,VPLP_move_2D_to)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"line-to",dictionary,2,0,VPLP_line_2D_to)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"ints-to-Point",dictionary,2,1,VPLP_ints_2D_to_2D_Point)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"ints-to-Rect",dictionary,4,1,VPLP_ints_2D_to_2D_Rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"draw-string",dictionary,1,0,VPLP_draw_2D_string)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"string-width",dictionary,1,1,VPLP_string_2D_width)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"global-to-local",dictionary,1,1,VPLP_global_2D_to_2D_local)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"frame-rect",dictionary,1,0,VPLP_frame_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"paint-rect",dictionary,1,0,VPLP_paint_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"erase-rect",dictionary,1,0,VPLP_erase_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"frame-round-rect",dictionary,3,0,VPLP_frame_2D_round_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"paint-round-rect",dictionary,3,0,VPLP_paint_2D_round_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"erase-round-rect",dictionary,3,0,VPLP_erase_2D_round_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"pt-to-angle",dictionary,2,1,VPLP_pt_2D_to_2D_angle)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"frame-arc",dictionary,3,0,VPLP_frame_2D_arc)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"paint-arc",dictionary,3,0,VPLP_paint_2D_arc)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"erase-arc",dictionary,3,0,VPLP_erase_2D_arc)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"offset-rect",dictionary,3,1,VPLP_offset_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"inset-rect",dictionary,3,1,VPLP_inset_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"sect-rect",dictionary,2,2,VPLP_sect_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"pt-in-rect",dictionary,2,1,VPLP_pt_2D_in_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"inval-rect",dictionary,2,0,VPLP_inval_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"valid-rect",dictionary,1,0,VPLP_valid_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"scroll-rect",dictionary,4,0,VPLP_scroll_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"clip-rect",dictionary,1,0,VPLP_clip_2D_rect)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"frame-oval",dictionary,1,0,VPLP_frame_2D_oval)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"paint-oval",dictionary,1,0,VPLP_paint_2D_oval)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"erase-oval",dictionary,1,0,VPLP_erase_2D_oval)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"set-origin",dictionary,2,0,VPLP_set_2D_origin)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"new-rgn",dictionary,0,1,VPLP_new_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"open-rgn",dictionary,0,0,VPLP_open_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"close-rgn",dictionary,1,0,VPLP_close_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"dispose-rgn",dictionary,1,0,VPLP_dispose_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"copy-rgn",dictionary,2,0,VPLP_copy_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"sect-rgn",dictionary,3,0,VPLP_sect_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"union-rgn",dictionary,3,0,VPLP_union_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"diff-rgn",dictionary,3,0,VPLP_diff_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"frame-rgn",dictionary,1,0,VPLP_frame_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"paint-rgn",dictionary,1,0,VPLP_paint_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"erase-rgn",dictionary,1,0,VPLP_erase_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"invert-rgn",dictionary,1,0,VPLP_invert_2D_rgn)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"pen-normal",dictionary,0,0,VPLP_pen_2D_normal)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"pen-stripe",dictionary,0,0,VPLP_pen_2D_stripe)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"pen-drag",dictionary,0,0,VPLP_pen_2D_drag)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"get-pen-state",dictionary,0,1,VPLP_get_2D_pen_2D_state)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"set-pen-state",dictionary,1,0,VPLP_set_2D_pen_2D_state)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"get-mouse",dictionary,0,1,VPLP_get_2D_mouse)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"still-down",dictionary,0,1,VPLP_still_2D_down)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"make-icon-content-info",dictionary,1,1,VPLP_make_2D_icon_2D_content_2D_info)) == NULL) return kERROR;

        return kNOERROR;

}

Nat4	load_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID);
Nat4	load_VPX_PrimitivesMacDrawing(V_Environment environment,char *bundleID)
{
		Nat4	result = 0;
		
		result = loadConstants_VPX_PrimitivesMacDrawing(environment,bundleID);
		result = loadStructures_VPX_PrimitivesMacDrawing(environment,bundleID);
		result = loadProcedures_VPX_PrimitivesMacDrawing(environment,bundleID);
		result = loadPrimitives_VPX_PrimitivesMacDrawing(environment,bundleID);
		
		return result;
}


