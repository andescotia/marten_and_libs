/*
	
	VPL_Primitives.c
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/
#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif	

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "VPX_PrimitivesUI.h"

typedef struct 
{
 UInt32			button;
 CFStringRef	string;
 WindowRef		window;
} VPLUIModalWindowInfo;

#define		VPLUIModalWindowNibName			CFSTR("VPXUI")
#define		VPLUIModalWindowShowName		CFSTR("Show")
#define		VPLUIModalWindowAskName			CFSTR("Ask")
#define		VPLUIModalWindowAnswerName		CFSTR("Answer")
#define		VPLUIModalWindowAnswerVName		CFSTR("Answer-V")
#define		VPLUIModalWindowPromptText		(ControlID){ 'VPXu', 1 }
#define		VPLUIModalWindowAskText			(ControlID){ 'VPXu', 3 }
#define		VPLUIModalWindowOKButton		(ControlID){ 'VPXu', 10 }
#define		VPLUIModalWindowCancelButton	(ControlID){ 'VPXu', 11 }
#define		VPLUIModalWindowOtherButton		(ControlID){ 'VPXu', 12 }
#define		VPLUIModalWindowOtherCommandID	'VuOT'
#define		VPLUIModalWindowAskCommandID	'VaOK'
#define		VPLUIModalWindowEncoding		kCFStringEncodingUTF8
#define		VPLUIModalWindowTransition		kWindowShowTransitionAction
//#define		VPLUIModalWindowTransition		(WindowTransitionEffect)4

#define		VPLUIModalWindowSelectName			CFSTR("Select")
#define		VPLUIModalWindowSelectList			(ControlID){ 'VPXu', 22 }
#define		VPLUIModalWindowSelectListColumn	'SELC'
#define		VPLUIModalWindowSelectCommandID		'VsOK'

#define		VPLUIBundleIDFramework			CFSTR("com.andescotia.frameworks.martenui.aqua")
#define		VPLUIBundleIDBundle				CFSTR("com.andescotia.bundles.martenui.aqua")


Int4 VPLP_answer( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );/* answer */
Int4 VPLP_answer( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* answer */
{

	Nat4		length = 0;
	V_Object	string = NULL;	
	V_Object	string1 = NULL;	
	V_Object	string2 = NULL;	
	V_Object	string3 = NULL;	
	Int1		*tempString = "";

	CFStringRef	displayString = NULL;
	CFStringRef	button1String = NULL;
	CFStringRef	button2String = NULL;
	CFStringRef	button3String = NULL;
	
	Int1		*primName = "answer";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArityMin(environment,primName, primInArity, 2 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputArityMax(environment,primName, primInArity, 4 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 1 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kString,0,primName, primitive, environment );
	if( result != kNOERROR ) return result;

#endif

	string = VPLPrimGetInputObject(0,primitive,environment);
	length = ((V_String) string)->length;
	tempString = ((V_String) string)->string;

	displayString = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
	if(displayString == NULL) displayString = CFStringCreateWithCString( NULL, tempString, kCFStringEncodingMacRoman );
	
	string1 = VPLPrimGetInputObject(1,primitive,environment);
	if( string1 == NULL || string1->type != kString){
	
		if( string1 ) tempString = object_to_string( string1, environment );
		if(tempString) {
			length = strlen( tempString );
		} else {
			record_error("answer: Input 2 type not string",NULL,kWRONGINPUTTYPE,environment);
			return kWRONGINPUTTYPE;
		}
	} else {
		length = ((V_String) string1)->length;
		tempString = ((V_String) string1)->string;
	}

	button1String = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );

	if(primInArity > 2 ) {
		string2 = VPLPrimGetInputObject(2,primitive,environment);
		if( string2 == NULL || string2->type != kString){
		
			if( string2 ) tempString = object_to_string( string2, environment );
			if(tempString) {
				length = strlen( tempString );
			} else {
				record_error("answer: Input 3 type not string",NULL,kWRONGINPUTTYPE,environment);
				return kWRONGINPUTTYPE;
			}
		} else {
			length = ((V_String) string2)->length;
			tempString = ((V_String) string2)->string;
		}
		button2String = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
	}

	if(primInArity > 3 ) {
		string3 = VPLPrimGetInputObject(3,primitive,environment);
		if( string3 == NULL || string3->type != kString){
		
			if( string3 ) tempString = object_to_string( string3, environment );
			if(tempString) {
				length = strlen( tempString );
			} else {
				record_error("answer: Input 4 type not string",NULL,kWRONGINPUTTYPE,environment);
				return kWRONGINPUTTYPE;
			}
		} else {
			length = ((V_String) string3)->length;
			tempString = ((V_String) string3)->string;
		}
		button3String = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
	}

    NSInteger answerResponse = displayAnswerModal(displayString, button1String, button2String, button3String);
    
    switch (answerResponse) {
        case 0:
            string = string1;
            break;
        case 1:
            string = string2;
            break;
        case 2:
            string = string3;
            break;
    }
	
	increment_count( string );

	VPLPrimSetOutputObject(environment,primInArity,0,primitive, (V_Object) string);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_answer_2D_v( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* answer-v */
Int4 VPLP_answer_2D_v( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* answer-v */
{

	Nat4		length = 0;
	V_Object	string = NULL;	
	V_Object	string1 = NULL;	
	V_Object	string2 = NULL;	
	V_Object	string3 = NULL;	
	Int1		*tempString = "";

	CFStringRef	displayString = NULL;
	CFStringRef	button1String = NULL;
	CFStringRef	button2String = NULL;
	CFStringRef	button3String = NULL;
	
	Int1		*primName = "answer-v";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArityMin(environment,primName, primInArity, 2 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputArityMax(environment,primName, primInArity, 4 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 1 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kString,0,primName, primitive, environment );
	if( result != kNOERROR ) return result;

#endif

	string = VPLPrimGetInputObject(0,primitive,environment);
	length = ((V_String) string)->length;
	tempString = ((V_String) string)->string;

	displayString = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
	if(displayString == NULL) displayString = CFStringCreateWithCString( NULL, tempString, kCFStringEncodingMacRoman );
	
	string1 = VPLPrimGetInputObject(1,primitive,environment);
	if( string1 == NULL || string1->type != kString){
	
		if( string1 ) tempString = object_to_string( string1, environment );
		if(tempString) {
			length = strlen( tempString );
		} else {
			record_error("answer-v: Input 2 type not string",NULL,kWRONGINPUTTYPE,environment);
			return kWRONGINPUTTYPE;
		}
	} else {
		length = ((V_String) string1)->length;
		tempString = ((V_String) string1)->string;
	}

	button1String = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );

	if(primInArity > 2 ) {
		string2 = VPLPrimGetInputObject(2,primitive,environment);
		if( string2 == NULL || string2->type != kString){
		
			if( string2 ) tempString = object_to_string( string2, environment );
			if(tempString) {
				length = strlen( tempString );
			} else {
				record_error("answer-v: Input 3 type not string",NULL,kWRONGINPUTTYPE,environment);
				return kWRONGINPUTTYPE;
			}
		} else {
			length = ((V_String) string2)->length;
			tempString = ((V_String) string2)->string;
		}
		button2String = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
	}

	if(primInArity > 3 ) {
		string3 = VPLPrimGetInputObject(3,primitive,environment);
		if( string3 == NULL || string3->type != kString){
		
			if( string3 ) tempString = object_to_string( string3, environment );
			if(tempString) {
				length = strlen( tempString );
			} else {
				record_error("answer-v: Input 4 type not string",NULL,kWRONGINPUTTYPE,environment);
				return kWRONGINPUTTYPE;
			}
		} else {
			length = ((V_String) string3)->length;
			tempString = ((V_String) string3)->string;
		}
		button3String = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
	}

    NSInteger answerResponse = displayAnswerModalV(displayString, button1String, button2String, button3String);
    
    switch (answerResponse) {
        case 0:
            string = string1;
            break;
        case 1:
            string = string2;
            break;
        case 2:
            string = string3;
            break;
    }
	
	increment_count( string );

	VPLPrimSetOutputObject(environment,primInArity,0,primitive, (V_Object) string);

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_ask_2D_value( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* ask */
Int4 VPLP_ask_2D_value( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* ask */
{

	Nat4		length = 0;
	V_Object	string = NULL;	
	Int1		*tempString = "";
	Int1		*tempCString = "";

	CFStringRef	displayString = NULL;
	CFStringRef	defaultString = NULL;
	Boolean		goodResult = false;

	Int1		*primName = "ask-value";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArityMin(environment,primName, primInArity, 0 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputArityMax(environment,primName, primInArity, 2 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 1 );
	if( result != kNOERROR ) return result;

	switch(primInArity) {
		case 1:
			result = VPLPrimCheckInputObjectType( kString,0,primName, primitive, environment );
			if( result != kNOERROR ) return result;
		
		default:
			break;
	}

#endif

	if(primInArity >= 1 ) {
		string = VPLPrimGetInputObject(0,primitive,environment);
		displayString = CFStringCreateWithCString( NULL, ((V_String) string)->string, VPLUIModalWindowEncoding );
		if(displayString == NULL) displayString = CFStringCreateWithCString( NULL, tempCString, kCFStringEncodingMacRoman );
	}

	if(primInArity >= 2 ) {
		string = VPLPrimGetInputObject(1,primitive,environment);
		if( string == NULL || string->type != kString){
		
			if( string ) tempString = object_to_string( string, environment );
			if(tempString) {
				length = strlen( tempString );
			} else {
				record_error("ask-value: Input 1 type not string in module - ",NULL,kWRONGINPUTTYPE,environment);
				return kWRONGINPUTTYPE;
			}
		} else {
			length = ((V_String) string)->length;
			tempString = ((V_String) string)->string;
		}

		defaultString = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
	}
	
    CFStringRef responseString = nil;
    NSInteger modalResult = displayAskModal(displayString, defaultString, &responseString);
    
	if( modalResult == 0 ) {
//		CFShow( responseString );
		length = CFStringGetMaximumSizeForEncoding( CFStringGetLength(responseString), VPLUIModalWindowEncoding ) + 1;
		tempCString = (Int1*)X_malloc( length );
		goodResult = CFStringGetCString( responseString, tempCString, length , VPLUIModalWindowEncoding );
			if( !goodResult ) {
				X_free(tempCString );
				VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
				goto EXIT;
			}
		string = string_to_object( tempCString, environment );
		VPLPrimSetOutputObject(environment,primInArity,0,primitive, (V_Object) string);
		X_free(tempCString );
	}
	else VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);

EXIT:
	if( displayString ) CFRelease( displayString );
	if( defaultString ) CFRelease( defaultString );
	if( responseString ) CFRelease( responseString );		

	*trigger = kSuccess;	
	return kNOERROR;
}

Int4 VPLP_ask_2D_text( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* ask */
Int4 VPLP_ask_2D_text( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* ask */
{
	return VPLP_ask_2D_value(environment,trigger,primInArity,primOutArity,primitive);
}

Int4 VPLP_show( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* show */
Int4 VPLP_show( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* show */
{
	Int1		*tempCString = "";

	CFStringRef	displayString = NULL;

	Int1		*primName = "show";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArityMin(environment,primName, primInArity, 1 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 0 );
	if( result != kNOERROR ) return result;

#endif

	result = VPLPrimGetInputObjectStringCoalesce( &tempCString, 0, primInArity, primName, primitive, environment);
	if( result != kNOERROR ) return result;

	displayString = CFStringCreateWithCString( NULL, tempCString, VPLUIModalWindowEncoding );
	if(displayString == NULL) displayString = CFStringCreateWithCString( NULL, tempCString, kCFStringEncodingMacRoman );
	
/*	err = CreateNibReference( VPLUIModalWindowNibName, &ourNIB );
	if( err != noErr ) {
		CFBundleRef				nibBundle = CFBundleGetBundleWithIdentifier( VPLUIBundleIDFramework );
		if( nibBundle == NULL )	nibBundle = CFBundleGetBundleWithIdentifier( VPLUIBundleIDBundle );
		if( nibBundle != NULL )	err = CreateNibReferenceWithCFBundle( nibBundle, VPLUIModalWindowNibName, &ourNIB );
	}
*/

    displayShowModal(displayString);

    if(displayString) CFRelease( displayString );
	X_free(tempCString );
    
    *trigger = kSuccess;
    return kNOERROR;
}

/*  Begin Select Window */

Int4 VPLP_select( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ); /* select */
Int4 VPLP_select( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ) /* select */
{

	Nat4		length = 0;
	V_Object	string = NULL;
	V_Object	list = NULL;
	Int1		*tempString = "";
	Int1		*tempCString = "";

	CFStringRef	displayString = NULL;
	Boolean		goodResult = false;

	Int1		*primName = "select";
	Int4		result = kNOERROR;

#ifdef VPL_VERBOSE_ENGINE
	result = VPLPrimCheckInputArityMin(environment,primName, primInArity, 1 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputArityMax(environment,primName, primInArity, 2 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckOutputArity(environment,primName, primOutArity, 1 );
	if( result != kNOERROR ) return result;

	result = VPLPrimCheckInputObjectType( kList,0,primName, primitive, environment );
	if( result != kNOERROR ) return result;

	if(primInArity == 2 ) {
		result = VPLPrimCheckInputObjectType( kString,1,primName, primitive, environment );
		if( result != kNOERROR ) return result;
	}

#endif

	list = VPLPrimGetInputObject(0,primitive,environment);
	V_List VPLUISelectList = (V_List)list;

	if(primInArity == 2 ) {
		string = VPLPrimGetInputObject(1,primitive,environment);
		length = ((V_String) string)->length;
		tempString = ((V_String) string)->string;

		displayString = CFStringCreateWithCString( NULL, tempString, VPLUIModalWindowEncoding );
		if(displayString == NULL) displayString = CFStringCreateWithCString( NULL, tempCString, kCFStringEncodingMacRoman );
	}
	
    CFMutableArrayRef choices = CFArrayCreateMutable(NULL, VPLUISelectList->listLength, NULL);
    for (int i = 0; i < VPLUISelectList->listLength; i++) {
        V_String item = (V_String)((V_List)VPLUISelectList->objectList[i]);
        CFStringRef itemString = CFStringCreateWithCString( NULL, item->string, VPLUIModalWindowEncoding );
        CFArrayAppendValue(choices, itemString);
    }
    
    CFStringRef selectStringOutput = nil;
    NSInteger modalResult = displaySelectModal(choices, displayString, &selectStringOutput);
    
	if( modalResult == 0 ) {
//		CFShow( theInfo.string );
		length = CFStringGetMaximumSizeForEncoding( CFStringGetLength(selectStringOutput), VPLUIModalWindowEncoding ) + 1;
		tempCString = (Int1*)X_malloc( length );
		goodResult = CFStringGetCString( selectStringOutput, tempCString, length , VPLUIModalWindowEncoding );
			if( !goodResult ) {
				X_free(tempCString );
				VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);
				goto EXIT;
			}
		string = (V_Object) create_string( tempCString, environment );
		VPLPrimSetOutputObject(environment,primInArity,0,primitive, (V_Object) string);
		X_free(tempCString );
	}
	else VPLPrimSetOutputObjectNULL(environment,primInArity,0,primitive);

EXIT:

	if( displayString ) CFRelease( displayString );
	if( selectStringOutput ) CFRelease( selectStringOutput );		

	*trigger = kSuccess;	
	return kNOERROR;
}








Nat4	loadConstants_VPX_PrimitivesUI(V_Environment environment,char *bundleID);
Nat4	loadConstants_VPX_PrimitivesUI(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadStructures_VPX_PrimitivesUI(V_Environment environment,char *bundleID);
Nat4	loadStructures_VPX_PrimitivesUI(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Nat4	loadProcedures_VPX_PrimitivesUI(V_Environment environment,char *bundleID);
Nat4	loadProcedures_VPX_PrimitivesUI(V_Environment environment,char *bundleID)
{
#pragma unused(environment)
#pragma unused(bundleID)
        
        return kNOERROR;

}

Int4 VPLP_ask( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive );
Int4 VPLP_ask( V_Environment environment , enum opTrigger *trigger , Nat4 primInArity , Nat4 primOutArity , vpl_PrimitiveInputs primitive ){
	return VPLP_ask_2D_value(environment,trigger,primInArity,primOutArity,primitive);
}

Nat4	loadPrimitives_VPX_PrimitivesUI(V_Environment environment,char *bundleID);
Nat4	loadPrimitives_VPX_PrimitivesUI(V_Environment environment,char *bundleID)
{
        V_ExtPrimitive	result = NULL;
        V_Dictionary	dictionary = environment->externalPrimitivesTable;
		
		if(dictionary){
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"show",dictionary,1,0,VPLP_show)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"ask-value",dictionary,0,1,VPLP_ask_2D_value)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"ask",dictionary,0,1,VPLP_ask_2D_value)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"ask-text",dictionary,0,1,VPLP_ask_2D_text)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"answer",dictionary,2,1,VPLP_answer)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"answer-v",dictionary,2,1,VPLP_answer_2D_v)) == NULL) return kERROR;
        if((result = extPrimitive_new(bundleID,kContinueOnSuccess,"select",dictionary,1,1,VPLP_select)) == NULL) return kERROR;
		}
		
        return kNOERROR;

}

Nat4	load_VPX_PrimitivesUI(V_Environment environment,char *bundleID);
Nat4	load_VPX_PrimitivesUI(V_Environment environment,char *bundleID)
{
		Nat4	result = 0;
		
		result = loadConstants_VPX_PrimitivesUI(environment,bundleID);
		result = loadStructures_VPX_PrimitivesUI(environment,bundleID);
		result = loadProcedures_VPX_PrimitivesUI(environment,bundleID);
		result = loadPrimitives_VPX_PrimitivesUI(environment,bundleID);
		
		return result;
}

