#include "VPL_PrimUtility.h"

#ifndef NEWEXPORT
#define NEWEXPORT

extern void MacVPLCallbackHandler(void);

#define DECLARE_ENVIRONMENT 	Int4 result = 0;V_List parameterList = NULL;V_List inputList = NULL;V_List outputList = NULL;Nat4 counter = 0;Int4 (*spawnPtr)(V_Environment,char *,V_List , V_List ,Int4 *);
#define INITIALIZE_ENVIRONMENT 	pClassConstantToIndex=gClassConstantToIndex;pMethodConstantToIndex=gMethodConstantToIndex;pValueConstantToIndex=gValueConstantToIndex;environment->callbackHandler = MacVPLCallbackHandler;environment->stackSpawner = vpx_spawn_stack;environment->compiled = kTRUE;environment->logging = kFALSE;result = post_load(environment);

#define LOOP(a) root[input[a]]
#define LIST(a) ((V_List) root[a])->objectList[counter]
#define ROOT(a) &root[a]
#define TERMINAL(a) root[a]
#define NONE vpx_none
#define INJECT(a) ((V_String) root[a])->string

#define LISTROOT(x,y) rootNode[y] = vpx_create_listRootNode(environment,rootNode[y],root[x]);

#define HEADER(x) 	Nat4 repeatLimit = 0; enum boolType	repeat = kFALSE; Nat4 counter = 0;  Bool nextCase = kFALSE; enum opTrigger theOutcome = kSuccess; enum opTrigger result = kSuccess; Nat4 contextClassIndex = contextIndex; V_Object root[x]; for(counter=0;counter<x;counter++) root[counter] = NULL; counter = 0;
#define HEADERWITHNONE(x) 	Nat4 repeatLimit = 0; enum boolType	repeat = kFALSE; Nat4 counter = 0;  Bool nextCase = kFALSE; enum opTrigger theOutcome = kSuccess; enum opTrigger result = kSuccess; Nat4 contextClassIndex = contextIndex; V_Object vpx_none = NULL; V_Object	root[x]; for(counter=0;counter<x;counter++) root[counter] = NULL; counter = 0; vpx_none = (V_Object) create_none(environment);
#define CLASSCONTEXT(x) 	contextIndex = pClassConstantToIndex[kVPXClass_ ## x];

#define DECREMENT(x) 	for(counter=0;counter<x;counter++) decrement_count(environment,root[counter]); counter = 0;
#define FOOTER(x) 	THEFINISH: DECREMENT(x) *outcome = theOutcome; return nextCase;
#define FOOTERWITHNONE(x) 	THEFINISH: DECREMENT(x) decrement_count(environment,vpx_none); *outcome = theOutcome; return nextCase;
#define FOOTERSINGLECASE(x) 	THEFINISH: DECREMENT(x) return theOutcome;
#define FOOTERSINGLECASEWITHNONE(x) 	THEFINISH: DECREMENT(x) decrement_count(environment,vpx_none); return theOutcome;

#define REPEATBEGIN repeat=kTRUE; counter=0; while(repeat){
#define REPEATBEGINWITHCOUNTER repeat=kTRUE; counter=0; while(repeat && counter < repeatLimit){
#define REPEATFINISH  counter++; } counter = 0;
#define LOOPTERMINALBEGIN(x) Nat4 input[x];
#define LISTROOTBEGIN(x) { V_ListRootNode rootNode[x]; for(counter=0;counter<x;counter++) rootNode[counter] = NULL; counter = 0;
#define LISTROOTFINISH(x,y) root[x] = vpx_flatten_listRootNodes(environment,rootNode[y]);
#define LISTROOTEND }

#define ROOTNULL(x,y) root[x] = vpx_increment_count(y);
#define ROOTEMPTY(x) root[x] = (V_Object) create_list(0,environment);

#define PARAMETERS environment,&repeat,contextClassIndex
#define UNKNOWNOPERATION ;

#define TERMINATEONFAILURE if(result == kFailure) { *inputRepeat = kFALSE; nextCase = kFALSE; goto THEFINISH; }
#define TERMINATEONSUCCESS if(result == kSuccess) { *inputRepeat = kFALSE; nextCase = kFALSE; goto THEFINISH; }
#define FAILONFAILURE if(result == kFailure) { theOutcome = kFailure; *inputRepeat = kFALSE; nextCase = kFALSE; goto THEFINISH; }
#define FAILONSUCCESS if(result == kSuccess) { theOutcome = kFailure; *inputRepeat = kFALSE; nextCase = kFALSE; goto THEFINISH; }
#define CONTINUEONFAILURE if(result == kFailure) { }
#define CONTINUEONSUCCESS if(result == kSuccess) { }
#define FINISHONFAILURE if(result == kFailure) { *inputRepeat = kFALSE; }
#define FINISHONSUCCESS if(result == kSuccess) { *inputRepeat = kFALSE; }
#define NEXTCASEONFAILURE if(result == kFailure) { nextCase = kTRUE; goto THEFINISH; }
#define NEXTCASEONSUCCESS if(result == kSuccess) { nextCase = kTRUE; goto THEFINISH; }

#define INPUT(x,y) root[x] = vpx_increment_count(terminal ## y);
#define OUTPUT(x,y) decrement_count(environment,*root ## x); *root ## x = vpx_increment_count(y);

#define GETINTEGER(x) vpx_get_integer(x)
#define GETREAL(x) vpx_get_real(x)
#define GETPOINTER(v,w,x,y,z) ( void * ) vpx_get_pointer(environment,v,#w,#x,y,z)
#define GETCONSTPOINTER(x,y,z) ( const x y ) vpx_get_const_pointer(environment,#y,z)
#define GETSTRUCTURE(x,y,z) *(( y *) ((V_ExternalBlock) z)->blockPtr)

#define PUTINTEGER(x,y) root[y] = (V_Object) int_to_integer(x,environment);
#define PUTREAL(x,y) root[y] = (V_Object) float_to_real(x,environment);
#define PUTPOINTER(w,x,y,z) root[z] = (V_Object) pointer_to_block(#w,0,strlen(#x),(Nat4) y,environment);
#define PUTSTRUCTURE(w,x,y,z) root[z] = (V_Object) create_externalBlock(#x,w,environment); ((V_ExternalBlock) root[z])->blockPtr = X_malloc(w); *((x *) ((V_ExternalBlock) root[z])->blockPtr) = y;

#ifdef __cplusplus
extern "C" {
#endif

Int4 vpx_spawn_stack(V_Environment environment,char *initial,V_List inputList, V_List outputList,Int4 *success);
enum opTrigger vpx_constant(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int1 *stringValue, V_Object *root);
enum opTrigger vpx_match(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int1 *stringValue, V_Object terminal);
enum opTrigger vpx_instantiate(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int4 classConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_get(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int4 classConstant, V_Object terminal, V_Object *root0, V_Object *root1);
enum opTrigger vpx_set(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int4 valueConstant, V_Object terminal0, V_Object terminal1, V_Object *root0);
enum opTrigger vpx_persistent(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int4 valueConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_primitive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,void *functionPtr,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);

enum opTrigger vpx_extconstant(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int4 enumValue, V_Object *root);
enum opTrigger vpx_extmatch(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int4 enumValue, V_Object terminal);
enum opTrigger vpx_extget(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int1 *tempOString,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_extset(V_Environment environment, enum boolType *repeat, Nat4 contextClassIndex, Int1 *tempOString,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);

enum opTrigger vpx_call_data_method(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int4 methodConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_context_method(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int4 methodConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_context_super_method(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int4 methodConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_evaluate(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int1 *inputString,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);

enum opTrigger vpx_call_inject_get(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int1 *stringConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_inject_set(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int1 *stringConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_inject_persistent(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int1 *stringConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_inject_instance(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int1 *stringConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_inject_method(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int1 *stringConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);
enum opTrigger vpx_call_inject_procedure(V_Environment environment, enum boolType *inputRepeat, Nat4 contextClassIndex,Int1 *stringConstant,Nat4 numberOfTerminals,Nat4 numberOfRoots, ...);

	
Nat4 vpx_multiplex(Nat4 numberOfTerminals, ...);
V_ListRootNode vpx_create_listRootNode(V_Environment environment,V_ListRootNode node, V_Object listRoot);
V_Object vpx_flatten_listRootNodes(V_Environment environment, V_ListRootNode node);
V_Object vpx_increment_count(V_Object object);

Int4 vpx_class_map(V_Environment environment, Int4	classConstant, Int1 *className);
Int4 vpx_method_map(V_Environment environment, Int4	methodConstant, Int1 *methodName);
Int4 vpx_value_map(V_Environment environment, Int4	valueConstant, Int1 *valueName);
Int4 vpx_method_initialize(V_Environment environment, V_Method tempMethod, void *functionPtr, Int1 *textValue);
void *vpx_get_const_pointer(V_Environment environment,Int1* level,V_Object terminal);
void *vpx_get_pointer(V_Environment environment,Nat4 size,Int1 *type,Int1* level,V_Object *object,V_Object terminal);
Real10 vpx_get_real(V_Object object);
Int4 vpx_get_integer(V_Object object);
void *vpx_get_none_pointer(V_Environment environment,Nat4 size,Int1 *type,Int1* level,V_Object *object);

#ifdef __cplusplus
}
#endif

#endif



