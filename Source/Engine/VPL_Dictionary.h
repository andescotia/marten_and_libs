/*
 *  VPL_Dictionary.h
 *  VPL
 *
 *  Created by scott on Sun Mar 03 2002.
 *  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef VPLDICTIONARY
#define VPLDICTIONARY
#include <MartenEngine/MartenEngine.h>

#ifndef __dead2
#define	__dead2
#endif
#ifndef __pure2
#define	__pure2
#endif

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define STRINGTOBJECT
#define VPL_VERBOSE_ENGINE 1

/*
#define TOOLBOX 
#define CARBON
*/

#if (TARGET_API_MAC_CARBON == 1) || (__APPLE_CC__)
	#define CARBON
#else
	#define TOOLBOX
#endif

#ifdef __GNUC__
	#ifndef NULL
	#define NULL ((void *) 0)
	#endif
#endif

#define CODEWARRIOR

#define VPXMALLOC(x,y) ( y * ) X_calloc(x,sizeof(y))
#define VPXREALLOC(x,y,z) ( z * ) X_realloc(x,y,sizeof(z))


#ifdef __cplusplus
extern "C" {
#endif

void *X_malloc(Nat4 size);
void *X_calloc(Nat4 number, Nat4 size);
void *X_realloc(void *theBlock, Nat4 number, Nat4 size);
void X_free(void *block);

void *get_node( V_Dictionary dictionary, Int1 *name );
V_DictionaryNode find_node( V_Dictionary dictionary, Int1 *name );
Nat4 add_node( V_Dictionary dictionary, Int1 *name, void *object );
V_DictionaryNode remove_node( V_Dictionary dictionary, Int1 *name);
Nat4 add_nodes( V_Dictionary dictionary, Nat4 arraySize, VPL_DictionaryNode nodeArray[] );
V_Dictionary create_dictionary(Nat4 numberOfNodes);
Nat4 sort_dictionary(V_Dictionary dictionary );

#ifdef __cplusplus
}
#endif

#endif
