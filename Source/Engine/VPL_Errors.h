#include "VPL_Environment.h"

#ifndef VPLERRORS
#define VPLERRORS

#ifdef __cplusplus
extern "C" {
#endif

V_ErrorNode	VPLLog_V_ErrorNode( V_ErrorNode theError, Bool printFlag, Bool interpreterFlag, V_Environment environment );
void		VPLLog_Environment_Report( Bool printFlag, Bool interpreterFlag, V_Environment environment );
void 		VPLLogError( Int1* outStr, V_Environment environment );
void		VPLLogEngineError( Int1* outStr, Bool interpreterFlag, V_Environment environment );
void		VPLLogString( Int1* outStr, V_Environment environment );
void 		VPLLogStringReturn( Int1* outStr, V_Environment environment );


#ifdef __cplusplus
}
#endif

#endif /* VPLERRORS */

