/*
	
	V_Structures.h
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/
#include "VPL_Environment.h"

#ifndef VPLSTRUCTURES
#define VPLSTRUCTURES

#ifdef __cplusplus
extern "C" {
#endif

Int1 *left_justify( Int1 *string );

Bool object_identity( V_Object objectA , V_Object objectB );
Bool boolean_identity( V_Boolean objectA , V_Boolean objectB );
Bool integer_identity( V_Integer objectA , V_Integer objectB );
Bool real_identity( V_Real objectA , V_Real objectB );
Bool string_identity( V_String stringA , V_String stringB );
Bool list_identity( V_List listA , V_List listB );
Bool instance_identity( V_Instance instanceA , V_Instance instanceB );
Bool external_identity( V_ExternalBlock objectA , V_ExternalBlock objectB );

Int4 object_compare( V_Object objectA , V_Object objectB , Int1 *attribute, V_Environment environment);

Int1 *integer2string( Int4 integer );
Int1 *real2string( Real10 number );

#ifdef __cplusplus
}
#endif

#endif
