/*
	
	VPL_Environment.h
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/

#ifndef VPLENVIRONMENT
#define VPLENVIRONMENT

#define MAXSTACKDEPTH 200

#include "VPL_Dictionary.h"

#include <unistd.h>

#ifdef _POSIX_THREADS
#include <pthread.h>
static pthread_mutex_t gHeapLock; 
static pthread_mutex_t gStackLock; 
static pthread_mutex_t gCallbackLock; 
#endif

enum multiplexType {
	kEmptyList = 0,
	kSingleton = 1,
	kPlural = 2
};

typedef struct VPL_Frame *V_Frame;
typedef struct VPL_RootNode *V_RootNode;

typedef struct VPL_Fault
{
	Nat4			theFaultCode;	/* Must be cast as Nat4 because will be used as an index into an array */
	Int1			*primaryString;
	Int1			*secondaryString;
	Int1			*operationName;
	Int1			*moduleName;
}	VPL_Fault;

typedef struct
{
	Int1 *newPointer;
	Int1 *oldPointer;
	struct VPL_PointerRecord *next;
}	VPL_PointerRecord, *V_PointerRecord;

enum valueNodeType {
	kUnknownNode,
	kAttributeNode,
	kPersistentNode
};

typedef struct VPL_WatchpointNode *V_WatchpointNode;
typedef struct VPL_WatchpointNode
{
	V_WatchpointNode	previous;
	V_WatchpointNode	next;
	V_Instance			theInstance;
}	VPL_WatchpointNode;

typedef struct VPL_ValueNode *V_ValueNode;
typedef struct VPL_ValueNode
{
	Nat4				type;
	Nat4				attributeOffset;
	V_Value				value;
	V_WatchpointNode	watchpointList;
}	VPL_ValueNode;


typedef struct VPL_ClassEntry *V_ClassEntry;
typedef struct VPL_ClassEntry
{
	Nat4			valid;
	Nat4			constructorIndex;
	Nat4			destructorIndex;
	Nat4			numberOfSuperClasses;
	Nat4			*superClasses;
	V_Class			theClass;
	V_Method		*methods;
	V_ValueNode		*values;
}	VPL_ClassEntry;

typedef struct VPL_ClassIndexTable
{
	Nat4			numberOfClasses;
	V_ClassEntry	*classes;
}	VPL_ClassIndexTable;

typedef struct VPL_RootNode
{
	V_RootNode		previousNode;
	V_RootNode		nextNode;
	V_Root			root;
	V_Object		object;
}	VPL_RootNode;

typedef struct VPL_Frame
{
	V_Frame		previousFrame;
	V_List		inputList;
	V_List		outputList;
	V_Case		frameCase;		/* For use in the interpreter */
	Int1		*methodName;	/* For use in calling "super" data driven methods and interpreter */
	Nat4		dataDriver;		/* For use in calling "super" data driven methods and interpreter */
	Nat4		vplEditor;		/* For use in the interpreter */
	V_Operation	operation;
	Nat1		repeatFlag;
	Nat1		methodSuccess;
	Nat1		stopNow;
	Nat1		stopNext;
	Nat4		caseCounter;
	Nat4		repeatCounter;
	V_RootNode	roots;
}	VPL_Frame;

typedef struct VPL_Stack
{
	V_Stack		previousStack;
	V_Stack		nextStack;
	Int4		stackDepth;
	Nat4		maxStackDepth;
	V_Frame		currentFrame;
	V_Object	vplStack;		/* For use in the interpreter */
}	VPL_Stack;

typedef struct VPL_ErrorNode
{
	V_ErrorNode	previous;
	Int1		*errorString;
	Int4		errorCode;
}	VPL_ErrorNode;


typedef struct VPL_CallbackCodeSegment
{
	Nat4					loadHighAddress;
	Nat4					orAddress;
	Nat4					loadHighOp;
	Nat4					orOp;
	Nat4					mtctrOp;
	Nat4					branchOp;
	Int1					*vplRoutine;
	V_ExtProcedure			callbackFunction;
	V_Environment			theEnvironment;
	Nat4					listInput;
	V_Object				object;
	V_CallbackCodeSegment	previous;
	V_CallbackCodeSegment	next;
	Nat4					use;
}	VPL_CallbackCodeSegment;

#ifdef __cplusplus
extern "C" {
#endif

Int1 *new_string( Int1 *string , V_Environment environment );
Int1 *new_cat_string( Int1 *firstString, Int1 *secondString , V_Environment environment );

Int4 record_error(Int1 *error, Int1 *module, Int4 errorCode, V_Environment environment);
Int4 dispose_fault(V_Environment environment);
enum opTrigger record_fault(V_Environment environment, enum faultCode faultCode, Int1 *primaryString, Int1 *secondaryString, Int1 *operationName, Int1 *moduleName);
V_Stack createStack(void);
Nat4 addStack(V_Environment ptrE, V_Stack stack);
V_Environment createEnvironment(void);
V_Heap createHeap(void);
Nat4 sort_dictionaries(V_Environment environment );

#ifdef __cplusplus
}
#endif

#endif
