/*
	
	VPL_Evaluate.c
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/
#include "VPL_Execution.h"
#include "VPL_PrimUtility.h"
#include <string.h>
#include <math.h>

unsigned long get_terminal(char theChar);
unsigned long get_terminal(char theChar)
{
	char tempChar = tolower(theChar);
	
	return tempChar - 'a';
}

V_EvalToken create_token(enum tokenType type,enum operatorType operationType,V_EvalExpression expression,Int4 integerValue,Real10 realValue);
V_EvalToken create_token(enum tokenType type,enum operatorType operationType,V_EvalExpression expression,Int4 integerValue,Real10 realValue)
{
	V_EvalToken	theToken = (V_EvalToken) X_malloc(sizeof(VPL_EvalToken));
	theToken->type = type;
	theToken->operationType = operationType;
	theToken->expression = expression;
	theToken->integerValue = integerValue;
	theToken->realValue = realValue;
	theToken->previous = NULL;
	theToken->next = NULL;
	
	return theToken;
}

V_EvalTokenList create_tokenList(void);
V_EvalTokenList create_tokenList(void)
{
	V_EvalTokenList	theTokenList = (V_EvalTokenList) X_malloc(sizeof(VPL_EvalTokenList));
	theTokenList->first = NULL;
	theTokenList->last = NULL;
	
	return theTokenList;
}

Int4 add_token(V_EvalTokenList tokenList,V_EvalToken token)
{
	V_EvalToken	tempToken = NULL;
	
	if(!token) return 0;
	else if(token->type == kTokenTypeExpression){
		add_token(tokenList,token->expression->before);
		add_token(tokenList,token->expression->after);
		add_token(tokenList,token->expression->operation);
	} else {
		if(!tokenList->first) tokenList->first = token;
		tempToken = tokenList->last;
		tokenList->last = token;
		token->previous = tempToken;
		token->next = NULL;
		if(tempToken) tempToken->next = token;
	}
	
	return 0;
}

V_EvalTokenList	parse_string(V_Environment environment,V_Stack stack,Int1* testString)
{
	VPL_EvalTokenList	*theInitialTokenList;
	V_EvalToken			theToken;
	Int1				theCharacter = '\0';
	Nat4				counter = 0;
	Nat4				stringLength = strlen(testString);
	Int1*				scratchPad = (Int1 *) X_malloc(stringLength*sizeof(Int1));
	Int4				theIntegerValue = 0;
	Real10				theRealValue = 0.0;
	Int1*				tempPad = NULL;
	Bool				isReal = kFALSE;
	V_Object			tempObject = NULL;
	
	memset(scratchPad,0,stringLength);
	
	theInitialTokenList = create_tokenList();
	theToken = create_token(kTokenTypeLParen,kOperatorTypeNoop,NULL,0,0.0);
	add_token(theInitialTokenList,theToken);
	
	for(counter = 0;counter < stringLength; counter++){
		theCharacter = testString[counter];
		if(isalpha(theCharacter)){
			tempObject = VPLEngineGetInputObject(get_terminal(theCharacter),stack->currentFrame,environment);
			if(!tempObject){
				theToken = create_token(kTokenTypeIntegerConstant,kOperatorTypeNoop,NULL,0,0.0);
			} else if (tempObject->type == kInteger){
				theToken = create_token(kTokenTypeIntegerConstant,kOperatorTypeNoop,NULL,((V_Integer) tempObject)->value,0.0);
			} else if (tempObject->type == kReal){
				theToken = create_token(kTokenTypeRealConstant,kOperatorTypeNoop,NULL,0,((V_Real) tempObject)->value);
			} else if (tempObject->type == kBoolean){
				theToken = create_token(kTokenTypeIntegerConstant,kOperatorTypeNoop,NULL,((V_Boolean) tempObject)->value,0.0);
			} else {
				record_error("evaluate: Invalid input: ",NULL,kERROR,environment);
				return NULL;
			}
		} else if(isdigit(theCharacter)){
			tempPad = scratchPad;
			while(isdigit(theCharacter) || theCharacter == '.'){
				if(theCharacter == '.') isReal = kTRUE;
				*tempPad++ = theCharacter;
				theCharacter = testString[++counter];
			}
			if(isReal) {
				theRealValue = atof(scratchPad);
				theToken = create_token(kTokenTypeRealConstant,kOperatorTypeNoop,NULL,0,theRealValue);
			} else {
				theIntegerValue = atoi(scratchPad);
				theToken = create_token(kTokenTypeIntegerConstant,kOperatorTypeNoop,NULL,theIntegerValue,0.0);
			}
			memset(scratchPad,0,stringLength);
			counter--;
		} else switch(theCharacter){
			case '(':
				theToken = create_token(kTokenTypeLParen,kOperatorTypeNoop,NULL,0,0.0);
				break;
				
			case ')':
				theToken = create_token(kTokenTypeRParen,kOperatorTypeNoop,NULL,0,0.0);
				break;
				
			case '@':
				theToken = create_token(kTokenTypeOperator,kOperatorTypeExponent,NULL,0,0.0);
				break;
				
			case '+':
				theToken = theInitialTokenList->last;
				if(theToken->type == kTokenTypeOperator || 
					theToken->type == kTokenTypeLParen) theToken = create_token(kTokenTypeOperator,kOperatorTypePositive,NULL,0,0.0);
				else theToken = create_token(kTokenTypeOperator,kOperatorTypeAddition,NULL,0,0.0);
				break;
				
			case '-':
				theToken = theInitialTokenList->last;
				if(theToken->type == kTokenTypeOperator || 
					theToken->type == kTokenTypeLParen) theToken = create_token(kTokenTypeOperator,kOperatorTypeNegative,NULL,0,0.0);
				else theToken = create_token(kTokenTypeOperator,kOperatorTypeSubtraction,NULL,0,0.0);
				break;
				
			case '*':
				theToken = create_token(kTokenTypeOperator,kOperatorTypeMultiplication,NULL,0,0.0);
				break;
				
			case '/':
				if(testString[counter+1] == '/') {
					theToken = create_token(kTokenTypeOperator,kOperatorTypeIntegerDivision,NULL,0,0.0);
					counter++;
				} else theToken = create_token(kTokenTypeOperator,kOperatorTypeDivision,NULL,0,0.0);
				break;
				
			case '%':
				theToken = create_token(kTokenTypeOperator,kOperatorTypeRemainder,NULL,0,0.0);
				break;
				
			case '&':
				theToken = create_token(kTokenTypeOperator,kOperatorTypeBitwiseAnd,NULL,0,0.0);
				break;
				
			case '|':
				theToken = create_token(kTokenTypeOperator,kOperatorTypeBitwiseOr,NULL,0,0.0);
				break;
				
			case '^':
				theToken = create_token(kTokenTypeOperator,kOperatorTypeBitwiseXor,NULL,0,0.0);
				break;
				
			case '~':
				theToken = create_token(kTokenTypeOperator,kOperatorTypeBitwiseNot,NULL,0,0.0);
				break;
				
			case '<':
				if(testString[counter+1] == '<') {
					theToken = create_token(kTokenTypeOperator,kOperatorTypeBitShiftLeft,NULL,0,0.0);
					counter++;
				}
				break;
				
			case '>':
				if(testString[counter+1] == '>') {
					theToken = create_token(kTokenTypeOperator,kOperatorTypeBitShiftRight,NULL,0,0.0);
					counter++;
				}
				break;
				
			default:
				continue;
		}
		add_token(theInitialTokenList,theToken);
	}
	theToken = create_token(kTokenTypeRParen,kOperatorTypeNoop,NULL,0,0.0);
	add_token(theInitialTokenList,theToken);
	
	return theInitialTokenList;
}

V_EvalToken reduce_binary(V_EvalToken* theFirstToken,V_EvalToken* theLastToken);
V_EvalToken reduce_binary(V_EvalToken* theFirstToken,V_EvalToken* theLastToken)
{
	V_EvalToken			theToken = *theFirstToken;
	V_EvalExpression	theExpression = (V_EvalExpression) X_malloc(sizeof(VPL_EvalExpression));

	if(	theToken->operationType == kOperatorTypePositive ||
		theToken->operationType == kOperatorTypeNegative ||
		theToken->operationType == kOperatorTypeBitwiseNot) {
		theExpression->before = NULL;
		theExpression->after = theToken->next;
		theExpression->operation = theToken;
		*theFirstToken = theToken;
		*theLastToken = theToken->next;
	} else {
		theExpression->before = theToken->previous;
		theExpression->after = theToken->next;
		theExpression->operation = theToken;
		*theFirstToken = theToken->previous;
		*theLastToken = theToken->next;
	}
	
	theToken = create_token(kTokenTypeExpression,kOperatorTypeNoop,theExpression,0,0.0);
	return theToken;
}
	
V_EvalToken reduce_simple(V_EvalToken theFirstToken,V_EvalToken theLastToken);
V_EvalToken reduce_simple(V_EvalToken theFirstToken,V_EvalToken theLastToken)
{
#pragma unused(theLastToken)

	V_EvalToken	theToken = theFirstToken;
	V_EvalToken	theReplacementToken = NULL;
	V_EvalToken	theRightToken = NULL;
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && 
			(theToken->operationType == kOperatorTypePositive ||
			theToken->operationType == kOperatorTypeNegative ||
			theToken->operationType == kOperatorTypeBitwiseNot))
		{
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && theToken->operationType == kOperatorTypeExponent) {
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && 
			(theToken->operationType == kOperatorTypeMultiplication ||
			theToken->operationType == kOperatorTypeIntegerDivision ||
			theToken->operationType == kOperatorTypeRemainder ||
			theToken->operationType == kOperatorTypeDivision))
		{
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && 
			(theToken->operationType == kOperatorTypeAddition ||
			theToken->operationType == kOperatorTypeSubtraction))
		{
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && 
			(theToken->operationType == kOperatorTypeBitShiftLeft ||
			theToken->operationType == kOperatorTypeBitShiftRight))
		{
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && 
			(theToken->operationType == kOperatorTypeBitwiseAnd))
		{
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && 
			(theToken->operationType == kOperatorTypeBitwiseXor))
		{
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	theToken = theFirstToken;
	while(theToken->type != kTokenTypeRParen){
		if(theToken->type == kTokenTypeOperator && 
			(theToken->operationType == kOperatorTypeBitwiseOr))
		{
			theReplacementToken = reduce_binary(&theToken,&theRightToken);
			theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	
	return theReplacementToken;
}

V_EvalToken reduce_complex(V_EvalToken theFirstToken,V_EvalToken theLastToken)
{
#pragma unused(theLastToken)

	V_EvalToken	theToken = theFirstToken;
	V_EvalToken	theReplacementToken = NULL;
	V_EvalToken	theRightToken = NULL;
	V_EvalToken	theLeftToken = NULL;
	
	theToken = theFirstToken;
	
	while(theToken){
		while(theToken->type != kTokenTypeRParen){
			theToken = theToken->next;
		}
	
		theRightToken = theToken;
	
		while(theToken->type != kTokenTypeLParen){
			theToken = theToken->previous;
		}
	
		theLeftToken = theToken;
	
		theReplacementToken = reduce_simple(theLeftToken,theRightToken);
		if(theLeftToken->previous) theLeftToken->previous->next = theReplacementToken;
		theReplacementToken->previous = theToken->previous;
		if(theRightToken->next) theRightToken->next->previous = theReplacementToken;
		theReplacementToken->next = theRightToken->next;
	
		theToken = theReplacementToken->next;
	}
	
	return theReplacementToken;
}

V_EvalToken execute_token(V_EvalToken* theFirstToken,V_EvalToken* theLastToken);
V_EvalToken execute_token(V_EvalToken* theFirstToken,V_EvalToken* theLastToken)
{
	V_EvalToken			theToken = *theFirstToken;
	Real10				realLeftValue = 0.0;
	Real10				realRightValue = theToken->previous->realValue;
	Real10				realResult = 0.0;
	Int4				integerLeftValue = 0;
	Int4				integerRightValue = theToken->previous->integerValue;
	Int4				integerResult = 0;
	enum tokenType		leftType = kTokenTypeRealConstant;
	enum tokenType		rightType = theToken->previous->type;
	enum tokenType		resultType = kTokenTypeRealConstant;
	Nat4				leftBitString = 0;
	Nat4				rightBitString = integerRightValue;
	
	*theFirstToken = theToken->previous->previous;
	*theLastToken = theToken;
	
	if(theToken->previous->previous) {
		realLeftValue = theToken->previous->previous->realValue;
		integerLeftValue = theToken->previous->previous->integerValue;
		leftBitString = integerLeftValue;
		leftType = theToken->previous->previous->type;
	}

	switch(theToken->operationType){
		case kOperatorTypeExponent:
			if(rightType == kTokenTypeIntegerConstant) realRightValue = integerRightValue;
			if(leftType == kTokenTypeIntegerConstant) realLeftValue = integerLeftValue;
			realResult = pow(realLeftValue,realRightValue);
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = realResult;
				resultType = kTokenTypeIntegerConstant;
			}
			break;

		case kOperatorTypeMultiplication:
			if(rightType == kTokenTypeIntegerConstant) realRightValue = integerRightValue;
			if(leftType == kTokenTypeIntegerConstant) realLeftValue = integerLeftValue;
			realResult = realRightValue*realLeftValue;
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = integerRightValue*integerLeftValue;
				resultType = kTokenTypeIntegerConstant;
			}
			break;

		case kOperatorTypeDivision:
			if(rightType == kTokenTypeIntegerConstant) realRightValue = integerRightValue;
			if(leftType == kTokenTypeIntegerConstant) realLeftValue = integerLeftValue;
			realResult = realLeftValue/realRightValue;
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = integerLeftValue%integerRightValue;
				if(integerResult == 0) {
					integerResult = integerLeftValue/integerRightValue;
					resultType = kTokenTypeIntegerConstant;
				}
			}
			break;

		case kOperatorTypeAddition:
			if(rightType == kTokenTypeIntegerConstant) realRightValue = integerRightValue;
			if(leftType == kTokenTypeIntegerConstant) realLeftValue = integerLeftValue;
			realResult = realLeftValue+realRightValue;
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = integerLeftValue+integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			}
			break;

		case kOperatorTypeSubtraction:
			if(rightType == kTokenTypeIntegerConstant) realRightValue = integerRightValue;
			if(leftType == kTokenTypeIntegerConstant) realLeftValue = integerLeftValue;
			realResult = realLeftValue-realRightValue;
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = integerLeftValue-integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			}
			break;

		case kOperatorTypePositive:
			if(rightType == kTokenTypeIntegerConstant) realRightValue = integerRightValue;
			realResult = (+1.0) * realRightValue;
			if(rightType == kTokenTypeIntegerConstant) {
				integerResult = (+1) * integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			}
			break;

		case kOperatorTypeNegative:
			if(rightType == kTokenTypeIntegerConstant) realRightValue = integerRightValue;
			realResult = (-1.0) * realRightValue;
			if(rightType == kTokenTypeIntegerConstant) {
				integerResult = (-1) * integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			}
			break;

		case kOperatorTypeIntegerDivision:
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = integerLeftValue/integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeRemainder:
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = integerLeftValue%integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeBitwiseAnd:
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = leftBitString&rightBitString;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeBitwiseOr:
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = leftBitString|rightBitString;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeBitwiseXor:
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = leftBitString^rightBitString;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeBitwiseNot:
			if(rightType == kTokenTypeIntegerConstant) {
				integerResult = !rightBitString;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeBitShiftLeft:
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = leftBitString<<integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeBitShiftRight:
			if(rightType == kTokenTypeIntegerConstant && leftType == kTokenTypeIntegerConstant) {
				integerResult = leftBitString>>integerRightValue;
				resultType = kTokenTypeIntegerConstant;
			} else return NULL;
			break;

		case kOperatorTypeNoop:
			return NULL;
			break;

	}
	
	theToken = create_token(resultType,kOperatorTypeNoop,NULL,integerResult,realResult);
	return theToken;
}
	
V_EvalToken execute_tokenList(V_EvalToken theFirstToken)
{
	V_EvalToken	theToken = theFirstToken;
	V_EvalToken	theReplacementToken = NULL;
	V_EvalToken	theRightToken = NULL;
	
	theToken = theFirstToken;
	
	while(theToken){
		if(theToken->type == kTokenTypeOperator) {
			theReplacementToken = execute_token(&theToken,&theRightToken);
			if(!theReplacementToken) return NULL;
			if(theToken->previous) theToken->previous->next = theReplacementToken;
			theReplacementToken->previous = theToken->previous;
			if(theRightToken->next) theRightToken->next->previous = theReplacementToken;
			theReplacementToken->next = theRightToken->next;
		} else theReplacementToken = theToken;
		theToken = theReplacementToken->next;
	}
	
	return theReplacementToken;
}

