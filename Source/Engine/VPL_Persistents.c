/*
	
	VPL_Methods.c
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/
#include <stdlib.h>
#include <string.h>
#include <CoreServices/CoreServices.h>

#include "VPL_Persistents.h"

/* Persistents routines */

Nat4 get_value_index(V_Environment environment,Int1 *valueName)
{
	Nat4				counter = 0;
	V_Dictionary		values = environment->valuesDictionary;
	Nat4				columnIndex = environment->totalValues;
	V_DictionaryNode	theValueNode = find_node(values,valueName);

	if(theValueNode) {
		columnIndex = (Nat4) theValueNode->object;
	} else {
		add_node(values,new_string(valueName,environment),(void *) columnIndex);
		if(environment->postLoad) sort_dictionary(values);
		environment->totalValues++;
		for(counter=0;counter<environment->classIndexTable->numberOfClasses;counter++){
			V_ClassEntry tempClassEntry = environment->classIndexTable->classes[counter];
			tempClassEntry->values = VPXREALLOC(tempClassEntry->values,environment->totalValues,V_ValueNode);
			tempClassEntry->values[columnIndex] = NULL;
		}
	}

	return columnIndex;
}

V_Value install_persistent_indices(V_Value operation,V_Environment environment,Int1 *name)
{
	Nat4				slashPosition = 0;
	Int1*				valueName = NULL;
	Int1*				className = NULL;

	if(name) {
		slashPosition = strcspn(name,"/");
		if(slashPosition == strlen(name)){
			valueName = name;
			operation->classIndex = 0;
		} else {
			valueName = name+slashPosition+1;
			className = VPXMALLOC(slashPosition + 1,Int1);
			className = strncpy(className,name,slashPosition);
			className[slashPosition] = '\0';
			operation->classIndex = VPL_get_class_index(environment,className,kFALSE);
			X_free(className);
		}
					
		operation->columnIndex = get_value_index(environment,valueName);
		
	} else {
		operation->classIndex = -1;
		operation->columnIndex = -1;
	}
	return operation;
}

V_Object get_persistent( V_Environment environment, Int1 *name )
{
	V_Value ptrM = NULL;
	V_Dictionary ptrT = NULL;
	
	ptrT = environment->persistentsTable;
	ptrM = get_persistentNode(ptrT,name);
	if(ptrM != NULL){
		return (ptrM->value);
	}
	
	record_error("Get persistent failure for: ",name,kERROR,environment);
	return NULL;

}

Nat4 set_persistent( V_Environment environment, Int1 *name, V_Object value )
{
	V_Value ptrM = NULL;
	V_Dictionary ptrT = NULL;

	ptrT = environment->persistentsTable;
	ptrM = get_persistentNode(ptrT,name);
	if(ptrM != NULL){
			ptrM->value  = value; /* Callers should take care of refcounts */
			return kNOERROR;
	}
	
	record_error("Set persistent failure for: ",name,kERROR,environment);
	return kERROR;

}

Nat4 destroy_persistentsTable(V_Dictionary ptrT,V_Environment environment)
{
	destroy_persistentsList(ptrT,environment);
	X_free(ptrT);
	return kNOERROR;
}

Nat4 destroy_persistentsList(V_Dictionary ptrP,V_Environment environment)
{
	V_DictionaryNode	*tempPersistentPtr = NULL;
	Nat4		counter = 0;

	if(ptrP->numberOfNodes != 0){
		tempPersistentPtr = ptrP->nodes;
		for(counter = 0; counter < ptrP->numberOfNodes; counter++)
		{
			if(*tempPersistentPtr != NULL) {
				destroy_persistentNode((V_Value)((*tempPersistentPtr)->object),environment);
				X_free(*tempPersistentPtr++);
			}
		}
	}
	X_free(ptrP->nodes);
	ptrP->nodes = NULL;
	ptrP->numberOfNodes = 0;
	return kNOERROR;
}

Nat4 destroy_persistentNode(V_Value ptrC,V_Environment environment)
{
	V_ClassEntry	theClassEntry = NULL;
	Nat4			columnIndex = 0;
	if(ptrC == NULL) return kNOERROR;
	columnIndex = ptrC->columnIndex;
	
	theClassEntry = environment->classIndexTable->classes[0];
	if(theClassEntry->values[columnIndex]){
		V_WatchpointNode watchpointNode = theClassEntry->values[columnIndex]->watchpointList;
		while(watchpointNode){
			V_WatchpointNode tempWatchpointNode = watchpointNode->previous;
			X_free(watchpointNode);
			watchpointNode = tempWatchpointNode;
		}
		theClassEntry->values[columnIndex]->watchpointList = NULL;
	}
	X_free(theClassEntry->values[columnIndex]);
	theClassEntry->values[columnIndex] = NULL;

	if(ptrC->value != NULL) decrement_count(environment,ptrC->value);
	if(ptrC->valueArchive != NULL) X_free(ptrC->valueArchive);
	X_free(ptrC->objectName);
	X_free(ptrC);
	return kNOERROR;
}

V_Value create_persistentNode( Int1 *persistentName, Nat4 archiveList[],V_Environment environment)
{
	V_ClassEntry		theClassEntry = NULL;
	Nat4				counter = 0;
	Nat4				tempInt = 0;
	Nat4				*tempPointer = NULL;
	Nat4				size = 0;

	V_Value thePersistent = VPXMALLOC(1,VPL_Value);
	
	thePersistent->objectName = new_string(persistentName,environment);
	thePersistent->classIndex = -1;
	thePersistent->columnIndex = -1;
	thePersistent->valueArchive = archive_copy(environment,(V_Archive) archiveList);
	thePersistent->value = NULL;
	size = archive_size(environment,thePersistent->valueArchive)/4;
	tempPointer = (Nat4 *) thePersistent->valueArchive;
	for(counter=0;counter<size;counter++){
	    tempInt = CFSwapInt32HostToBig(*(tempPointer + counter));
		*(tempPointer + counter) = tempInt;
	}
	
	install_persistent_indices(thePersistent,environment,persistentName);

	theClassEntry = environment->classIndexTable->classes[thePersistent->classIndex];
	theClassEntry->values[thePersistent->columnIndex] = VPXMALLOC(1,VPL_ValueNode);
	theClassEntry->values[thePersistent->columnIndex]->type = kPersistentNode;
	theClassEntry->values[thePersistent->columnIndex]->attributeOffset = 0;
	theClassEntry->values[thePersistent->columnIndex]->value = thePersistent;
	theClassEntry->values[thePersistent->columnIndex]->watchpointList = NULL;

	return thePersistent;
}

V_Value persistent_unarchive(V_Value persistent,V_Environment environment)
{
	decrement_count(environment,persistent->value);
	persistent->value = unarchive_object(environment,(V_Archive) persistent->valueArchive);
	
	return persistent;
}

V_Value get_persistentNode( V_Dictionary ptrT, Int1 *name )
{
        return (V_Value) get_node(ptrT,name);
}

