/*
	
	VPL_Operations.c
	Copyright 2000 Scott B. Anderson, All Rights Reserved.
	
*/
#include <stdlib.h>
#include <string.h>

#include "VPL_Operations.h"

extern V_Object decrement_count(V_Environment environment,V_Object ptrO);
extern Nat4 VPL_get_class_index(V_Environment environment,Int1* className,Bool valid);

Nat4 connect_to_terminal( V_Case ptrA, V_Terminal terminal, Nat4 rootdex, Nat4 upstreamIndex)
{
	V_Operation upstream = NULL;
	
	if(rootdex == 0) return kNOERROR;
	upstream = (ptrA->operationsList)[upstreamIndex];
	if(upstream == (V_Operation) terminal->operation){
		terminal->loopRoot = (*(upstream->outputList + (rootdex-1)));
		terminal->mode = iLoop;
		terminal->loopRootIndex = rootdex;
		(*(upstream->outputList + (rootdex-1)))->mode = iLoop;
	} else {
		terminal->root = (*(upstream->outputList + (rootdex-1)));
		terminal->rootIndex = rootdex;
		terminal->rootOperation = upstreamIndex;
	}

	return kNOERROR;
}

Nat4 connect_root_to_terminal( V_Case ptrA, Nat4 rootdex, Nat4 upstreamIndex, Nat4 termdex, Nat4 downstreamIndex )
{
	V_Operation downstream = NULL;
	
	downstream = (ptrA->operationsList)[downstreamIndex];
	connect_to_terminal(ptrA,(*(downstream->inputList + (termdex-1))),rootdex,upstreamIndex);

	return kNOERROR;
}

V_Root create_root(Nat4 index,V_Operation operation)
{
	V_Root ptrA = (V_Root) X_malloc(sizeof (VPL_Root));
	ptrA->mode = iSimple;
	ptrA->index = index;
	ptrA->operation = operation;
	ptrA->tempObject = NULL;
	
	return ptrA;
}

Nat4 destroy_root(V_Root ptrP)
{
	X_free(ptrP);
	return kNOERROR;
}

V_Terminal create_terminal(Nat4 offset,V_Operation operation)
{
	V_Terminal ptrA = (V_Terminal) X_malloc(sizeof (VPL_Terminal));
	ptrA->mode = iSimple;
	ptrA->offset = offset;
	ptrA->root = NULL;
	ptrA->rootIndex = 0;
	ptrA->rootOperation = 0;
	ptrA->loopRoot = NULL;
	ptrA->loopRootIndex = 0;
	ptrA->operation = operation;
	ptrA->tempObject = NULL;
	
	return ptrA;
}

Nat4 destroy_terminal(V_Terminal ptrP)
{
	X_free(ptrP);
	return kNOERROR;
}

V_Case create_case( Nat4 index )
{
#pragma unused(index)

	V_Case theCase = (V_Case) X_malloc(sizeof (VPL_Case));
	theCase->parentMethod = NULL;
	theCase->numberOfOperations = 0;
	theCase->operationsList = NULL;
	theCase->vplCase = 0;

	return theCase;
}

Nat4 destroy_case( V_Environment environment,V_Case ptrP)
{

	destroy_operationsList(environment,ptrP);
	X_free(ptrP);
	return kNOERROR;
}

Nat4 install_operation(V_Operation ptrP,Nat4 type,Nat4 inarity,Nat4 outarity,Bool repeat,Nat2 action,Bool trigger)
{
	V_Terminal	*tempTerminalPtr = NULL;
	V_Root		*tempRootPtr = NULL;
	Nat4		counter = 0;

	ptrP->type = type;

	ptrP->inarity = inarity;
	if(inarity){
		ptrP->inputList = (V_Terminal *) calloc(inarity,sizeof (V_Terminal));
		tempTerminalPtr = ptrP->inputList;
		for(counter = 0; counter < inarity; counter++)
		{
			*tempTerminalPtr++ = create_terminal(counter,ptrP);
		}
	}
	else ptrP->inputList = NULL;

	ptrP->outarity = outarity;
	if(outarity){
		ptrP->outputList = (V_Root *) calloc(outarity,sizeof (V_Root));
		tempRootPtr = ptrP->outputList;
		for(counter = 0; counter < outarity; counter++)
		{
			*tempRootPtr++ = create_root(counter,ptrP);
		}
	}
	else ptrP->outputList = NULL;

	ptrP->repeat = repeat;
	ptrP->action = action;
	ptrP->trigger = trigger;
	ptrP->classIndex = -1;
	ptrP->columnIndex = -1;

	return kNOERROR;
}

Nat4 install_named(V_Environment environment, V_Operation theOperation,Nat4 type,Nat4 inarity,Nat4 outarity,Bool repeat,Nat2 action,Bool trigger,Nat4 inject,Int1 *name)
{
	install_operation(theOperation,type,inarity,outarity,repeat,action,trigger);
	theOperation->inject = inject;
	theOperation->objectName = new_string(name,environment);;

	return kNOERROR;
}

Nat4 destroy_method( V_Environment environment,V_Method ptrC)
{
	if(ptrC == NULL) return kNOERROR;
	destroy_casesList(environment,ptrC);
	X_free(ptrC->objectName);
	X_free(ptrC);
	return kNOERROR;
}

Nat4 destroy_operation( V_Environment environment,V_Operation ptrP)
{

	if(ptrP == NULL) return kNOERROR;
	if(ptrP->type == kLocal) {
		destroy_casesList(environment,(V_Method) ptrP);
	}
	ptrP->casesList = NULL;
	ptrP->numberOfCases =  0;
	destroy_inputList(ptrP);
	destroy_outputList(ptrP);

	decrement_count(environment,ptrP->object);

	X_free(ptrP->objectName);
	X_free(ptrP);
	return kNOERROR;
}

V_Operation create_operation(Nat4 index,V_Case opCase)
{
	V_Operation	ptrP = (V_Operation) X_malloc(sizeof (VPL_Operation));
	if(ptrP == NULL) return NULL;
	
	ptrP->objectName = NULL;	/* Used by Persistent, Universal, Instantiate, Get, Set */
	ptrP->classIndex = -1;
	ptrP->columnIndex = -1;
	ptrP->inarity = 0;
	ptrP->outarity = 0;
	ptrP->numberOfCases = 0;
	ptrP->casesList = NULL;
	
/* End of base class field initialization */	
	
	ptrP->owner = (struct VPL_Case *) opCase;
	ptrP->index = index;
	ptrP->type = kOperation;
	ptrP->inputList = NULL;
	ptrP->outputList = NULL;

	ptrP->repeat = kFALSE;
	ptrP->action = kContinue;
	ptrP->trigger = kSuccess;

	ptrP->breakPoint = kFALSE;
	ptrP->object = NULL; /* Used by Constant, Match */
	ptrP->integer = 0; /* Used by Constant, Match */
	ptrP->inject = 0;	/* Index of inject terminal used to create name */
	ptrP->functionPtr = NULL;	/* Used by Primitive */
	ptrP->callSuper = kFALSE;
	ptrP->valueType = kNormal;
	ptrP->vplOperation = 0;

	return ptrP;
}

Nat4 destroy_inputList(V_Operation ptrP)
{
	V_Terminal	*tempTerminalPtr = NULL;
	Nat4		counter = 0;
	if(ptrP->inarity != 0){
		tempTerminalPtr = ptrP->inputList;
		for(counter = 0; counter < ptrP->inarity; counter++)
		{
			if(*tempTerminalPtr != NULL) destroy_terminal(*tempTerminalPtr++);
		}
	}
	X_free(ptrP->inputList);
	ptrP->inputList = NULL;
	ptrP->inarity = 0;
	return kNOERROR;
}

Nat4 destroy_outputList(V_Operation ptrP)
{
	V_Root	*tempRootPtr = NULL;
	Nat4		counter = 0;
	if(ptrP->outarity != 0){
		tempRootPtr = ptrP->outputList;
		for(counter = 0; counter < ptrP->outarity; counter++)
		{
			if(*tempRootPtr != NULL) destroy_root(*tempRootPtr++);
		}
	}
	X_free(ptrP->outputList);
	ptrP->outputList = NULL;
	ptrP->outarity = 0;
	return kNOERROR;
}

Nat4 destroy_casesList( V_Environment environment,V_Method ptrP)
{
	V_Case	*tempCasePtr = NULL;
	Nat4		counter = 0;
	if(ptrP->numberOfCases != 0){
		tempCasePtr = (V_Case *) ptrP->casesList;
		for(counter = 0; counter < ptrP->numberOfCases; counter++)
		{
			destroy_case(environment,*tempCasePtr++);
		}
	}
	X_free(ptrP->casesList);
	ptrP->casesList = NULL;
	ptrP->numberOfCases = 0;
	return kNOERROR;
}

Nat4 destroy_operationsList( V_Environment environment,V_Case ptrP)
{
	V_Operation	*tempOperationPtr = NULL;
	Nat4		counter = 0;
	if(ptrP->numberOfOperations != 0){
		tempOperationPtr = ptrP->operationsList;
		for(counter = 0; counter < ptrP->numberOfOperations; counter++)
		{
			if(*tempOperationPtr != NULL) {
						destroy_operation(environment,*tempOperationPtr++);
			}
		}
	}
	X_free(ptrP->operationsList);
	ptrP->operationsList = NULL;
	ptrP->numberOfOperations = 0;
	return kNOERROR;
}

