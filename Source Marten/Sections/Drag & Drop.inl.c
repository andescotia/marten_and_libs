/* A VPL Section File */
/*

Drag & Drop.inl.c
Copyright: 2004 Andescotia LLC

*/

#include "MartenInlineProject.h"




	Nat4 tempAttribute_Drag_20_Reference_2F_Drag_20_Input_20_Callback[] = {
0000000000, 0X00000310, 0X0000009C, 0X00000022, 0X00000014, 0X000000FC, 0X00000378, 0X000000E8,
0X0000034C, 0X000000E4, 0X000000E0, 0X00000304, 0X000002EC, 0X000002CC, 0X000002D4, 0X000002B4,
0X000002AC, 0X000000DC, 0X0000027C, 0X00000258, 0X00000260, 0X00000190, 0X00000228, 0X00000204,
0X0000020C, 0X0000018C, 0X000001E0, 0X000001C8, 0X000001A8, 0X000001B0, 0X00000188, 0X00000180,
0X000000D8, 0X00000144, 0X000000D4, 0X00000114, 0X000000CC, 0X000000B0, 0X000000B8, 0X00190008,
0000000000, 0000000000, 0000000000, 0000000000, 0X000000CC, 0X0000000D, 0X000000BC, 0X4D657468,
0X6F642043, 0X616C6C62, 0X61636B00, 0X00000100, 0000000000, 0X00000130, 0X0000016C, 0X00000298,
0X0000031C, 0X00000338, 0X00000364, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000394,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000011C, 0X00000013, 0X44726167,
0X20496E70, 0X75742043, 0X616C6C62, 0X61636B00, 0X00000006, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000014C, 0X0000001D, 0X2F537461, 0X6E646172, 0X64204472, 0X61672049, 0X6E707574,
0X2043616C, 0X6C626163, 0X6B000000, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000188, 0X00000003, 0X00000194, 0X000001F0, 0X00000244, 0X00180008, 0000000000, 0000000000,
0000000000, 0000000000, 0X000001C8, 0X00000001, 0X000001B4, 0X41747472, 0X69627574, 0X65205370,
0X65636966, 0X69657200, 0X000001CC, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X000001E8, 0X00000005, 0X4F776E65, 0X72000000, 0X00170008, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000228, 0X00000001, 0X00000210, 0X41747461, 0X63686D65, 0X6E742053, 0X70656369,
0X66696572, 0000000000, 0X0000022C, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000001, 0X00170008, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000027C, 0X00000001,
0X00000264, 0X41747461, 0X63686D65, 0X6E742053, 0X70656369, 0X66696572, 0000000000, 0X00000280,
0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000002, 0X00000007, 0000000000,
0000000000, 0000000000, 0000000000, 0X000002B4, 0X00000001, 0X000002B8, 0X00180008, 0000000000,
0000000000, 0000000000, 0000000000, 0X000002EC, 0X00000001, 0X000002D8, 0X41747472, 0X69627574,
0X65205370, 0X65636966, 0X69657200, 0X000002F0, 0X00000006, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000030C, 0X0000000F, 0X43616C6C, 0X6261636B, 0X20526573, 0X756C7400, 0X00000007,
0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000354, 0X0000000C, 0X2F446F20, 0X43616C6C, 0X6261636B,
0000000000, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000380, 0X00000010,
0X44726167, 0X496E7075, 0X7450726F, 0X63507472, 0000000000, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Abstract_20_Drag_20_Handler_2F_Drop_20_Flavors[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Window_20_Drag_20_Handler_2F_Drop_20_Flavors[] = {
0000000000, 0X00000054, 0X00000020, 0X00000003, 0X00000014, 0X00000054, 0X0000003C, 0X00000034,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000003C, 0X00000001, 0X00000040,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000005C, 0X00000016, 0X50617374,
0X65626F61, 0X72642046, 0X6C61766F, 0X72205465, 0X78740000
	};
	Nat4 tempAttribute_Window_20_Drag_20_Handler_2F_Current_20_Drop_20_Flavors[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Window_20_Drag_20_Handler_2F_Tracking_20_Callback[] = {
0000000000, 0X0000031C, 0X0000009C, 0X00000022, 0X00000014, 0X000000FC, 0X0000037C, 0X000000E8,
0X00000350, 0X000000E4, 0X000000E0, 0X00000308, 0X000002F0, 0X000002D0, 0X000002D8, 0X000002B8,
0X000002B0, 0X000000DC, 0X00000280, 0X0000025C, 0X00000264, 0X00000194, 0X0000022C, 0X00000208,
0X00000210, 0X00000190, 0X000001E4, 0X000001CC, 0X000001AC, 0X000001B4, 0X0000018C, 0X00000184,
0X000000D8, 0X00000150, 0X000000D4, 0X00000114, 0X000000CC, 0X000000B0, 0X000000B8, 0X00190008,
0000000000, 0000000000, 0000000000, 0000000000, 0X000000CC, 0X0000000D, 0X000000BC, 0X4D657468,
0X6F642043, 0X616C6C62, 0X61636B00, 0X00000100, 0000000000, 0X0000013C, 0X00000170, 0X0000029C,
0X00000320, 0X0000033C, 0X00000368, 0000000000, 0000000000, 0000000000, 0000000000, 0X000003A0,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000011C, 0X0000001D, 0X57696E64,
0X6F772044, 0X72616720, 0X54726163, 0X6B696E67, 0X2043616C, 0X6C626163, 0X6B000000, 0X00000006,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000158, 0X00000014, 0X2F446F20, 0X54726163,
0X6B696E67, 0X2048616E, 0X646C6572, 0000000000, 0X00000007, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000018C, 0X00000003, 0X00000198, 0X000001F4, 0X00000248, 0X00180008, 0000000000,
0000000000, 0000000000, 0000000000, 0X000001CC, 0X00000001, 0X000001B8, 0X41747472, 0X69627574,
0X65205370, 0X65636966, 0X69657200, 0X000001D0, 0X00000006, 0000000000, 0000000000, 0000000000,
0000000000, 0X000001EC, 0X00000005, 0X4F776E65, 0X72000000, 0X00170008, 0000000000, 0000000000,
0000000000, 0000000000, 0X0000022C, 0X00000001, 0X00000214, 0X41747461, 0X63686D65, 0X6E742053,
0X70656369, 0X66696572, 0000000000, 0X00000230, 0X00000004, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000001, 0X00170008, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000280,
0X00000001, 0X00000268, 0X41747461, 0X63686D65, 0X6E742053, 0X70656369, 0X66696572, 0000000000,
0X00000284, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000004, 0X00000007,
0000000000, 0000000000, 0000000000, 0000000000, 0X000002B8, 0X00000001, 0X000002BC, 0X00180008,
0000000000, 0000000000, 0000000000, 0000000000, 0X000002F0, 0X00000001, 0X000002DC, 0X41747472,
0X69627574, 0X65205370, 0X65636966, 0X69657200, 0X000002F4, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000310, 0X0000000F, 0X43616C6C, 0X6261636B, 0X20526573, 0X756C7400,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000006,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000358, 0X0000000C, 0X2F446F20, 0X43616C6C,
0X6261636B, 0000000000, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000384,
0X0000001A, 0X44726167, 0X54726163, 0X6B696E67, 0X48616E64, 0X6C657250, 0X726F6350, 0X74720000,
0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Window_20_Drag_20_Handler_2F_Receive_20_Callback[] = {
0000000000, 0X000002C0, 0X0000008C, 0X0000001E, 0X00000014, 0X000000EC, 0X00000310, 0X000000D8,
0X000002E4, 0X000000D4, 0X000000D0, 0X0000029C, 0X00000284, 0X00000264, 0X0000026C, 0X0000024C,
0X00000244, 0X000000CC, 0X00000214, 0X000001F0, 0X000001F8, 0X0000017C, 0X000001CC, 0X000001B4,
0X00000194, 0X0000019C, 0X00000178, 0X00000170, 0X000000C8, 0X00000140, 0X000000C4, 0X00000104,
0X000000BC, 0X000000A0, 0X000000A8, 0X00190008, 0000000000, 0000000000, 0000000000, 0000000000,
0X000000BC, 0X0000000D, 0X000000AC, 0X4D657468, 0X6F642043, 0X616C6C62, 0X61636B00, 0X000000F0,
0000000000, 0X0000012C, 0X0000015C, 0X00000230, 0X000002B4, 0X000002D0, 0X000002FC, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000334, 0X00000006, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000010C, 0X0000001C, 0X57696E64, 0X6F772044, 0X72616720, 0X52656365, 0X69766520,
0X43616C6C, 0X6261636B, 0000000000, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000148, 0X00000013, 0X2F446F20, 0X52656365, 0X69766520, 0X48616E64, 0X6C657200, 0X00000007,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000178, 0X00000002, 0X00000180, 0X000001DC,
0X00180008, 0000000000, 0000000000, 0000000000, 0000000000, 0X000001B4, 0X00000001, 0X000001A0,
0X41747472, 0X69627574, 0X65205370, 0X65636966, 0X69657200, 0X000001B8, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X000001D4, 0X00000005, 0X4F776E65, 0X72000000, 0X00170008,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000214, 0X00000001, 0X000001FC, 0X41747461,
0X63686D65, 0X6E742053, 0X70656369, 0X66696572, 0000000000, 0X00000218, 0X00000004, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000003, 0X00000007, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000024C, 0X00000001, 0X00000250, 0X00180008, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000284, 0X00000001, 0X00000270, 0X41747472, 0X69627574, 0X65205370, 0X65636966,
0X69657200, 0X00000288, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X000002A4,
0X0000000F, 0X43616C6C, 0X6261636B, 0X20526573, 0X756C7400, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000006, 0000000000, 0000000000, 0000000000,
0000000000, 0X000002EC, 0X0000000C, 0X2F446F20, 0X43616C6C, 0X6261636B, 0000000000, 0X00000006,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000318, 0X00000019, 0X44726167, 0X52656365,
0X69766548, 0X616E646C, 0X65725072, 0X6F635074, 0X72000000, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};


Nat4 VPLC_Abstract_20_Drag_202620_Drop_20_Reference_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Abstract_20_Drag_202620_Drop_20_Reference_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 474 626 }{ 200 300 } */
	tempAttribute = attribute_add("DragRef",tempClass,NULL,environment);
	tempAttribute = attribute_add("Pasteboard",tempClass,NULL,environment);
/* Stop Attributes */

/* NULL SUPER CLASS */
	return kNOERROR;
}

/* Start Universals: { 171 353 }{ 603 326 } */
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_DragRef(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_DragRef,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_DragRef(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_DragRef,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Drag_20_Reference,1,1,NONE,ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_DragRef,2,0,TERMINAL(1),TERMINAL(0));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERSINGLECASEWITHNONE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Release_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Release_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Pasteboard,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(1));

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Pasteboard,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Release(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_DragRef,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Release_case_1_local_4(PARAMETERS,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Show_20_Hilite(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_method_Default_20_TRUE(PARAMETERS,TERMINAL(2),ROOT(4));

PUTINTEGER(ShowDragHilite( GETPOINTER(0,OpaqueDragRef,*,ROOT(6),TERMINAL(3)),GETPOINTER(0,OpaqueRgnHandle,*,ROOT(7),TERMINAL(1)),GETINTEGER(TERMINAL(4))),5);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(8)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Hide_20_Hilite(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

PUTINTEGER(HideDragHilite( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1))),2);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(7)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

PUTINTEGER(GetDragAttributes( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1)),GETPOINTER(4,unsigned int,*,ROOT(4),NONE)),2);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_method_Get_20_UInt32(PARAMETERS,TERMINAL(4),NONE,ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERWITHNONE(7)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Has_20_Left_20_Sender_20_Window_3F_(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Attributes,1,1,TERMINAL(0),ROOT(1));

result = vpx_extconstant(PARAMETERS,kDragHasLeftSenderWindow,ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_test_2D_bit_3F_,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Inside_20_Sender_20_Window_3F_(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Attributes,1,1,TERMINAL(0),ROOT(1));

result = vpx_extconstant(PARAMETERS,kDragInsideSenderWindow,ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_test_2D_one_3F_,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Inside_20_Sender_20_Application_3F_(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Attributes,1,1,TERMINAL(0),ROOT(1));

result = vpx_extconstant(PARAMETERS,kDragInsideSenderApplication,ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_test_2D_one_3F_,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Pasteboard,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_2_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_2_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"FALSE",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Release_3F_,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(7)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
FAILONSUCCESS

PUTINTEGER(GetDragPasteboard( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1)),GETPOINTER(0,OpaquePasteboardRef,**,ROOT(4),NONE)),2);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
FAILONFAILURE

result = vpx_method_Coerce_20_Pointer(PARAMETERS,TERMINAL(4),ROOT(5));

result = vpx_method_Pasteboard_2F_New(PARAMETERS,TERMINAL(4),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Pasteboard,2,0,TERMINAL(0),TERMINAL(6));

result = vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_2_local_9(PARAMETERS,TERMINAL(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERWITHNONE(7)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Find_20_Tasty_20_Flavors_3F_(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Pasteboard,1,1,TERMINAL(0),ROOT(3));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Tasty_20_Flavors_3F_,3,1,TERMINAL(3),TERMINAL(1),TERMINAL(2),ROOT(4));

result = vpx_match(PARAMETERS,"( )",TERMINAL(4));
FAILONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Values_20_As_20_UTIs(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Pasteboard,1,1,TERMINAL(0),ROOT(2));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Values_20_As_20_UTIs,2,2,TERMINAL(2),TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_match(PARAMETERS,"( )",TERMINAL(4));
FAILONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Pasteboard(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Pasteboard,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Pasteboard(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Pasteboard,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Item_20_Bounds(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

PUTINTEGER(GetDragItemBounds( GETPOINTER(0,OpaqueDragRef,*,ROOT(4),TERMINAL(2)),GETINTEGER(TERMINAL(1)),GETPOINTER(8,Rect,*,ROOT(5),NONE)),3);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(3));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTERSINGLECASEWITHNONE(6)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Item_20_Bounds(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

PUTINTEGER(SetDragItemBounds( GETPOINTER(0,OpaqueDragRef,*,ROOT(5),TERMINAL(3)),GETINTEGER(TERMINAL(1)),GETCONSTPOINTER(Rect,*,TERMINAL(2))),4);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(4));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
NEXTCASEONSUCCESS

PUTINTEGER(EqualRect( GETCONSTPOINTER(Rect,*,TERMINAL(0)),GETCONSTPOINTER(Rect,*,TERMINAL(1))),2);
result = kSuccess;

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(2));
NEXTCASEONSUCCESS

UnionRect( GETCONSTPOINTER(Rect,*,TERMINAL(0)),GETCONSTPOINTER(Rect,*,TERMINAL(1)),GETPOINTER(8,Rect,*,ROOT(3),TERMINAL(0)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(10)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Count_20_Drag_20_Items,1,1,TERMINAL(0),ROOT(1));
FAILONFAILURE

result = vpx_method_Make_20_Count(PARAMETERS,TERMINAL(1),ROOT(2));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(2))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_primitive(PARAMETERS,VPLP__2D_1,1,1,LIST(2),ROOT(3));
LISTROOT(3,0)
REPEATFINISH
LISTROOTFINISH(3,0)
LISTROOTEND
} else {
ROOTEMPTY(3)
}

if( (repeatLimit = vpx_multiplex(1,TERMINAL(3))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Item_20_Bounds,2,1,TERMINAL(0),LIST(3),ROOT(4));
FAILONFAILURE
LISTROOT(4,0)
REPEATFINISH
LISTROOTFINISH(4,0)
LISTROOTEND
} else {
ROOTEMPTY(4)
}

result = vpx_match(PARAMETERS,"( )",TERMINAL(4));
FAILONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_constant(PARAMETERS,"NULL",ROOT(7));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(4))) ){
REPEATBEGINWITHCOUNTER
LOOPTERMINALBEGIN(1)
LOOPTERMINAL(0,7,8)
result = vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds_case_1_local_9(PARAMETERS,LOOP(0),LIST(4),ROOT(8));
REPEATFINISH
} else {
ROOTNULL(8,TERMINAL(7))
}

if( (repeatLimit = vpx_multiplex(1,TERMINAL(4))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_primitive(PARAMETERS,VPLP_Rect_2D_to_2D_list,1,1,LIST(4),ROOT(9));
LISTROOT(9,0)
REPEATFINISH
LISTROOTFINISH(9,0)
LISTROOTEND
} else {
ROOTEMPTY(9)
}

result = kSuccess;

OUTPUT(0,TERMINAL(8))
FOOTERSINGLECASE(10)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Count_20_Drag_20_Items(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(7)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

PUTINTEGER(CountDragItems( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1)),GETPOINTER(2,unsigned short,*,ROOT(4),NONE)),2);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
FAILONFAILURE

result = vpx_method_Get_20_UInt16(PARAMETERS,TERMINAL(4),NONE,ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASEWITHNONE(7)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Mouse(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(6)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
FAILONSUCCESS

PUTINTEGER(GetDragMouse( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1)),GETPOINTER(4,Point,*,ROOT(4),NONE),GETPOINTER(4,Point,*,ROOT(5),NONE)),2);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(4))
OUTPUT(1,TERMINAL(5))
FOOTERSINGLECASEWITHNONE(6)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Origin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(5)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
FAILONSUCCESS

PUTINTEGER(GetDragOrigin( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1)),GETPOINTER(4,Point,*,ROOT(4),NONE)),2);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASEWITHNONE(5)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERSINGLECASEWITHNONE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_PasteboardRef,1,1,TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

PUTINTEGER(NewDragWithPasteboard( GETPOINTER(0,OpaquePasteboardRef,*,ROOT(4),TERMINAL(2)),GETPOINTER(0,OpaqueDragRef,**,ROOT(5),NONE)),3);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_DragRef,2,0,TERMINAL(0),TERMINAL(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Pasteboard,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(6));

result = vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_1_local_9(PARAMETERS,TERMINAL(1),TERMINAL(6),ROOT(7));

result = kSuccess;

FOOTERWITHNONE(8)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(1));

result = kSuccess;
FAILONSUCCESS

FOOTER(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Track_20_Drag(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

result = vpx_persistent(PARAMETERS,kVPXValue_Current_20_Drag,1,0,TERMINAL(0));

PUTINTEGER(TrackDrag( GETPOINTER(0,OpaqueDragRef,*,ROOT(5),TERMINAL(3)),GETCONSTPOINTER(EventRecord,*,TERMINAL(1)),GETPOINTER(0,OpaqueRgnHandle,*,ROOT(6),TERMINAL(2))),4);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(4));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"NULL",ROOT(7));

result = vpx_persistent(PARAMETERS,kVPXValue_Current_20_Drag,1,0,TERMINAL(7));

result = kSuccess;

FOOTERSINGLECASE(8)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Drag_20_CGImage(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
FAILONSUCCESS

result = vpx_method_Default_20_Zero(PARAMETERS,TERMINAL(3),ROOT(5));

PUTINTEGER(SetDragImageWithCGImage( GETPOINTER(0,OpaqueDragRef,*,ROOT(7),TERMINAL(4)),GETPOINTER(0,CGImage,*,ROOT(8),TERMINAL(1)),GETCONSTPOINTER(CGPoint,*,TERMINAL(2)),GETINTEGER(TERMINAL(5))),6);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(6));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Drag_20_Allowable_20_Actions(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

result = vpx_method_Default_20_Zero(PARAMETERS,TERMINAL(1),ROOT(4));

result = vpx_method_Default_20_FALSE(PARAMETERS,TERMINAL(2),ROOT(5));

PUTINTEGER(SetDragAllowableActions( GETPOINTER(0,OpaqueDragRef,*,ROOT(7),TERMINAL(3)),GETINTEGER(TERMINAL(4)),GETINTEGER(TERMINAL(5))),6);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(6));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(8)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Allowable_20_Actions(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(7)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
FAILONSUCCESS

PUTINTEGER(GetDragAllowableActions( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1)),GETPOINTER(4,unsigned int,*,ROOT(4),NONE)),2);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
FAILONFAILURE

result = vpx_method_Get_20_UInt32(PARAMETERS,TERMINAL(4),NONE,ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASEWITHNONE(7)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Drag_20_Drop_20_Action(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

result = vpx_method_Default_20_Zero(PARAMETERS,TERMINAL(1),ROOT(3));

PUTINTEGER(SetDragDropAction( GETPOINTER(0,OpaqueDragRef,*,ROOT(5),TERMINAL(2)),GETINTEGER(TERMINAL(3))),4);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(4));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Drop_20_Action(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(7)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
FAILONSUCCESS

PUTINTEGER(GetDragDropAction( GETPOINTER(0,OpaqueDragRef,*,ROOT(3),TERMINAL(1)),GETPOINTER(4,unsigned int,*,ROOT(4),NONE)),2);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
FAILONFAILURE

result = vpx_method_Get_20_UInt32(PARAMETERS,TERMINAL(4),NONE,ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASEWITHNONE(7)
}

/* Stop Universals */



Nat4 VPLC_Drop_20_Reference_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Drop_20_Reference_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("DragRef",tempClass,NULL,environment);
	tempAttribute = attribute_add("Pasteboard",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Abstract Drag & Drop Reference");
	return kNOERROR;
}

/* Start Universals: { 384 773 }{ 200 300 } */
enum opTrigger vpx_method_Drop_20_Reference_2F_New(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Drop_20_Reference,1,1,NONE,ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_DragRef,2,0,TERMINAL(1),TERMINAL(0));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERSINGLECASEWITHNONE(2)
}

/* Stop Universals */



Nat4 VPLC_Drag_20_Reference_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Drag_20_Reference_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 386 764 }{ 195 355 } */
	tempAttribute = attribute_add("DragRef",tempClass,NULL,environment);
	tempAttribute = attribute_add("Pasteboard",tempClass,NULL,environment);
	tempAttribute = attribute_add("Drag Input Callback",tempClass,tempAttribute_Drag_20_Reference_2F_Drag_20_Input_20_Callback,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Abstract Drag & Drop Reference");
	return kNOERROR;
}

/* Start Universals: { 537 863 }{ 200 300 } */
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,optionKey,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_extconstant(PARAMETERS,kDragActionCopy,ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_Drop_20_Action,2,0,TERMINAL(1),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kDragActionMove,ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_Drop_20_Action,2,0,TERMINAL(1),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Inside_20_Sender_20_Application_3F_,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
NEXTCASEONSUCCESS

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Drop_20_Action,1,1,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = vpx_extmatch(PARAMETERS,kDragActionCopy,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_extconstant(PARAMETERS,kThemeCopyArrowCursor,ROOT(2));

result = vpx_method_Set_20_Cursor(PARAMETERS,TERMINAL(2));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kThemeArrowCursor,ROOT(1));

result = vpx_method_Set_20_Cursor(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
if(vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADERWITHNONE(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,noErr,ROOT(3));

result = vpx_method_Get_20_SInt16(PARAMETERS,TERMINAL(2),NONE,ROOT(4),ROOT(5));

result = vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_4(PARAMETERS,TERMINAL(5),TERMINAL(0));

result = vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback_case_1_local_5(PARAMETERS,TERMINAL(0));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(6)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Open_20_Drag_20_Input_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Input_20_Callback,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_Pointer,2,1,TERMINAL(1),TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_Input_20_Proc,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Close_20_Drag_20_Input_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Input_20_Callback,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Get_20_Drag_20_Input_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Drag_20_Input_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Set_20_Drag_20_Input_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Drag_20_Input_20_Callback,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Set_20_Drag_20_Input_20_Proc(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_DragRef,1,1,TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_constant(PARAMETERS,"NULL",ROOT(3));

PUTINTEGER(SetDragInputProc( GETPOINTER(0,OpaqueDragRef,*,ROOT(5),TERMINAL(2)),GETPOINTER(2,short,*,ROOT(6),TERMINAL(1)),GETPOINTER(0,void,*,ROOT(7),TERMINAL(3))),4);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(8)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Track_20_Drag_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Drag_20_Reference_2F_Track_20_Drag_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_Drag_20_Input_20_Callback,1,0,TERMINAL(0));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Track_20_Drag,3,0,TERMINAL(0),TERMINAL(1),TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close_20_Drag_20_Input_20_Callback,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Track_20_Drag_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Drag_20_Reference_2F_Track_20_Drag_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close_20_Drag_20_Input_20_Callback,1,0,TERMINAL(0));

result = kSuccess;
FAILONSUCCESS

FOOTER(3)
}

enum opTrigger vpx_method_Drag_20_Reference_2F_Track_20_Drag(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Drag_20_Reference_2F_Track_20_Drag_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_Drag_20_Reference_2F_Track_20_Drag_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

/* Stop Universals */



Nat4 VPLC_Abstract_20_Drag_20_Handler_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Abstract_20_Drag_20_Handler_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 247 444 }{ 171 370 } */
	tempAttribute = attribute_add("Owner",tempClass,NULL,environment);
	tempAttribute = attribute_add("The Drag",tempClass,NULL,environment);
	tempAttribute = attribute_add("Drop Flavors",tempClass,tempAttribute_Abstract_20_Drag_20_Handler_2F_Drop_20_Flavors,environment);
	tempAttribute = attribute_add("Current Drop Flavors",tempClass,NULL,environment);
/* Stop Attributes */

/* NULL SUPER CLASS */
	return kNOERROR;
}

/* Start Universals: { 258 792 }{ 585 352 } */
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_The_20_Drag,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_persistent(PARAMETERS,kVPXValue_Current_20_Drag,0,1,ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_2_local_4(PARAMETERS,TERMINAL(0),TERMINAL(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Drop_20_Reference_2F_New(PARAMETERS,TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_The_20_Drag,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
if(vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Owner,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Install_20_Handlers,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Close_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Close_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Owner,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Close(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Remove_20_Handlers,1,0,TERMINAL(0));

result = vpx_method_Abstract_20_Drag_20_Handler_2F_Close_case_1_local_3(PARAMETERS,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Owner(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_Owner(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Owner,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_persistent(PARAMETERS,kVPXValue_Current_20_Drag,0,1,ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_The_20_Drag,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_The_20_Drag(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_The_20_Drag,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavors(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Drop_20_Flavors,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_Drop_20_Flavors(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Drop_20_Flavors,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Make_20_Class(PARAMETERS,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"Drop",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Known_20_IDs_20_For_20_Type,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drop_20_Flavors,1,1,TERMINAL(0),ROOT(1));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(1))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs_case_1_local_3(PARAMETERS,LIST(1),ROOT(2));
LISTROOT(2,0)
REPEATFINISH
LISTROOTFINISH(2,0)
LISTROOTEND
} else {
ROOTEMPTY(2)
}

result = vpx_method__28_Flatten_29_(PARAMETERS,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Current_20_Drop_20_Flavors(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Current_20_Drop_20_Flavors,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_Current_20_Drop_20_Flavors(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Current_20_Drop_20_Flavors,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Any_20_Drop_20_Flavors_3F_(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Current_20_Drop_20_Flavors,1,1,TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drop_20_Flavor_20_UTIs,1,1,TERMINAL(0),ROOT(2));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Tasty_20_Flavors_3F_,3,1,TERMINAL(1),TERMINAL(2),TERMINAL(3),ROOT(4));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Check_20_Tasty_20_Flavors,2,0,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"( )",ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Current_20_Drop_20_Flavors,2,0,TERMINAL(0),TERMINAL(5));

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Current_20_Drop_20_Flavors,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Make_20_Class(PARAMETERS,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_method_Make_20_Clone(PARAMETERS,TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drop_20_Flavors,1,1,TERMINAL(0),ROOT(2));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(2))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors_case_1_local_3(PARAMETERS,LIST(2),ROOT(3));
LISTROOT(3,0)
REPEATFINISH
LISTROOTFINISH(3,0)
LISTROOTEND
} else {
ROOTEMPTY(3)
}

if( (repeatLimit = vpx_multiplex(1,TERMINAL(3))) ){
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Is_20_Tasty_20_Flavor_3F_,3,0,LIST(3),TERMINAL(0),TERMINAL(1));
FAILONFAILURE
REPEATFINISH
} else {
}

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drop_20_Flavor_20_UTIs,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Values_20_As_20_UTIs,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Current_20_Drop_20_Flavors,2,0,TERMINAL(0),TERMINAL(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( )",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Current_20_Drop_20_Flavors,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_The_20_Drag,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_persistent(PARAMETERS,kVPXValue_Current_20_Drag,0,1,ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP__3D_,2,0,TERMINAL(1),TERMINAL(2));
FAILONSUCCESS

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_3_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_3_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_The_20_Drag,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_3_case_1_local_4(PARAMETERS,TERMINAL(1));

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_The_20_Drag,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Current_20_Drop_20_Flavors,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_2(PARAMETERS,TERMINAL(0));
TERMINATEONFAILURE

result = vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_3(PARAMETERS,TERMINAL(0));

result = vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop_case_1_local_4(PARAMETERS,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Install_20_Handlers(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Remove_20_Handlers(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Show_20_Hilite(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Hide_20_Hilite(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Tasty_20_Pasteboard(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_The_20_Drag,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Pasteboard,1,1,TERMINAL(1),ROOT(2));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

/* Stop Universals */



Nat4 VPLC_Window_20_Drag_20_Handler_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Window_20_Drag_20_Handler_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 535 352 }{ 199 340 } */
	tempAttribute = attribute_add("Owner",tempClass,NULL,environment);
	tempAttribute = attribute_add("The Drag",tempClass,NULL,environment);
	tempAttribute = attribute_add("Drop Flavors",tempClass,tempAttribute_Window_20_Drag_20_Handler_2F_Drop_20_Flavors,environment);
	tempAttribute = attribute_add("Current Drop Flavors",tempClass,tempAttribute_Window_20_Drag_20_Handler_2F_Current_20_Drop_20_Flavors,environment);
	tempAttribute = attribute_add("Tracking Callback",tempClass,tempAttribute_Window_20_Drag_20_Handler_2F_Tracking_20_Callback,environment);
	tempAttribute = attribute_add("Receive Callback",tempClass,tempAttribute_Window_20_Drag_20_Handler_2F_Receive_20_Callback,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Abstract Drag Handler");
	return kNOERROR;
}

/* Start Universals: { 332 841 }{ 442 319 } */
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kDragTrackingEnterWindow,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Drag_20_Enter_20_Window,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kDragTrackingInWindow,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Drag_20_In_20_Window,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kDragTrackingLeaveWindow,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Drag_20_Leave_20_Window,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kDragTrackingEnterHandler,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Drag_20_Enter_20_Handler,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_5(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_5(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kDragTrackingLeaveHandler,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Drag_20_Leave_20_Handler,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_6(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_6(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,dragNotAcceptedErr,ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
if(vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
if(vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
if(vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
if(vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_5(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3_case_6(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Default_20_The_20_Drag,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));

result = vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Receive_20_Handler(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Default_20_The_20_Drag,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Drag_20_Receive,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Any_20_Drop_20_Flavors_3F_,2,0,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Drag_20_Track_20_In_20_Window,2,0,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Show_20_Hilite,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_extconstant(PARAMETERS,noErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,dragNotAcceptedErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,noErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Track_20_In_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Check_20_Drop_20_Flavors,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Any_20_Drop_20_Flavors_3F_,2,0,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Show_20_Hilite,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_extconstant(PARAMETERS,noErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,dragNotAcceptedErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Leave_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Hide_20_Hilite,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_extconstant(PARAMETERS,noErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_persistent(PARAMETERS,kVPXValue_appDEBUG_5F_VERBOSITY,0,1,ROOT(1));

result = vpx_match(PARAMETERS,"0",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_constant(PARAMETERS,"PasteboardRef Check",ROOT(2));

result = vpx_constant(PARAMETERS,"1",ROOT(3));

result = vpx_method_Debug_20_Value(PARAMETERS,TERMINAL(2),TERMINAL(0),TERMINAL(3));

CFShow( GETCONSTPOINTER(void,*,TERMINAL(0)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_PasteboardRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_1_local_4(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"PasteboardRef is NULL!",ROOT(1));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERSINGLECASEWITHNONE(1)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Pasteboard,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drop_20_Flavors,1,1,TERMINAL(1),ROOT(3));

result = vpx_method_Make_20_Clone(PARAMETERS,TERMINAL(3),ROOT(4));

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_5(PARAMETERS,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Known_20_Flavors,2,0,TERMINAL(2),TERMINAL(4));

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1_local_7(PARAMETERS,TERMINAL(2),ROOT(5));

result = vpx_match(PARAMETERS,"0",TERMINAL(5));
TERMINATEONSUCCESS

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kDragActionMove,ROOT(2));

result = vpx_extconstant(PARAMETERS,kDragActionCopy,ROOT(3));

result = vpx_call_evaluate(PARAMETERS,"A | B",2,1,TERMINAL(3),TERMINAL(2),ROOT(4));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_Allowable_20_Actions,3,0,TERMINAL(0),TERMINAL(4),TERMINAL(5));
TERMINATEONFAILURE

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,noErr,ROOT(2));

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler_case_1_local_3(PARAMETERS,TERMINAL(1),TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Check_20_Drop_20_Flavors,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Leave_20_Handler(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Dispose_20_Drop,1,0,TERMINAL(0));

result = vpx_extconstant(PARAMETERS,noErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Hide_20_Hilite,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Accept_20_Drop,2,0,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_extconstant(PARAMETERS,noErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,dragNotAcceptedErr,ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_UPP_20_Pointer,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Callback_20_Pointer,1,1,TERMINAL(0),ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(9)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Tracking_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(3),ROOT(4));

result = vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2_case_1_local_6(PARAMETERS,TERMINAL(2),ROOT(5));

PUTINTEGER(RemoveTrackingHandler( GETPOINTER(2,short,*,ROOT(7),TERMINAL(5)),GETPOINTER(0,OpaqueWindowPtr,*,ROOT(8),TERMINAL(4))),6);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_UPP_20_Pointer,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Callback_20_Pointer,1,1,TERMINAL(0),ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(9)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Receive_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(3),ROOT(4));

result = vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3_case_1_local_6(PARAMETERS,TERMINAL(2),ROOT(5));

PUTINTEGER(RemoveReceiveHandler( GETPOINTER(2,short,*,ROOT(7),TERMINAL(5)),GETPOINTER(0,OpaqueWindowPtr,*,ROOT(8),TERMINAL(4))),6);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers_case_1_local_3(PARAMETERS,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(11)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Tracking_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_Pointer,2,1,TERMINAL(2),TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(4),ROOT(5));

result = vpx_constant(PARAMETERS,"NULL",ROOT(6));

PUTINTEGER(InstallTrackingHandler( GETPOINTER(2,short,*,ROOT(8),TERMINAL(3)),GETPOINTER(0,OpaqueWindowPtr,*,ROOT(9),TERMINAL(5)),GETPOINTER(0,void,*,ROOT(10),TERMINAL(6))),7);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(11)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(11)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Receive_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_Pointer,2,1,TERMINAL(2),TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(4),ROOT(5));

result = vpx_constant(PARAMETERS,"NULL",ROOT(6));

PUTINTEGER(InstallReceiveHandler( GETPOINTER(2,short,*,ROOT(8),TERMINAL(3)),GETPOINTER(0,OpaqueWindowPtr,*,ROOT(9),TERMINAL(5)),GETPOINTER(0,void,*,ROOT(10),TERMINAL(6))),7);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(11)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1_local_3(PARAMETERS,TERMINAL(0));

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Has_20_Left_20_Sender_20_Window_3F_,1,1,TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Show_20_Drag_20_Hilite,1,0,TERMINAL(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_3_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_3_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(8)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(1));

PUTPOINTER(OpaqueRgnHandle,*,NewRgn(),2);
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(1),ROOT(3));

PUTPOINTER(Rect,*,GetWindowPortBounds( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(5),TERMINAL(3)),GETPOINTER(8,Rect,*,ROOT(6),NONE)),4);
result = kSuccess;

RectRgn( GETPOINTER(0,OpaqueRgnHandle,*,ROOT(7),TERMINAL(2)),GETCONSTPOINTER(Rect,*,TERMINAL(4)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTERSINGLECASEWITHNONE(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Has_20_Left_20_Sender_20_Window_3F_,1,1,TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_3_local_4(PARAMETERS,TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Show_20_Hilite,3,0,TERMINAL(1),TERMINAL(3),NONE);

DisposeRgn( GETPOINTER(0,OpaqueRgnHandle,*,ROOT(4),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
if(vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Hide_20_Drag_20_Hilite,1,0,TERMINAL(2));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Hide_20_Hilite,1,0,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Any_20_Drop_20_Flavors_3F_,2,0,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Extract_20_Drop_20_Flavors,2,1,TERMINAL(0),TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Accept_20_Flavors,3,0,TERMINAL(2),TERMINAL(1),TERMINAL(3));
FAILONFAILURE

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Inside_20_Sender_20_Application_3F_,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
TERMINATEONFAILURE

result = vpx_extconstant(PARAMETERS,kDragActionNothing,ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_Drop_20_Action,2,0,TERMINAL(0),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_2_local_2(PARAMETERS,TERMINAL(1));

result = kSuccess;
FAILONSUCCESS

FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Get_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Owner,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_extget(PARAMETERS,"where",1,2,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
OUTPUT(1,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_extget(PARAMETERS,"where",1,2,TERMINAL(0),ROOT(1),ROOT(2));

PUTINTEGER(WaitMouseMoved( GETSTRUCTURE(4,Point,TERMINAL(2))),3);
result = kSuccess;

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(3));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
OUTPUT(1,TERMINAL(2))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Pasteboard_20_Create_20_Unique,1,1,TERMINAL(0),ROOT(2));

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Pasteboard_20_Clear,1,1,TERMINAL(0),ROOT(3));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Put_20_Values_20_As_20_UTIs,3,1,TERMINAL(0),TERMINAL(1),NONE,ROOT(4));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERWITHNONE(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(0));

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"VPLWindow Item",ROOT(3));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(2),TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame_20_In_20_Window,1,1,TERMINAL(2),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Item_20_ID,1,1,TERMINAL(1),ROOT(5));

result = vpx_method_Rectangle_20_Local_20_To_20_Global(PARAMETERS,TERMINAL(4),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(6),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Item_20_Bounds,3,0,TERMINAL(0),TERMINAL(5),TERMINAL(7));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Pasteboard,1,1,NONE,ROOT(2));

result = vpx_constant(PARAMETERS,"Drag",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window_20_Pasteboard_20_Handler,1,1,TERMINAL(0),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Flavors_20_For_20_Type,2,1,TERMINAL(4),TERMINAL(3),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Known_20_Flavors,2,0,TERMINAL(2),TERMINAL(5));

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_7(PARAMETERS,TERMINAL(2),TERMINAL(1),ROOT(6));
FAILONFAILURE

result = vpx_instantiate(PARAMETERS,kVPXClass_Drag_20_Reference,1,1,NONE,ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_New_20_With_20_Pasteboard,2,0,TERMINAL(7),TERMINAL(2));
FAILONFAILURE

if( (repeatLimit = vpx_multiplex(2,TERMINAL(6),TERMINAL(1))) ){
REPEATBEGINWITHCOUNTER
result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5_case_1_local_10(PARAMETERS,TERMINAL(7),LIST(6),LIST(1));
REPEATFINISH
} else {
}

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTERSINGLECASEWITHNONE(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(5)
INPUT(0,0)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kDragActionCopy,ROOT(1));

result = vpx_extconstant(PARAMETERS,kDragActionMove,ROOT(2));

result = vpx_extconstant(PARAMETERS,kDragActionDelete,ROOT(3));

result = vpx_call_evaluate(PARAMETERS,"A | B | C",3,1,TERMINAL(1),TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_Allowable_20_Actions,3,0,TERMINAL(0),TERMINAL(4),NONE);
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASEWITHNONE(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Track_20_Drag,3,0,TERMINAL(0),TERMINAL(1),TERMINAL(2));
NEXTCASEONFAILURE

DisposeRgn( GETPOINTER(0,OpaqueRgnHandle,*,ROOT(3),TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

DisposeRgn( GETPOINTER(0,OpaqueRgnHandle,*,ROOT(3),TERMINAL(2)));
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Drop_20_Action,1,1,TERMINAL(0),ROOT(3));

result = vpx_extmatch(PARAMETERS,kDragActionDelete,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Delete_20_Items,2,0,TERMINAL(1),TERMINAL(2));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Drag_20_Drop_20_Action,1,1,TERMINAL(0),ROOT(3));

result = vpx_extmatch(PARAMETERS,kDragActionMove,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Inside_20_Sender_20_Window_3F_,1,1,TERMINAL(0),ROOT(4));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(4));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Inside_20_Sender_20_Application_3F_,1,1,TERMINAL(0),ROOT(5));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(5));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Delete_20_Items,2,0,TERMINAL(1),TERMINAL(2));

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_Drag_20_Image,4,1,TERMINAL(1),TERMINAL(0),TERMINAL(2),TERMINAL(4),ROOT(5));
NEXTCASEONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_4(PARAMETERS,TERMINAL(0),TERMINAL(3),TERMINAL(5));
NEXTCASEONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1_local_5(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(2));

result = vpx_method_Set_20_Arrow_20_Cursor(PARAMETERS);

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(0));

result = kSuccess;
FAILONSUCCESS

FOOTER(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_4(PARAMETERS,TERMINAL(2),ROOT(4),ROOT(5));
NEXTCASEONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_5(PARAMETERS,TERMINAL(3),TERMINAL(1),ROOT(6));
NEXTCASEONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1_local_6(PARAMETERS,TERMINAL(6),TERMINAL(0),TERMINAL(1),TERMINAL(4),TERMINAL(5));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(6));

result = kSuccess;

FOOTER(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Delete_20_Items(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"/Delete Items",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_View,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(5),TERMINAL(1),ROOT(6));

result = vpx_method_New_20_Callback(PARAMETERS,TERMINAL(2),TERMINAL(6),NONE,ROOT(7));

result = vpx_method_Execute_20_Callback_20_Deferred(PARAMETERS,TERMINAL(7),NONE,NONE,NONE,NONE);

result = kSuccess;

FOOTERSINGLECASEWITHNONE(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(10)
INPUT(0,0)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kWindowContentRgn,ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(0),ROOT(2));

PUTINTEGER(GetWindowBounds( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(4),TERMINAL(2)),GETINTEGER(TERMINAL(1)),GETPOINTER(8,Rect,*,ROOT(5),NONE)),3);
result = kSuccess;

result = vpx_extget(PARAMETERS,"left",1,2,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_extget(PARAMETERS,"top",1,2,TERMINAL(5),ROOT(8),ROOT(9));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
OUTPUT(1,TERMINAL(9))
FOOTERSINGLECASEWITHNONE(10)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"VPLWindow Item",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(0),ROOT(2));

UnionRect( GETCONSTPOINTER(Rect,*,TERMINAL(2)),GETCONSTPOINTER(Rect,*,TERMINAL(1)),GETPOINTER(8,Rect,*,ROOT(3),TERMINAL(2)));
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Rect_2D_to_2D_list,1,1,TERMINAL(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Rect_2D_to_2D_list,1,1,TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0,V_Object *root1,V_Object *root2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0,V_Object *root1,V_Object *root2)
{
HEADER(17)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame_20_In_20_Window,1,1,TERMINAL(0),ROOT(5));

PUTPOINTER(OpaqueRgnHandle,*,NewRgn(),6);
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(5),ROOT(7));

result = vpx_constant(PARAMETERS,"-2",ROOT(8));

InsetRect( GETPOINTER(8,Rect,*,ROOT(9),TERMINAL(7)),GETINTEGER(TERMINAL(8)),GETINTEGER(TERMINAL(8)));
result = kSuccess;

OffsetRect( GETPOINTER(8,Rect,*,ROOT(10),TERMINAL(9)),GETINTEGER(TERMINAL(1)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

RectRgn( GETPOINTER(0,OpaqueRgnHandle,*,ROOT(11),TERMINAL(6)),GETCONSTPOINTER(Rect,*,TERMINAL(10)));
result = kSuccess;

UnionRgn( GETPOINTER(0,OpaqueRgnHandle,*,ROOT(12),TERMINAL(3)),GETPOINTER(0,OpaqueRgnHandle,*,ROOT(13),TERMINAL(6)),GETPOINTER(0,OpaqueRgnHandle,*,ROOT(14),TERMINAL(3)));
result = kSuccess;

DisposeRgn( GETPOINTER(0,OpaqueRgnHandle,*,ROOT(15),TERMINAL(6)));
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1_local_12(PARAMETERS,TERMINAL(4),TERMINAL(9),ROOT(16));

result = kSuccess;

OUTPUT(0,TERMINAL(10))
OUTPUT(1,TERMINAL(14))
OUTPUT(2,TERMINAL(16))
FOOTER(17)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0,V_Object *root1,V_Object *root2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0,V_Object *root1,V_Object *root2)
{
HEADERWITHNONE(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
OUTPUT(1,TERMINAL(3))
OUTPUT(2,TERMINAL(4))
FOOTERWITHNONE(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0,V_Object *root1,V_Object *root2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0,V_Object *root1,V_Object *root2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0,root1,root2))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0,root1,root2);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Point",ROOT(2));

result = vpx_constant(PARAMETERS,"4",ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_make_2D_external,2,1,TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_extset(PARAMETERS,"v",2,1,TERMINAL(4),TERMINAL(0),ROOT(5));

result = vpx_extset(PARAMETERS,"h",2,1,TERMINAL(4),TERMINAL(1),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Add_20_Point(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1_local_4(PARAMETERS,TERMINAL(4),TERMINAL(5),ROOT(6));

SubPt( GETSTRUCTURE(4,Point,TERMINAL(0)),GETPOINTER(4,Point,*,ROOT(7),TERMINAL(6)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTER(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Point",ROOT(2));

result = vpx_constant(PARAMETERS,"4",ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_make_2D_external,2,1,TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_extset(PARAMETERS,"v",2,1,TERMINAL(4),TERMINAL(0),ROOT(5));

result = vpx_extset(PARAMETERS,"h",2,1,TERMINAL(4),TERMINAL(1),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(10)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,1,TERMINAL(1),ROOT(3));

result = vpx_extget(PARAMETERS,"top",1,2,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_extget(PARAMETERS,"left",1,2,TERMINAL(3),ROOT(6),ROOT(7));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3_local_6(PARAMETERS,TERMINAL(5),TERMINAL(7),ROOT(8));

SubPt( GETSTRUCTURE(4,Point,TERMINAL(0)),GETPOINTER(4,Point,*,ROOT(9),TERMINAL(8)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(9))
FOOTER(10)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1,V_Object *root2,V_Object *root3);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1,V_Object *root2,V_Object *root3)
{
HEADER(17)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

PUTPOINTER(OpaqueRgnHandle,*,NewRgn(),4);
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_5(PARAMETERS,TERMINAL(3),ROOT(5),ROOT(6));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(1))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_6(PARAMETERS,LIST(1),ROOT(7));
LISTROOT(7,0)
REPEATFINISH
LISTROOTFINISH(7,0)
LISTROOTEND
} else {
ROOTEMPTY(7)
}

result = vpx_constant(PARAMETERS,"NULL",ROOT(8));

result = vpx_constant(PARAMETERS,"1",ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_split_2D_nth,2,2,TERMINAL(7),TERMINAL(9),ROOT(10),ROOT(11));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(10))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
LOOPTERMINALBEGIN(2)
LOOPTERMINAL(0,4,13)
LOOPTERMINAL(1,8,14)
result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_10(PARAMETERS,LIST(10),TERMINAL(5),TERMINAL(6),LOOP(0),LOOP(1),ROOT(12),ROOT(13),ROOT(14));
LISTROOT(12,0)
REPEATFINISH
LISTROOTFINISH(12,0)
LISTROOTEND
} else {
ROOTEMPTY(12)
ROOTNULL(13,TERMINAL(4))
ROOTNULL(14,TERMINAL(8))
}

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(6),TERMINAL(5),ROOT(15));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2_case_1_local_12(PARAMETERS,TERMINAL(2),TERMINAL(14),TERMINAL(15),ROOT(16));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
OUTPUT(1,TERMINAL(16))
OUTPUT(2,TERMINAL(13))
OUTPUT(3,TERMINAL(14))
FOOTERSINGLECASE(17)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_extget(PARAMETERS,"h",1,2,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_extget(PARAMETERS,"v",1,2,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_method_New_20_CGPoint(PARAMETERS,TERMINAL(2),TERMINAL(4),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4)
{
HEADERWITHNONE(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_Offscreen_20_Image,3,2,TERMINAL(1),TERMINAL(2),TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(6));
TERMINATEONSUCCESS

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_1_local_4(PARAMETERS,TERMINAL(3),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_CGImage,4,0,TERMINAL(0),TERMINAL(5),TERMINAL(7),NONE);
CONTINUEONFAILURE

CGImageRelease( GETPOINTER(0,CGImage,*,ROOT(8),TERMINAL(5)));
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(6));

result = kSuccess;

FOOTERWITHNONE(9)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame,1,1,TERMINAL(0),ROOT(3));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"( )",ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP__3D_,2,1,TERMINAL(1),TERMINAL(4),ROOT(5));

result = vpx_method_Rectangle_20_Size(PARAMETERS,TERMINAL(3),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(6),ROOT(7),ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP_max,2,1,TERMINAL(7),TERMINAL(2),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_choose,3,1,TERMINAL(5),TERMINAL(7),TERMINAL(9),ROOT(10));

result = kSuccess;

OUTPUT(0,TERMINAL(8))
OUTPUT(1,TERMINAL(10))
FOOTERSINGLECASE(11)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADERWITHNONE(12)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"42",ROOT(3));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_3_case_1_local_3(PARAMETERS,TERMINAL(1),TERMINAL(2),TERMINAL(3),ROOT(4),ROOT(5));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_32_2D_RGBA,4,0,TERMINAL(0),TERMINAL(4),TERMINAL(5),NONE);
FAILONFAILURE

result = vpx_constant(PARAMETERS,"0.0",ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Translate_20_CTM,3,0,TERMINAL(0),TERMINAL(6),TERMINAL(5));

result = vpx_constant(PARAMETERS,"1.0",ROOT(7));

result = vpx_constant(PARAMETERS,"-1.0",ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Scale_20_CTM,3,0,TERMINAL(0),TERMINAL(7),TERMINAL(8));

result = vpx_constant(PARAMETERS,"0",ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(9),TERMINAL(5),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,4,1,TERMINAL(9),TERMINAL(9),TERMINAL(5),TERMINAL(10),ROOT(11));

result = kSuccess;

OUTPUT(0,TERMINAL(11))
FOOTERSINGLECASEWITHNONE(12)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Location,1,1,TERMINAL(0),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Window_20_Origin,TERMINAL(0),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,3,1,TERMINAL(3),TERMINAL(2),TERMINAL(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_constant(PARAMETERS,"( 0 0 )",ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Origin,TERMINAL(0),TERMINAL(3),ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(4),TERMINAL(2),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Location,2,0,TERMINAL(5),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Reset_20_Cache,1,0,TERMINAL(5));

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5_case_1_local_2(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_8_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_8_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame,1,1,TERMINAL(0),ROOT(2));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"( )",ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__3D_,2,1,TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_method_Rectangle_20_Size(PARAMETERS,TERMINAL(2),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_constant(PARAMETERS,"42",ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP_max,2,1,TERMINAL(6),TERMINAL(8),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_choose,3,1,TERMINAL(4),TERMINAL(6),TERMINAL(9),ROOT(10));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
OUTPUT(1,TERMINAL(10))
FOOTERSINGLECASE(11)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(12)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame,1,1,TERMINAL(1),ROOT(3));
FAILONFAILURE

result = vpx_method_Rectangle_20_Size(PARAMETERS,TERMINAL(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_constant(PARAMETERS,"0.0",ROOT(7));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_8_case_1_local_6(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(8),ROOT(9));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Translate_20_CTM,3,0,TERMINAL(0),TERMINAL(7),TERMINAL(9));

result = vpx_constant(PARAMETERS,"1.0",ROOT(10));

result = vpx_constant(PARAMETERS,"-1.0",ROOT(11));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Scale_20_CTM,3,0,TERMINAL(0),TERMINAL(10),TERMINAL(11));

result = kSuccess;

FOOTERSINGLECASE(12)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,3,TERMINAL(1),ROOT(2),ROOT(3),ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(3),ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Origin,TERMINAL(5),TERMINAL(4),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Location,2,0,TERMINAL(6),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_CGContextRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_constant(PARAMETERS,"1.0",ROOT(2));

result = vpx_constant(PARAMETERS,"1.0",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Fill_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Stroke_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(20)
result = kSuccess;

result = vpx_constant(PARAMETERS,"31",ROOT(0));

result = vpx_constant(PARAMETERS,"HIThemeTextInfo",ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_make_2D_external,2,1,TERMINAL(1),TERMINAL(0),ROOT(2));

result = vpx_extconstant(PARAMETERS,kHIThemeTextInfoVersionZero,ROOT(3));

result = vpx_extset(PARAMETERS,"version",2,1,TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_extconstant(PARAMETERS,kThemeSmallEmphasizedSystemFont,ROOT(5));

result = vpx_extconstant(PARAMETERS,kThemeEmphasizedSystemFont,ROOT(6));

result = vpx_extset(PARAMETERS,"state",2,1,TERMINAL(4),TERMINAL(6),ROOT(7));

result = vpx_extconstant(PARAMETERS,kThemeStateActive,ROOT(8));

result = vpx_extset(PARAMETERS,"fontID",2,1,TERMINAL(7),TERMINAL(8),ROOT(9));

result = vpx_extconstant(PARAMETERS,kHIThemeTextHorizontalFlushCenter,ROOT(10));

result = vpx_extset(PARAMETERS,"horizontalFlushness",2,1,TERMINAL(9),TERMINAL(10),ROOT(11));

result = vpx_extconstant(PARAMETERS,kHIThemeTextVerticalFlushCenter,ROOT(12));

result = vpx_extset(PARAMETERS,"verticalFlushness",2,1,TERMINAL(11),TERMINAL(12),ROOT(13));

result = vpx_constant(PARAMETERS,"0",ROOT(14));

result = vpx_extset(PARAMETERS,"options",2,1,TERMINAL(13),TERMINAL(14),ROOT(15));

result = vpx_extconstant(PARAMETERS,kHIThemeTextTruncationNone,ROOT(16));

result = vpx_extset(PARAMETERS,"truncationPosition",2,1,TERMINAL(15),TERMINAL(16),ROOT(17));

result = vpx_constant(PARAMETERS,"1",ROOT(18));

result = vpx_extset(PARAMETERS,"truncationMaxLines",2,1,TERMINAL(17),TERMINAL(18),ROOT(19));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(20)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(0),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__2B_1,1,1,TERMINAL(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_to_2D_string,1,1,TERMINAL(4),ROOT(5));

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(5),ROOT(6));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_6(PARAMETERS,TERMINAL(1),ROOT(7));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11_case_1_local_7(PARAMETERS,ROOT(8));

result = vpx_extconstant(PARAMETERS,kHIThemeOrientationInverted,ROOT(9));

PUTINTEGER(HIThemeDrawTextBox( GETCONSTPOINTER(void,*,TERMINAL(6)),GETCONSTPOINTER(CGRect,*,TERMINAL(2)),GETPOINTER(40,HIThemeTextInfo,*,ROOT(11),TERMINAL(8)),GETPOINTER(0,CGContext,*,ROOT(12),TERMINAL(7)),GETINTEGER(TERMINAL(9))),10);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(6)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(13)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_constant(PARAMETERS,"10",ROOT(3));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(2),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Add_20_Star,3,0,TERMINAL(1),TERMINAL(4),TERMINAL(3));

result = vpx_constant(PARAMETERS,"( 0.063 0.4080 0.5920 0.92 )",ROOT(5));

result = vpx_constant(PARAMETERS,"( 0.063 .04080 0.5920 1 )",ROOT(6));

result = vpx_constant(PARAMETERS,"( 0.537 0.129 0.165 0.92 )",ROOT(7));

result = vpx_constant(PARAMETERS,"( 0.537 0.129 0.165 1 )",ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Fill_20_And_20_Stroke_20_Path,3,0,TERMINAL(1),TERMINAL(7),TERMINAL(8));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10_case_1_local_11(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(2),TERMINAL(3),ROOT(4));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"( 0 0 )",ROOT(5));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_5(PARAMETERS,TERMINAL(2),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"NULL",ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Draw_20_CG,3,0,TERMINAL(2),TERMINAL(7),TERMINAL(0));
FAILONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_8(PARAMETERS,TERMINAL(0),TERMINAL(2),TERMINAL(3));
FAILONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_9(PARAMETERS,TERMINAL(2),TERMINAL(6));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1_local_10(PARAMETERS,TERMINAL(3),TERMINAL(0),TERMINAL(4));

result = kSuccess;

FOOTER(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"16",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_32_2D_RGBA,4,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),NONE);
FAILONFAILURE

result = vpx_constant(PARAMETERS,"0",ROOT(3));

result = vpx_constant(PARAMETERS,"1.0",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Fill_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(3),TERMINAL(4),TERMINAL(3),TERMINAL(4));

result = vpx_constant(PARAMETERS,"( 0 0 16 16 )",ROOT(5));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(5),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Fill_20_Rect,2,0,TERMINAL(0),TERMINAL(6));

result = kSuccess;

FOOTERWITHNONE(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_CG_20_Bitmap_20_Context,1,1,NONE,ROOT(2));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1_local_3(PARAMETERS,TERMINAL(2),TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_Image,1,1,TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
OUTPUT(1,TERMINAL(2))
FOOTERWITHNONE(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
OUTPUT(1,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_extget(PARAMETERS,"h",1,2,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_extget(PARAMETERS,"v",1,2,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_method_New_20_CGPoint(PARAMETERS,TERMINAL(2),TERMINAL(4),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4)
{
HEADERWITHNONE(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_2(PARAMETERS,TERMINAL(2),NONE,ROOT(5),ROOT(6));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(6));
TERMINATEONSUCCESS

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2_local_4(PARAMETERS,TERMINAL(3),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Drag_20_CGImage,4,0,TERMINAL(0),TERMINAL(5),TERMINAL(7),NONE);
CONTINUEONFAILURE

CGImageRelease( GETPOINTER(0,CGImage,*,ROOT(8),TERMINAL(5)));
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(6));

result = kSuccess;

FOOTERWITHNONE(9)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4))
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(2),TERMINAL(3),ROOT(4),ROOT(5),ROOT(6),ROOT(7));
FAILONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image_case_1_local_3(PARAMETERS,TERMINAL(1),TERMINAL(0),TERMINAL(4),TERMINAL(5),TERMINAL(7));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame,1,1,TERMINAL(0),ROOT(3));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"( )",ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP__3D_,2,1,TERMINAL(1),TERMINAL(4),ROOT(5));

result = vpx_method_Rectangle_20_Size(PARAMETERS,TERMINAL(3),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(6),ROOT(7),ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP_max,2,1,TERMINAL(7),TERMINAL(2),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_choose,3,1,TERMINAL(5),TERMINAL(7),TERMINAL(9),ROOT(10));

result = kSuccess;

OUTPUT(0,TERMINAL(8))
OUTPUT(1,TERMINAL(10))
FOOTERSINGLECASE(11)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADERWITHNONE(12)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"42",ROOT(3));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_3_case_1_local_3(PARAMETERS,TERMINAL(1),TERMINAL(2),TERMINAL(3),ROOT(4),ROOT(5));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_32_2D_RGBA,4,0,TERMINAL(0),TERMINAL(4),TERMINAL(5),NONE);
FAILONFAILURE

result = vpx_constant(PARAMETERS,"0.0",ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Translate_20_CTM,3,0,TERMINAL(0),TERMINAL(6),TERMINAL(5));

result = vpx_constant(PARAMETERS,"1.0",ROOT(7));

result = vpx_constant(PARAMETERS,"-1.0",ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Scale_20_CTM,3,0,TERMINAL(0),TERMINAL(7),TERMINAL(8));

result = vpx_constant(PARAMETERS,"0",ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(9),TERMINAL(5),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,4,1,TERMINAL(9),TERMINAL(9),TERMINAL(5),TERMINAL(10),ROOT(11));

result = kSuccess;

OUTPUT(0,TERMINAL(11))
FOOTERSINGLECASEWITHNONE(12)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Location,1,1,TERMINAL(0),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Window_20_Origin,TERMINAL(0),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,3,1,TERMINAL(3),TERMINAL(2),TERMINAL(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_constant(PARAMETERS,"( 0 0 )",ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Origin,TERMINAL(0),TERMINAL(3),ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(4),TERMINAL(2),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Location,2,0,TERMINAL(5),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Reset_20_Cache,1,0,TERMINAL(5));

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5_case_1_local_2(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_8_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_8_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame,1,1,TERMINAL(0),ROOT(2));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"( )",ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__3D_,2,1,TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_method_Rectangle_20_Size(PARAMETERS,TERMINAL(2),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_constant(PARAMETERS,"42",ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP_max,2,1,TERMINAL(6),TERMINAL(8),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_choose,3,1,TERMINAL(4),TERMINAL(6),TERMINAL(9),ROOT(10));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
OUTPUT(1,TERMINAL(10))
FOOTERSINGLECASE(11)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(12)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame,1,1,TERMINAL(1),ROOT(3));
FAILONFAILURE

result = vpx_method_Rectangle_20_Size(PARAMETERS,TERMINAL(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_constant(PARAMETERS,"0.0",ROOT(7));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_8_case_1_local_6(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(8),ROOT(9));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Translate_20_CTM,3,0,TERMINAL(0),TERMINAL(7),TERMINAL(9));

result = vpx_constant(PARAMETERS,"1.0",ROOT(10));

result = vpx_constant(PARAMETERS,"-1.0",ROOT(11));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Scale_20_CTM,3,0,TERMINAL(0),TERMINAL(10),TERMINAL(11));

result = kSuccess;

FOOTERSINGLECASE(12)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,3,TERMINAL(1),ROOT(2),ROOT(3),ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(3),ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Origin,TERMINAL(5),TERMINAL(4),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Location,2,0,TERMINAL(6),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_CGContextRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_constant(PARAMETERS,"1.0",ROOT(2));

result = vpx_constant(PARAMETERS,"1.0",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Fill_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Stroke_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(20)
result = kSuccess;

result = vpx_constant(PARAMETERS,"31",ROOT(0));

result = vpx_constant(PARAMETERS,"HIThemeTextInfo",ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_make_2D_external,2,1,TERMINAL(1),TERMINAL(0),ROOT(2));

result = vpx_extconstant(PARAMETERS,kHIThemeTextInfoVersionZero,ROOT(3));

result = vpx_extset(PARAMETERS,"version",2,1,TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_extconstant(PARAMETERS,kThemeSmallEmphasizedSystemFont,ROOT(5));

result = vpx_extconstant(PARAMETERS,kThemeEmphasizedSystemFont,ROOT(6));

result = vpx_extset(PARAMETERS,"state",2,1,TERMINAL(4),TERMINAL(6),ROOT(7));

result = vpx_extconstant(PARAMETERS,kThemeStateActive,ROOT(8));

result = vpx_extset(PARAMETERS,"fontID",2,1,TERMINAL(7),TERMINAL(8),ROOT(9));

result = vpx_extconstant(PARAMETERS,kHIThemeTextHorizontalFlushCenter,ROOT(10));

result = vpx_extset(PARAMETERS,"horizontalFlushness",2,1,TERMINAL(9),TERMINAL(10),ROOT(11));

result = vpx_extconstant(PARAMETERS,kHIThemeTextVerticalFlushCenter,ROOT(12));

result = vpx_extset(PARAMETERS,"verticalFlushness",2,1,TERMINAL(11),TERMINAL(12),ROOT(13));

result = vpx_constant(PARAMETERS,"0",ROOT(14));

result = vpx_extset(PARAMETERS,"options",2,1,TERMINAL(13),TERMINAL(14),ROOT(15));

result = vpx_extconstant(PARAMETERS,kHIThemeTextTruncationNone,ROOT(16));

result = vpx_extset(PARAMETERS,"truncationPosition",2,1,TERMINAL(15),TERMINAL(16),ROOT(17));

result = vpx_constant(PARAMETERS,"1",ROOT(18));

result = vpx_extset(PARAMETERS,"truncationMaxLines",2,1,TERMINAL(17),TERMINAL(18),ROOT(19));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(20)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(0),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__2B_1,1,1,TERMINAL(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_to_2D_string,1,1,TERMINAL(4),ROOT(5));

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(5),ROOT(6));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_6(PARAMETERS,TERMINAL(1),ROOT(7));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13_case_1_local_7(PARAMETERS,ROOT(8));

result = vpx_extconstant(PARAMETERS,kHIThemeOrientationInverted,ROOT(9));

PUTINTEGER(HIThemeDrawTextBox( GETCONSTPOINTER(void,*,TERMINAL(6)),GETCONSTPOINTER(CGRect,*,TERMINAL(2)),GETPOINTER(40,HIThemeTextInfo,*,ROOT(11),TERMINAL(8)),GETPOINTER(0,CGContext,*,ROOT(12),TERMINAL(7)),GETINTEGER(TERMINAL(9))),10);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(6)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(13)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADERWITHNONE(11)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_constant(PARAMETERS,"10",ROOT(3));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(2),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Add_20_Burst,4,0,TERMINAL(1),NONE,TERMINAL(4),TERMINAL(3));

result = vpx_constant(PARAMETERS,"( 0.063 0.4080 0.5920 0.92 )",ROOT(5));

result = vpx_constant(PARAMETERS,"( 0.063 .04080 0.5920 1 )",ROOT(6));

result = vpx_constant(PARAMETERS,"( 0.537 0.129 0.165 0.92 )",ROOT(7));

result = vpx_constant(PARAMETERS,"( 0.537 0.129 0.165 1 )",ROOT(8));

result = vpx_constant(PARAMETERS,"( 1 0.2 0.2 1 )",ROOT(9));

result = vpx_constant(PARAMETERS,"( 0 0 0 1 )",ROOT(10));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Fill_20_And_20_Stroke_20_Path,3,0,TERMINAL(1),TERMINAL(9),TERMINAL(10));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10_case_1_local_13(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(11)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(2),TERMINAL(3),ROOT(4));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"( 0 0 )",ROOT(5));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_5(PARAMETERS,TERMINAL(2),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"NULL",ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Draw_20_CG,3,0,TERMINAL(2),TERMINAL(7),TERMINAL(0));
FAILONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_8(PARAMETERS,TERMINAL(0),TERMINAL(2),TERMINAL(3));
FAILONFAILURE

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_9(PARAMETERS,TERMINAL(2),TERMINAL(6));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1_local_10(PARAMETERS,TERMINAL(3),TERMINAL(0),TERMINAL(4));

result = kSuccess;

FOOTER(8)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"16",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_32_2D_RGBA,4,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),NONE);
FAILONFAILURE

result = vpx_constant(PARAMETERS,"0",ROOT(3));

result = vpx_constant(PARAMETERS,"1.0",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Fill_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(3),TERMINAL(4),TERMINAL(3),TERMINAL(4));

result = vpx_constant(PARAMETERS,"( 0 0 16 16 )",ROOT(5));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(5),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Fill_20_Rect,2,0,TERMINAL(0),TERMINAL(6));

result = kSuccess;

FOOTERWITHNONE(7)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_CG_20_Bitmap_20_Context,1,1,NONE,ROOT(3));

result = vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_Image,1,1,TERMINAL(3),ROOT(4));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(4))
OUTPUT(1,TERMINAL(3))
FOOTERWITHNONE(5)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
OUTPUT(1,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0,root1))
vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0,root1);
return outcome;
}

/* Stop Universals */






Nat4	loadClasses_Drag_202620_Drop(V_Environment environment);
Nat4	loadClasses_Drag_202620_Drop(V_Environment environment)
{
	V_Class result = NULL;

	result = class_new("Abstract Drag & Drop Reference",environment);
	if(result == NULL) return kERROR;
	VPLC_Abstract_20_Drag_202620_Drop_20_Reference_class_load(result,environment);
	result = class_new("Drop Reference",environment);
	if(result == NULL) return kERROR;
	VPLC_Drop_20_Reference_class_load(result,environment);
	result = class_new("Drag Reference",environment);
	if(result == NULL) return kERROR;
	VPLC_Drag_20_Reference_class_load(result,environment);
	result = class_new("Abstract Drag Handler",environment);
	if(result == NULL) return kERROR;
	VPLC_Abstract_20_Drag_20_Handler_class_load(result,environment);
	result = class_new("Window Drag Handler",environment);
	if(result == NULL) return kERROR;
	VPLC_Window_20_Drag_20_Handler_class_load(result,environment);
	return kNOERROR;

}

Nat4	loadUniversals_Drag_202620_Drop(V_Environment environment);
Nat4	loadUniversals_Drag_202620_Drop(V_Environment environment)
{
	V_Method result = NULL;

	result = method_new("Abstract Drag & Drop Reference/Get DragRef",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_DragRef,NULL);

	result = method_new("Abstract Drag & Drop Reference/Set DragRef",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_DragRef,NULL);

	result = method_new("Abstract Drag & Drop Reference/New",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New,NULL);

	result = method_new("Abstract Drag & Drop Reference/Release",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Release,NULL);

	result = method_new("Abstract Drag & Drop Reference/Show Hilite",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Show_20_Hilite,NULL);

	result = method_new("Abstract Drag & Drop Reference/Hide Hilite",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Hide_20_Hilite,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Attributes",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Attributes,NULL);

	result = method_new("Abstract Drag & Drop Reference/Has Left Sender Window?",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Has_20_Left_20_Sender_20_Window_3F_,NULL);

	result = method_new("Abstract Drag & Drop Reference/Inside Sender Window?",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Inside_20_Sender_20_Window_3F_,NULL);

	result = method_new("Abstract Drag & Drop Reference/Inside Sender Application?",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Inside_20_Sender_20_Application_3F_,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Drag Pasteboard",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Pasteboard,NULL);

	result = method_new("Abstract Drag & Drop Reference/Find Tasty Flavors?",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Find_20_Tasty_20_Flavors_3F_,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Values As UTIs",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Values_20_As_20_UTIs,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Pasteboard",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Pasteboard,NULL);

	result = method_new("Abstract Drag & Drop Reference/Set Pasteboard",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Pasteboard,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Item Bounds",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Item_20_Bounds,NULL);

	result = method_new("Abstract Drag & Drop Reference/Set Item Bounds",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Item_20_Bounds,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Total Bounds",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Total_20_Bounds,NULL);

	result = method_new("Abstract Drag & Drop Reference/Count Drag Items",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Count_20_Drag_20_Items,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Drag Mouse",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Mouse,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Drag Origin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Origin,NULL);

	result = method_new("Abstract Drag & Drop Reference/New With Pasteboard",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_New_20_With_20_Pasteboard,NULL);

	result = method_new("Abstract Drag & Drop Reference/Track Drag",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Track_20_Drag,NULL);

	result = method_new("Abstract Drag & Drop Reference/Set Drag CGImage",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Drag_20_CGImage,NULL);

	result = method_new("Abstract Drag & Drop Reference/Set Drag Allowable Actions",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Drag_20_Allowable_20_Actions,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Drag Allowable Actions",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Allowable_20_Actions,NULL);

	result = method_new("Abstract Drag & Drop Reference/Set Drag Drop Action",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Set_20_Drag_20_Drop_20_Action,NULL);

	result = method_new("Abstract Drag & Drop Reference/Get Drag Drop Action",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_202620_Drop_20_Reference_2F_Get_20_Drag_20_Drop_20_Action,NULL);

	result = method_new("Drop Reference/New",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drop_20_Reference_2F_New,NULL);

	result = method_new("Drag Reference/Standard Drag Input Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drag_20_Reference_2F_Standard_20_Drag_20_Input_20_Callback,NULL);

	result = method_new("Drag Reference/Open Drag Input Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drag_20_Reference_2F_Open_20_Drag_20_Input_20_Callback,NULL);

	result = method_new("Drag Reference/Close Drag Input Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drag_20_Reference_2F_Close_20_Drag_20_Input_20_Callback,NULL);

	result = method_new("Drag Reference/Get Drag Input Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drag_20_Reference_2F_Get_20_Drag_20_Input_20_Callback,NULL);

	result = method_new("Drag Reference/Set Drag Input Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drag_20_Reference_2F_Set_20_Drag_20_Input_20_Callback,NULL);

	result = method_new("Drag Reference/Set Drag Input Proc",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drag_20_Reference_2F_Set_20_Drag_20_Input_20_Proc,NULL);

	result = method_new("Drag Reference/Track Drag",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Drag_20_Reference_2F_Track_20_Drag,NULL);

	result = method_new("Abstract Drag Handler/Default The Drag",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Default_20_The_20_Drag,NULL);

	result = method_new("Abstract Drag Handler/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Open,NULL);

	result = method_new("Abstract Drag Handler/Close",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Close,NULL);

	result = method_new("Abstract Drag Handler/Get Owner",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Owner,NULL);

	result = method_new("Abstract Drag Handler/Set Owner",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_Owner,NULL);

	result = method_new("Abstract Drag Handler/Get The Drag",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_The_20_Drag,NULL);

	result = method_new("Abstract Drag Handler/Set The Drag",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_The_20_Drag,NULL);

	result = method_new("Abstract Drag Handler/Get Drop Flavors",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavors,NULL);

	result = method_new("Abstract Drag Handler/Set Drop Flavors",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_Drop_20_Flavors,NULL);

	result = method_new("Abstract Drag Handler/Get Drop Flavor UTIs",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Drop_20_Flavor_20_UTIs,NULL);

	result = method_new("Abstract Drag Handler/Get Current Drop Flavors",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Current_20_Drop_20_Flavors,NULL);

	result = method_new("Abstract Drag Handler/Set Current Drop Flavors",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Set_20_Current_20_Drop_20_Flavors,NULL);

	result = method_new("Abstract Drag Handler/Any Drop Flavors?",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Any_20_Drop_20_Flavors_3F_,NULL);

	result = method_new("Abstract Drag Handler/Check Drop Flavors",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Drop_20_Flavors,NULL);

	result = method_new("Abstract Drag Handler/Check Tasty Flavors",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Check_20_Tasty_20_Flavors,NULL);

	result = method_new("Abstract Drag Handler/Extract Drop Flavors",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Extract_20_Drop_20_Flavors,NULL);

	result = method_new("Abstract Drag Handler/Dispose Drop",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Dispose_20_Drop,NULL);

	result = method_new("Abstract Drag Handler/Install Handlers",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Install_20_Handlers,NULL);

	result = method_new("Abstract Drag Handler/Remove Handlers",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Remove_20_Handlers,NULL);

	result = method_new("Abstract Drag Handler/Show Hilite",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Show_20_Hilite,NULL);

	result = method_new("Abstract Drag Handler/Hide Hilite",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Hide_20_Hilite,NULL);

	result = method_new("Abstract Drag Handler/Get Tasty Pasteboard",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Abstract_20_Drag_20_Handler_2F_Get_20_Tasty_20_Pasteboard,NULL);

	result = method_new("Window Drag Handler/Do Tracking Handler",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Tracking_20_Handler,NULL);

	result = method_new("Window Drag Handler/Do Receive Handler",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Do_20_Receive_20_Handler,NULL);

	result = method_new("Window Drag Handler/Drag In Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_In_20_Window,NULL);

	result = method_new("Window Drag Handler/Drag Track In Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Track_20_In_20_Window,NULL);

	result = method_new("Window Drag Handler/Drag Enter Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Window,NULL);

	result = method_new("Window Drag Handler/Drag Leave Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Leave_20_Window,NULL);

	result = method_new("Window Drag Handler/Drag Enter Handler",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Enter_20_Handler,NULL);

	result = method_new("Window Drag Handler/Drag Leave Handler",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Leave_20_Handler,NULL);

	result = method_new("Window Drag Handler/Drag Receive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Receive,NULL);

	result = method_new("Window Drag Handler/Remove Handlers",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Remove_20_Handlers,NULL);

	result = method_new("Window Drag Handler/Install Handlers",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Install_20_Handlers,NULL);

	result = method_new("Window Drag Handler/Show Hilite",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Show_20_Hilite,NULL);

	result = method_new("Window Drag Handler/Hide Hilite",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Hide_20_Hilite,NULL);

	result = method_new("Window Drag Handler/Accept Drop",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Accept_20_Drop,NULL);

	result = method_new("Window Drag Handler/Get Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Get_20_Window,NULL);

	result = method_new("Window Drag Handler/Drag Items",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Drag_20_Items,NULL);

	result = method_new("Window Drag Handler/Delete Items",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Delete_20_Items,NULL);

	result = method_new("Window Drag Handler/Create Drag Image",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Drag_20_Image,NULL);

	result = method_new("Window Drag Handler/Create Offscreen Image",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Window_20_Drag_20_Handler_2F_Create_20_Offscreen_20_Image,NULL);

	return kNOERROR;

}



Nat4	loadPersistents_Drag_202620_Drop(V_Environment environment);
Nat4	loadPersistents_Drag_202620_Drop(V_Environment environment)
{
	V_Value tempPersistent = NULL;
	V_Dictionary dictionary = environment->persistentsTable;

	tempPersistent = create_persistentNode("Current Drag",NULL,environment);
	add_node(dictionary,tempPersistent->objectName,tempPersistent);

	return kNOERROR;

}

Nat4	load_Drag_202620_Drop(V_Environment environment);
Nat4	load_Drag_202620_Drop(V_Environment environment)
{

	loadClasses_Drag_202620_Drop(environment);
	loadUniversals_Drag_202620_Drop(environment);
	loadPersistents_Drag_202620_Drop(environment);
	return kNOERROR;

}

