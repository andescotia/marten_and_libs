#!/usr/bin/env bash

set -e
set -x

XC=xcodebuild

CURRENT_BUILD='(no build)'
trap 'echo Error building project $CURRENT_BUILD' ERR

build()
{
	local Project="$1"
	local Configuration="${2:-Debug}"
	local Target="${3:-$Project}"

	CURRENT_BUILD="$Project/$Configuration/$Target"

	pushd Xcode/"$Project"
	$XC -target "$Target" -configuration "$Configuration"
	popd
}

cd $(dirname "$0")

build MartenEngine
build MartenInfo
build MartenUIAqua
build MartenSystem
build MartenStandard
build MartenBSD
build MartenInterpreter
build MartenInterpreterApp "" "Marten Interpreter_d App"
build MartenInterpreterApp "" "marten-ic"
build MartenMacOSCarbon

build "Marten IDE" "" Marten
