# Journal

## 2022-01-18

Getting the IDE built from source.

Hurray! I managed to build the IDE from source code. I bought an old iMac5,1 from eBay and installed macOS 10.6.8 on it.
I had to try a few ISOs from [archive.org]() before I succeeded, but I got there in the end. Initially I thought I'd need
an older version of MacOS X, so tried 10.4 Tiger, with no success in booting any installation media, then 10.5 Leopard.
But on Leopard I realised that the code was configured to use the 10.6 Snow Leopard SDK, so I didn't spend much time and
upgraded to 10.6. I installed Xcode 3.2 from the installation disc and I chipped away until I got the IDE compiled and running.

I had tried to do this with virtual machines on my current Mac, but the responsiveness was abysmal. Snow Leopard is especially
slow due to its UI element animations that cannot be disabled. Attempting to move the mouse, click a button, or do anything while
a modal with a flashing button is displayed takes A LOT of patience.

I had also tried to bite the bullet and build directly for 64-bit, when I realised that Carbon needs to be excised and
replaced with something else (I don't know what yet), and all the NIBs were inaccessible in current Xcode.

## 2022-01-19

Modernising the source.

I'm converting all the .nib files (Interface Builder Carbon Document NIB 2.x) to .xib (Interface Builder Carbon Document XIB 3.x).

Apple did "move fast and break thinks" [1](https://www.explainxkcd.com/wiki/index.php/1428:_Move_Fast_and_Break_Things):

* Xcode 4.2 doesn't have the Interface Builder anymore, and cannot display, edit or manipulate the old NIBs
* ibtool from Xcode 4.2 throws lots of errors about unknown NIB format as well when trying to use it to get XIBs.
* I've installed Xcode 3.2.2 and ibtool converted the files. I've updated the projects to reference the XIBs.

Xcode 3.2 keeps beachballing on me when editing the Marten IDE project. I hope that 4.2 will behave better once I get to use it.

For time travel purposes: Xcode 3.2 is the one that MacOS X 10.6 comes with, and Xcode 4.2 is the latest one that 10.6 can run.

Xcode 4 cannot edit Carbon XIBs, even though it can still building them. Apple's solution is to use Interface Builder 3.2
to edit these. That means I now have both Xcode 4.2 and 3.2 installed together.

Rebuilding the XIBs with Cocoa: of course the coordinate system is reversed, with Carbon starting at the upper left corner of
the window, while Cocoa starts at the bottom left... Why make my life too easy?

## 2022-01-20

MartenUIAqua is now using Cocoa (mostly)

After a whole day's effort, I've converted the MartenUIAqua UI primitives to Cocoa. I haven't touched the Graphics or Files
primitives just yet, but the UI primitives seem to work as well as they did with Carbon. I do not know what bugs I've introduced
that weren't there already.

The good news is that this work can be used as reference for further changes.

## 2022-01-21

Application Dock Tile no longer uses Carbon

I've replaced the Dock Tile code in MartenStandard so that it uses `[NSApp dockTile]` while keeping most of the original code
in place. I've implemented a callback stack mechanism in order to express the drawing desires while messing as little as possible
with the existing logic.

## 2022-01-21

Breakpoints

Do you know what rocks when debugging? Breakpoints. Do you know what helps a lot with setting breakpoints across inter-related projects?
Workspaces. I thought this might help for the entire day, but I've been hunting some gremlins, had a hard time, and decided to create one.
It is helping to remove some of the debugging delays. I broke something crucial somewhere I can't can't open or create new projects, and
I can't see what. It happened after I did the Application Dock Tile work, but I'm not sure there's any relation. I broke Interpreter_d too
by "cleaning up unneeded files".

Notes to self:.
- The whole `Interpreter_d.app` directory has to be copied into the `Marten` IDE bundle
- `MartenInfo` really wants its own local copy of `MartenInfo.framework` in order to function
- You cannot store raw pointers in NSArray, because it will attempt to _retain_ them and crash

I also cannot figure out how to stop the Interpreter_d icon from bouncing in the dock without the `EventAvail` trick.

## 2022-01-21

VPX loading

There appear to be two VPX project loading paths. Once of them is broken. You know you've taken the broken path if you're asked where
to _save_ your interpreter. You've taken the working one if you're asked to _name_ your app. The File/Open menu appears to always take the
broken path. Weirdly enough, it fails only if you don't have an interpreter app already - or it's unable to find it.

The other way to load projects is by dragging and dropping the vpx file. I seem to have a 50/50 chance of picking either path...

Why am I looking into this? Well... I didn't mention it before, but Marten is writen in Prograph. I have several copies of it, and each vpx
behaves differently when open, even though they're all IDENTICAL! The weirdest behaviour so far is that I copied one of those that does open
properly onto one that doesn't, and the latter opens properly too, although git says there's no difference. #gremlins

What's #gremlins? It's a term I use for thing that break for no apparent reason, have hard to identify causes, and are hard to fix. You go to
bed with everything working, and you wake up to nuclear winter, although nobody touched anything. The sanest insane explanation is that gremlins
screwed with it while you weren't watching.

## 2022-01-21

Crazy assembly

`Source/Engine/VPL_UtilityCallback.c :: VPLEnvironmentCallbackCreate`, file lines 132-147. I thought replacing the x86 32-bit inline asm would be
easy-ish. Then I encountered this...

There's abuse of calling conventions everywhere... My idea is to figure out a way to use function pointers and variadic arguments, and let
the C compiler do its thing, rather than reinvent the wheel. I'm ignoring the PPC code completely, by the way.

## 2022-01-22

Debugging: Summary Type

It got tedious quick to debug `Int1 *` strings. Xcode has this thing called "Edit Summary Format", but its documentation is as non-existent as it gets.
A few Google results and some experimentation later, and I got these:

* `Int *`

  `{(char *)$VAR}:s`

* `V_Object`

  * used for terminals (node inputs)
  * a hint of the contained type, for easier casting in the variable list
  * `{(enum dataType)$VAR.type}`

* `V_Object *`

  * used for roots (node outputs)
  * similar to `V_Object`, but with pointer dereferencing
  * `{(enum dataType)$VAR->type}`

* `StrFileName`

  * This is a Pascal string of a maximum of 63 characters
  * (len={(int)$VAR[0]}); {(char*)$VAR+1}:s
  * This is a fingers-crossed approach, hoping there's always a 0 character at the end.
  * I have no clue as to what happens with longer file names or where the full path to the file is supposed to go

## 2022-01-22

Pinacle of debugging

Put a breakpoint at `VPLLogStringReturn` in _VPL_Errors.c_. It will most likely give you a usable stack trace pointing at the cause of a problem.
In my case, I'm trying to figure out why Marten isn't finding the project files when I open a project. The error messages are taken directly from
MacOS APIs, which meant zero results when searching for "Did not find file" in the source code.

## 2022-01-22

Variadic function forwarding

I'm not a big fan of inline assembly for variadic function forwarding, but apparently that's the way to go... _Shakes head_ Marten has quite a few places
where it uses asm in order to pass variable number of parameters to other functions. I was hoping there'd be a better way, but alas, it doesn't look like it.

Refs:

* <https://stackoverflow.com/questions/150543/forward-an-invocation-of-a-variadic-function-in-c>
* <http://c-faq.com/varargs/handoff.html>
* <https://stackoverflow.com/questions/1721655/passing-parameters-dynamically-to-variadic-functions>
* <https://www.haible.de/bruno/documentation/ffcall/avcall/avcall.html>

It would take some crazy refactoring to pass an array of arguments as the last parameter. I'm not discounting it yet though, for portability purposes.

## 2022-01-24

Interface Builder 3 IDs

What do 'LNam' and 'usrb' mean? Well, it turns out that if you put up to four characters between single quotes in a constant you get an integer.
Interface Builder elements have numeric integers. In this case, `LNam = 1280205165` and `usrb = 1970500194`. You can find those numbers in Carbon XIBs.

It turns out that I broke some functionality just by converting the NIB 2.0 files to XIB 3.0. `LNam` and `usrb` are the actual signatures in NIB 2.0,
but in XIB 3.0 they get turned into numbers. Somehow the `LNam` can be discovered by the API even after conversion, but the `usrb` cannot... They still
use their alphanumeric names in the Interface Builder though. It's just the on-disk format that changed.

It also looks like it's not a file format issue. I put the NIBs back and the problems didn't go away.

The Carbon error codes are all in `MacErrors.h` by the way. Googling resulted in mostly nothing. I did discover <osstatus.com> just now though, which
pointed me to MacErrors.h

There's also a lot of help at <http://mirror.informatimago.com/next/developer.apple.com/documentation/Carbon/index.html> while Apple decided that old
documentation isn't worth keeping around (and even writing new documentation isn't worth the effort, by the looks of their developer docs site).

## 2022-01-25

The VPX/VPL/VPZ archive format

I've been looking into the disk persistence file format today. I'm having trouble using the Marten IDE, and thought I'd have a look inside the files
it produces. I feel like sometimes I'd like to edit these files while Marten is unusable, unhelpful, or just a pain.

For now I have mastered the decoding of the files in Python. There's Python code to this effect in the _Radu_ directory. My intention is to add an
export to YML to make reading the object graph easy in a terminal and, later on, an encoder that would take the YML and produse VplArchive files.
I'd also like to write a project exporter that produces the inline files that correspond to Marten code - and not even necessarily for C either.
>>>>>>> 1e00ef58e29307e75844c5e847066b601d35cb10
