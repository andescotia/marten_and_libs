## Description

The current repository contains a minimal subset of all the code I have received from Scott Anderson.

This code has been adapted from the original archive to make it actually compile.

Please see [LICENCE.md]() for an exmplanation of how I came into possession of the source code.

## Tool set

* MacOS X 10.6.8 Snow Leopard
* XCode 3.2

I put a quick bash script together - `build.bash` - to build everything in one go from the Terminal.

All build output goes into _XCode/build_

# Hurdles

This code is _OLD_! It supports PowerPC and i386 processors.

Migration to 64-bit is hindered by two things:

1. (easy) inline asm
    there isn't a lot of it, and it can be refactored to use macros, as it's following very well defined patterns.
2. (hard) Carbon
    Marten makes heavy use of Carbon, which has been removed from macOS Catalina onward. Replacing it may be a monumental task, to say the least.

# Where's the rest of it?

I did not dump the entire source code in here because:

1. It wasn't versioned, so there's no information that would help.
2. There's a lot of crud in there that I'd like to prune before I put it in git.
3. If necessary, I'll put the archive on a file sharing site.

Happy code archeology :)
